//
//  StreamerSizeCalculator2ViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 11/6/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

@class RAUnit;
@class CalcTextfield;

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface StreamerSizeCalculator2ViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIPrintInteractionControllerDelegate> 
{
    bool isIpad;
    
    double mass;
    double diameter;
    double length;
    
    CalcTextfield *massTextField;
    UILabel *massUnitLabel;
    CalcTextfield *diameterTextField;
    UILabel *diameterUnitLabel;
    CalcTextfield *lengthTextField;
    UILabel *lengthUnitLabel;
    
    RAUnit *massUnit;
    RAUnit *diameterUnit;
    RAUnit *lengthUnit;
    
    UIScrollView *scrollView;
    // Rocket *rocket;
    // FlightLog *flightLog;
    
    UITextField *activeField;
    UIActionSheet *sheet;

}

@property (nonatomic, retain) IBOutlet CalcTextfield *massTextField;
@property (nonatomic, retain) IBOutlet UILabel *massUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *diameterTextField;
@property (nonatomic, retain) IBOutlet UILabel *diameterUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *lengthTextField;
@property (nonatomic, retain) IBOutlet UILabel *lengthUnitLabel;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

- (IBAction)updateFields:(id)sender;
- (IBAction)infoButtonAction:(id)sender;
- (IBAction) exportButtonAction:(id)sender;
- (IBAction) showEmailModalView:(id)sender;
- (IBAction) printCalculation:(id)sender;
- (NSString *) exportMsgText;
- (NSString *) exportMsgHtml;
- (void) enableTextfields;
- (void) resignAllResponders;

- (void) resetTextfieldDisplay;

@end