//
//  CalculatorRootViewControllerCell.h
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

@interface CalculatorRootViewControllerCell : UITableViewCell {
    
    UILabel *label;

}

@property (nonatomic, retain) IBOutlet UILabel *label;

@end
