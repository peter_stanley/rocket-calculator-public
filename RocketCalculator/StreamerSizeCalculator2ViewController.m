//
//  StreamerSizeCalculator2ViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 11/6/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

#import "StreamerSizeCalculator2ViewController.h"
#import "RAUnit.h"
#import "RocketCalculatorAppDelegate.h"
#import "Calculator.h"
#import "GenericFormulaViewController.h"
#import "CalcTextfield.h"

@implementation StreamerSizeCalculator2ViewController

#define MASS_TEXTFIELD 1
#define DIAMETER_TEXTFIELD 2
#define LENGTH_TEXTFIELD 3

@synthesize massTextField, diameterTextField, lengthTextField, massUnitLabel, diameterUnitLabel, lengthUnitLabel, scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)registerForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unRegisterForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardDidShowNotification 
                                                  object:nil]; 
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];  
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    RocketCalculatorAppDelegate *appDelegate = (RocketCalculatorAppDelegate*)[[UIApplication sharedApplication] delegate];
        
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown) {
        
        CGFloat tabBarHeight = appDelegate.tabBarController.tabBar.frame.size.height;
        
        NSDictionary* info = [aNotification userInfo];
        
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        
        self.scrollView.contentInset = contentInsets;
        
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        CGRect aRect = self.view.frame;
        
        aRect.size.height -= kbSize.height;

        
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            
            //CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height);
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height+tabBarHeight);
            
            [scrollView setContentOffset:scrollPoint animated:YES];
            
        }
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mass = 0;
    diameter = 0;
    length = 0;

    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];

    CGFloat frameHeight = self.view.frame.size.height;
    CGFloat frameWidth = self.view.frame.size.width;
    
    UIScrollView *tempScrollView=(UIScrollView *)self.view;
    tempScrollView.contentSize=CGSizeMake(frameWidth, frameHeight * 2);

    self.title = @"Streamer Size";
    
	UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateFields:)];
	gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
	[gestureRecognizer release];
    
    massUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_rocket_mass"] ofDimension:@"weight"];
    
    diameterUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_streamer_diameter"] ofDimension:@"length"];
    
    lengthUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_streamer_length"] ofDimension:@"length"];

    // Setup toolbar
    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 75.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    // Add actionsheet button
    UIButton *exportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exportButton.frame = CGRectMake(0, 0, 25, 20); //set frame for button
    
    UIImage *buttonImage = [UIImage imageNamed:@"action2.png"];
    
    [exportButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [exportButton addTarget:self action:@selector(exportButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:exportButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    
    // Create a spacer.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    bi.width = 8.0f;
    [buttons addObject:bi];
    [bi release];
    self.navigationController.navigationBar.translucent = NO;
    // Add info button
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    infoButton.frame = CGRectMake(0, 0, 20, 20); //set frame for button
    
    buttonImage = [UIImage imageNamed:@"info_g.png"];
    
    [infoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    
    [tools setItems:buttons animated:NO];
    [buttons release];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    [tools release];
    self.navigationItem.rightBarButtonItem = twoButtons;
    
    [twoButtons release];

}

- (IBAction)infoButtonAction:(id)sender {
    GenericFormulaViewController *infoView = [[[GenericFormulaViewController alloc] init] autorelease];
    infoView.htmlFileName = @"streamerCalculator2";
    [infoView setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    //[self presentModalViewController:infoView animated:YES];
    [self presentViewController:infoView animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.massUnitLabel.text = [NSString stringWithFormat:@"(%@)", massUnit.name];
    self.massTextField.placeholder = [NSString stringWithFormat:@"%@", massUnit.label];
    
    self.diameterUnitLabel.text = [NSString stringWithFormat:@"(%@)", diameterUnit.name];
    self.diameterTextField.placeholder = [NSString stringWithFormat:@"%@", diameterUnit.label];
    
    self.lengthUnitLabel.text = [NSString stringWithFormat:@"(%@)", lengthUnit.name];
    self.lengthTextField.placeholder = [NSString stringWithFormat:@"%@", lengthUnit.label];
    
    [self resetTextfieldDisplay];
}

- (void)willEnterForeground
{
    NSLog(@"will enter foreground seen");
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self resetTextfieldDisplay];
    [self.view setNeedsDisplay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unRegisterForKeyboardNotifications];
}
- (void)viewDidUnload
{
    self.scrollView = nil;
    self.lengthUnitLabel = nil;
    self.diameterUnitLabel = nil;
    self.massUnitLabel = nil;
    self.lengthTextField = nil;
    self.diameterTextField = nil;
    self.massTextField = nil;
    
    [super viewDidUnload];
}

- (void)dealloc
{
    [scrollView release];
    [lengthUnitLabel release];
    [diameterUnitLabel release];
    [massUnitLabel release];
    [lengthTextField release];
    [diameterTextField release];
    [massTextField release];
    
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    if (isIpad) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    } else {
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }

    activeField = textField;
    
    if (textField.tag == MASS_TEXTFIELD) {
        
        massTextField.text = nil;
        mass = 0;
        
        diameterTextField.text = nil;
        diameter = 0;
        
        lengthTextField.text = nil;
        length = 0;
        
    } else if (textField.tag == DIAMETER_TEXTFIELD) {
        
        diameterTextField.text = nil;
        diameter = 0;
        
        lengthTextField.text = nil;
        length = 0;
        
    } else if (textField.tag == LENGTH_TEXTFIELD) {
        
        [textField resignFirstResponder];
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    activeField = nil;
    
}
- (IBAction)updateFields:(id)sender
{
    
    [self resignAllResponders];
    [self enableTextfields];
    
    double baseMass = 0;
    double baseDiameter = 0;
    double baseLength = 0;
    double calcArea = 0;
    double calcWidth = 0;
    double calcLength = 0;
    
    if (!mass) {
        
        mass = [massTextField.text doubleValue];
        
    }
    
    if (!diameter) {
        
        diameter = [diameterTextField.text doubleValue];
        
    }
    
    if (!length) {
        
        length = [lengthTextField.text doubleValue];
        
    }

    // get base units
    baseMass = [[massUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:mass]] doubleValue];
    baseDiameter = [[diameterUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:diameter]] doubleValue];    
    baseLength = [[lengthUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:length]] doubleValue];        
    
    // if nothing changed do nothing
    if (baseMass != 0 && baseDiameter != 0 && baseLength != 0) {
        NSLog(@"Doing nothing");
        return;
    }
    
    Calculator *calculator = [[Calculator alloc] init];
        
    calcArea = [calculator calcStreamerAreaFromMass:baseMass];
    
    if (baseDiameter == 0) {

        calcWidth = [calculator calcStreamerWidthFromArea:calcArea];
    } else {

        calcWidth = baseDiameter;
    }
    
    calcLength = [calculator calcStreamerLengthFromWidth:calcWidth andArea:calcArea];
    
    if (mass && !(diameter && length)) { // Calc width and length only
        
        diameter = [[diameterUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcWidth] toUnit:diameterUnit]floatValue];
        
        diameterTextField.text = [NSString stringWithFormat:@"%.2f", diameter];
        
        // supply length
        length = [[lengthUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcLength] toUnit:lengthUnit]floatValue];
        
        lengthTextField.text = [NSString stringWithFormat:@"%.2f", length];
        
    } else if (diameter && !length) { // Update only length
        
        length = [[lengthUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcLength] toUnit:lengthUnit]floatValue];
        
        lengthTextField.text = [NSString stringWithFormat:@"%.2f", length];
        
    }
    
    [calculator release];
    
    [self resetTextfieldDisplay];
}

-(IBAction) showEmailModalView:(id)sender 
{
    
    if([MFMailComposeViewController canSendMail]){
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        
        if (picker) {
    
            picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
            NSString *mailSubject = [NSString stringWithFormat:@"%@ Calculation", self.title];
            NSString *mailMsg = [self exportMsgHtml];
    
            [picker setSubject:mailSubject];
    
            [picker setMessageBody:mailMsg isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
    
            picker.navigationBar.barStyle = UIBarStyleDefault; // choose your style, unfortunately, Translucent colors behave quirky.
    
            //[self presentModalViewController:picker animated:YES];
            [self presentViewController:picker animated:YES completion:nil];
            [picker release];
            
        }
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Mail Accounts" message:@"Please set up a Mail account in order to send email." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        
        
    }

    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{ 
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-("
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
            
            break;
    }
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)exportButtonAction:(id)sender
{
    if (![UIPrintInteractionController isPrintingAvailable]) {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Email Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", nil];
        
    } else {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Export Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", @"Print", nil];
    }
    // Show the sheet
    [sheet showFromTabBar:self.tabBarController.tabBar];
    [sheet release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    //NSLog(@"Button %d", buttonIndex);
    switch (buttonIndex) {
        case 0:
            [self showEmailModalView:nil];
            break;
            
        case 1:
            [self printCalculation:nil];
            break;
            
        default:
            break;
    }
}

#pragma mark -

#pragma mark Printing



// Print button handler that sends our print instructions to the OS

- (IBAction)printCalculation:(id)sender {
    
    // Get a reference to the singleton iOS printing concierge
    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    
    if(pic && [UIPrintInteractionController isPrintingAvailable]) {
        //  UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = self.title;
        pic.printInfo = printInfo;
        
        // NSString *msg = @"dsfs fs ff sdf asf asf safs";
        NSString *msg = [self exportMsgText];
        // NSLog(@"msg = %@", msg);
        UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc]
                                                     initWithText:msg];
        textFormatter.startPage = 0;
        textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
        textFormatter.maximumContentWidth = 6 * 72.0;
        pic.printFormatter = textFormatter;
        [textFormatter release];
        pic.showsPageRange = YES;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSLog(@"Printing could not complete because of error: %@", error);
            }
        };
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [pic presentFromRect:self.tabBarController.tabBar.frame inView:self.view animated:YES completionHandler:completionHandler];
        } else {
            [pic presentAnimated:YES completionHandler:completionHandler];
        }
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Printing Error" message:@"Your device doesn't support printing" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    
}

- (NSString *) exportMsgText
{
    NSString *title = self.title;
    
    NSString *massString = massTextField.text;
    NSString *massUnitString = massUnit.name;
    
    NSString *diameterString = diameterTextField.text;
    NSString *diameterUnitString = diameterUnit.name;
    
    NSString *lengthString = lengthTextField.text;
    NSString *lengthUnitString = lengthUnit.name;
    
    NSString *msg = [NSString stringWithFormat:@"%@\n\nRocket Mass: %@ (%@)\nStreamer Width: %@ (%@)\nStreamer Length: %@ (%@)\n\n\nGenerated by Rocket Calculator\nAvailable on the Apple App Store",title, massString, massUnitString, diameterString, diameterUnitString, lengthString, lengthUnitString];
    
    return msg;

}

- (NSString *) exportMsgHtml
{
    NSString *title = self.title;
    
    NSString *massString = massTextField.text;
    NSString *massUnitString = massUnit.name;
    
    NSString *diameterString = diameterTextField.text;
    NSString *diameterUnitString = diameterUnit.name;
    
    NSString *lengthString = lengthTextField.text;
    NSString *lengthUnitString = lengthUnit.name;
        
    NSString *msg = [NSString stringWithFormat:@"%@<br /><br />Rocket Mass: %@ (%@)<br />Streamer Width: %@ (%@)<br />Streamer Length: %@ (%@)<br /><br /><br />Generated by Rocket Calculator<br />Available on the Apple <a href=\"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=484673201&mt=8\">App Store</a>",title, massString, massUnitString, diameterString, diameterUnitString, lengthString, lengthUnitString];
    
    return msg;

}

- (void)enableTextfields
{
    
    [massTextField setUserInteractionEnabled:YES];
    [diameterTextField setUserInteractionEnabled:YES];
    
}

- (void) resignAllResponders
{
    
    [massTextField resignFirstResponder];
    [diameterTextField resignFirstResponder];
    
}

- (void) resetTextfieldDisplay
{
    // Tint required fields
    
    if (massTextField.text == nil || [massTextField.text isEqualToString:@""] || [massTextField.text doubleValue] == 0) {
        
        self.massTextField.text = nil;
        [self.massTextField setRequired:YES];
        
    } else {
        
        [self.massTextField setRequired:NO];
        [self.diameterTextField setUserInteractionEnabled:YES];
    }
    
    if ((diameterTextField.text == nil || [diameterTextField.text isEqualToString:@""] || [diameterTextField.text doubleValue] == 0) && (!massTextField.text || !mass) ) {
        
        [self.diameterTextField setRequired:NO];
        [self.diameterTextField setUserInteractionEnabled:NO];
    }
    
    if ((diameterTextField.text == nil || [diameterTextField.text isEqualToString:@""] || [diameterTextField.text doubleValue] == 0) && (massTextField.text || mass) ) {
        
        [self.diameterTextField setRequired:NO];
        [self.diameterTextField setUserInteractionEnabled:YES];
    }


    
}


@end
