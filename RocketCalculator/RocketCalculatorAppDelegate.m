//
//  RocketCalculatorAppDelegate.m
//  RocketCalculator
//
//  Created by Peter Stanley on 11/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "RocketCalculatorAppDelegate.h"
#import "CalculatorRootViewController.h"
#import "RAUnit.h"

@interface RocketCalculatorAppDelegate (Private)

- (NSString *) documentPathOfFile:(NSString *)filename;
- (NSString *) resourcePathOfPlist:(NSString *)filename;
- (NSArray *) loadUnitsOfDimension:(NSString *)dimension;

@end

@implementation RocketCalculatorAppDelegate

@synthesize window = _window, tabBarController;
@synthesize calculatorRootViewController, infoListRootViewController;
@synthesize unitDict, unitLabels;

+ (void)initialize
{
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *pListPath = [path stringByAppendingPathComponent:@"Settings.bundle/Root.plist"];
    NSDictionary *pList = [NSDictionary dictionaryWithContentsOfFile:pListPath];
    
    NSMutableArray *prefsArray = [pList objectForKey:@"PreferenceSpecifiers"];
    NSMutableDictionary *regDictionary = [NSMutableDictionary dictionary];
    for (NSDictionary *dict in prefsArray) {
        NSString *key = [dict objectForKey:@"Key"];
        if (key) {
            id value = [dict objectForKey:@"DefaultValue"];
            [regDictionary setObject:value forKey:key];
        } 
        
    }
     
    [[NSUserDefaults standardUserDefaults] registerDefaults:regDictionary];
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],@"enableTWTAlert",[NSNumber numberWithBool:YES],@"enableMachAlert",nil]];

}

#pragma mark -
#pragma mark Application's documents directory

/**
 Returns the path to the application's documents directory.
 */
- (NSString *)applicationDocumentsDirectory {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (void)dealloc
{
    [unitLabels release];
    [unitDict release];
    [infoListRootViewController release];
    [calculatorRootViewController release];
    [tabBarController release];
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if (![prefs stringForKey:@"temperature"]) {
        
        [prefs setValue:@"15" forKey:@"temperature"];
        
    }
    
    if (![prefs stringForKey:@"elevation"]) {
        
        [prefs setValue:@"0" forKey:@"elevation"];
        
    }

    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = self.tabBarController;
    
   [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

#pragma mark -
#pragma mark Utilities

// returns an array of all units of desired dimension
- (NSArray *) unitsOfDimension:(NSString *)dimension
{
	// if unit dimension is already loaded, we will find it here
	if(self.unitDict) {
		NSArray *unitsOfDimension = [unitDict objectForKey:dimension];
		if(unitsOfDimension) {
			return unitsOfDimension;
		}
	}
	
	// this will load the desired dimension and return it's units (if available)
	return [self loadUnitsOfDimension:dimension];
}


// returns the desired unit and loads units of the dimension if it cannot be found
- (RAUnit *) unitWithName:(NSString *)name ofDimension:(NSString *)dimension
{
	if(!name || !dimension) {
		return nil;
	}
	
	NSArray *unitsOfDimension = [self unitsOfDimension:dimension];
	
	// the dimension exists, the unit must be here if it is a valid unit
	if(unitsOfDimension) {
		for(RAUnit *unit in unitsOfDimension) {
			if([unit.name isEqualToString:name]) {
				return unit;
			}
		}
		
		NSLog(@"Error: Requested unit with name \"%@\" of dimension \"%@\" does not exist!", name, dimension);
	}
	
	return nil;
}


// returns YES when the units are there (no matter if they were there already or have just been loaded)
- (NSArray *) loadUnitsOfDimension:(NSString *)dimension
{
	if(dimension) {
		if(!unitDict) {
			self.unitDict = [NSMutableDictionary dictionary];
		}
		if(!unitLabels) {
			self.unitLabels = [NSDictionary dictionaryWithContentsOfFile:[self resourcePathOfPlist:@"Unit_labels"]];
		}
		
		NSString *unitResource = [self resourcePathOfPlist:[NSString stringWithFormat:@"Units_%@", dimension]];
		NSDictionary *unitInfoDict = [NSDictionary dictionaryWithContentsOfFile:unitResource];
		if(unitInfoDict) {
			NSString *baseUnitName = [unitInfoDict objectForKey:@"baseUnit"];
			NSArray *unitsOfDimension = [unitInfoDict objectForKey:@"units"];
			float minRange = 0;
			float maxRange = 99999999;
			if([unitInfoDict valueForKey:@"minPlausibleRange"]) {
				minRange = [[unitInfoDict valueForKey:@"minPlausibleRange"] floatValue];
			}
			if([unitInfoDict valueForKey:@"maxPlausibleRange"]) {
				maxRange = [[unitInfoDict valueForKey:@"maxPlausibleRange"] floatValue];
			}
			
			// We got units, prepare to instantiate them
			if(unitsOfDimension) {
				RAUnit *newBaseUnit = nil;
				NSMutableArray *newUnits = [NSMutableArray arrayWithCapacity:[unitsOfDimension count]];
				
				// create RAUnit objects for each unit we find
				for(NSDictionary *uDict in unitsOfDimension) {
					RAUnit *unit = [[RAUnit alloc] initWithDict:uDict ofDimension:dimension];
					
					NSString *locLabel = [unitLabels objectForKey:unit.name];
					if(nil != locLabel) {
						unit.label = locLabel;
					}
					
					if([unit.name isEqualToString:baseUnitName]) {
						newBaseUnit = unit;
					}
					
					[newUnits addObject:unit];
					[unit release];
				}
				
				// assign the base unit to all units and return
				if(newBaseUnit) {
					NSDecimalNumber *minPRange = (NSDecimalNumber *)[NSDecimalNumber numberWithFloat:minRange];
					NSDecimalNumber *maxPRange = (NSDecimalNumber *)[NSDecimalNumber numberWithFloat:maxRange];
					for(RAUnit *unit in newUnits) {
						unit.baseUnit = newBaseUnit;
						unit.minPlausibleRange = minPRange;
						unit.maxPlausibleRange = maxPRange;
					}
					
					// save unitDict
					NSArray *dimensionArray = [NSArray arrayWithArray:newUnits];
					[unitDict setObject:dimensionArray forKey:dimension];
					
					return dimensionArray;
				}
				else {
					NSLog(@"Error: Created units, but base unit named %@ was not amongst them! NOT KEEPING UNITS!", baseUnitName);
				}
			}
			else {
				NSLog(@"Error: There are no units for dimension \"%@\"", dimension);
			}
		}
		else {
			NSLog(@"Error: There is no dict for dimension \"%@\"", dimension);
		}
	}
	
	return nil;
}

- (NSString *) documentPathOfFile:(NSString *)filename
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [[paths objectAtIndex:0] stringByAppendingPathComponent:filename];
}

- (NSString *) resourcePathOfPlist:(NSString *)filename
{
	NSBundle *thisBundle = [NSBundle bundleForClass:[self class]];
	return [thisBundle pathForResource:filename ofType:@"plist"];
}

- (BOOL) deviceIsIpad {
    BOOL isIpad = NO;
    if ([UIDevice instancesRespondToSelector:@selector(userInterfaceIdiom)]) {
        UIUserInterfaceIdiom idiom = [[UIDevice currentDevice] userInterfaceIdiom];
        
        if (idiom == UIUserInterfaceIdiomPad) {
            isIpad = YES;
        }
    }
    return isIpad;
}

@end