//
//  BPCalculatorViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

#import "BPCalculatorViewController.h"
#import "RAUnit.h"
#import "RocketCalculatorAppDelegate.h"
#import "Calculator.h"
#import "GenericFormulaViewController.h"
#import "CalcTextfield.h"

@implementation BPCalculatorViewController

#define WIDTH_TEXTFIELD 1
#define LENGTH_TEXTFIELD 2
#define PRESSURE_TEXTFIELD 3
#define BP_TEXTFIELD 4
#define FORCE_TEXTFIELD 5

@synthesize widthTextField, lengthTextField, pressureTextField, widthUnitLabel, lengthUnitLabel, pressureUnitLabel, bpAmountTextField, bpAmountUnitLabel, forceTextField, forceUnitLabel, widthUnitLabel2, count256Label, count440Label, count632Label, scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)registerForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unRegisterForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardDidShowNotification 
                                                  object:nil]; 
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];  
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    RocketCalculatorAppDelegate *appDelegate = (RocketCalculatorAppDelegate*)[[UIApplication sharedApplication] delegate];
        
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown) {
        
        CGFloat tabBarHeight = appDelegate.tabBarController.tabBar.frame.size.height;
        
        NSDictionary* info = [aNotification userInfo];
        
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        
        self.scrollView.contentInset = contentInsets;
        
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        CGRect aRect = self.view.frame;
        
        aRect.size.height -= kbSize.height;

        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height+tabBarHeight);
            
            [scrollView setContentOffset:scrollPoint animated:YES];
            
        }
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
}
 
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    width = 0;
    length = 0;
    pressure = 0;
    amount = 0;
    force = 0;
    
    self.widthTextField.text = nil;
    self.lengthTextField.text = nil;
    self.pressureTextField.text = nil;
    self.bpAmountTextField.text = nil;
    self.forceTextField.text = nil;
    
    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];

    CGFloat frameHeight = self.view.frame.size.height;
    CGFloat frameWidth = self.view.frame.size.width;
    
    UIScrollView *tempScrollView=(UIScrollView *)self.view;
    tempScrollView.contentSize=CGSizeMake(frameWidth, frameHeight * 2);

    self.title = @"Ejection Charge";
    
	UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateFields:)];
	gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
	[gestureRecognizer release];
    
    widthUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_bp_tube_width"] ofDimension:@"length"];
    
    lengthUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_bp_tube_length"] ofDimension:@"length"];
    
    pressureUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_bp_pressure"] ofDimension:@"pressure"];
    
    amountUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_bp_amount"] ofDimension:@"weight"];

    forceUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_bp_force"] ofDimension:@"force"];

    tmpForceUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_bp_force_lbf"] ofDimension:@"force"];

    pressureTextField.text = [NSString stringWithFormat:@"%.2f", [[pressureUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:15.0] toUnit:pressureUnit]floatValue]];

     // Setup toolbar
    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 75.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    // Add actionsheet button
    UIButton *exportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exportButton.frame = CGRectMake(0, 0, 25, 20); //set frame for button
    
    UIImage *buttonImage = [UIImage imageNamed:@"action2.png"];
    
    [exportButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [exportButton addTarget:self action:@selector(exportButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:exportButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    self.navigationController.navigationBar.translucent = NO;
    // Create a spacer.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    bi.width = 8.0f;
    [buttons addObject:bi];
    [bi release];
    
    // Add info button
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    infoButton.frame = CGRectMake(0, 0, 20, 20); //set frame for button
    
    buttonImage = [UIImage imageNamed:@"info_g.png"];
    
    [infoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    
    [tools setItems:buttons animated:NO];
    [buttons release];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    [tools release];
    self.navigationItem.rightBarButtonItem = twoButtons;
    
    [twoButtons release];
    
    
    [self resetTextfieldDisplay];

}

- (IBAction)infoButtonAction:(id)sender {
    GenericFormulaViewController *infoView = [[[GenericFormulaViewController alloc] init] autorelease];
    infoView.htmlFileName = @"blackPowder";
    [infoView setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    //[self presentModalViewController:infoView animated:YES];
    [self presentViewController:infoView animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    
    if (!pressure) {
        pressure = [[pressureUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:15] toUnit:pressureUnit]doubleValue];
    }
    
    [self registerForKeyboardNotifications];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.widthUnitLabel.text = [NSString stringWithFormat:@"(%@)", widthUnit.name];
    self.widthTextField.placeholder = [NSString stringWithFormat:@"%@", widthUnit.label];
    
    self.lengthUnitLabel.text = [NSString stringWithFormat:@"(%@)", lengthUnit.name];
    self.lengthTextField.placeholder = [NSString stringWithFormat:@"%@", lengthUnit.label];
    
    self.pressureUnitLabel.text = [NSString stringWithFormat:@"(%@)", pressureUnit.name];
    self.pressureTextField.placeholder = [NSString stringWithFormat:@"%@", pressureUnit.label];
    
    self.bpAmountUnitLabel.text = [NSString stringWithFormat:@"(%@)", amountUnit.name];
    self.bpAmountTextField.placeholder = [NSString stringWithFormat:@"%@", amountUnit.label];
    
    self.forceTextField.placeholder = [NSString stringWithFormat:@"%@", forceUnit.label];
    self.forceUnitLabel.text = [NSString stringWithFormat:@"(%@)", forceUnit.name];
   // self.widthUnitLabel2.text = [NSString stringWithFormat:@"%@", widthUnit.name];

    [self resetTextfieldDisplay];
    
    [super viewWillAppear:animated];

}

- (void)willEnterForeground
{

    [[NSUserDefaults standardUserDefaults] synchronize];
    [self resetTextfieldDisplay];
    [self.view setNeedsDisplay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unRegisterForKeyboardNotifications];
}
- (void)viewDidUnload
{
    self.scrollView = nil;
    self.count632Label = nil;
    self.count440Label = nil;
    self.count256Label = nil;
    self.widthUnitLabel2 = nil;
    self.forceUnitLabel = nil;
    self.forceTextField = nil;
    self.bpAmountUnitLabel = nil;
    self.bpAmountTextField = nil;
    self.pressureUnitLabel = nil;
    self.lengthUnitLabel = nil;
    self.widthUnitLabel = nil;
    self.pressureTextField = nil;
    self.lengthTextField = nil;
    self.widthTextField = nil;
    
    [super viewDidUnload];
}

- (void)dealloc
{
    [scrollView release];
    [count632Label release];
    [count440Label release];
    [count256Label release];
    [widthUnitLabel2 release];
    [forceUnitLabel release];
    [forceTextField release];
    [bpAmountUnitLabel release];
    [bpAmountTextField release];
    [pressureUnitLabel release];
    [lengthUnitLabel release];
    [widthUnitLabel release];
    [pressureTextField release];
    [lengthTextField release];
    [widthTextField release];
    
    [super dealloc];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    if (isIpad) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    } else {
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }

    activeField = textField;
    
    if (textField.tag == WIDTH_TEXTFIELD) {

        widthTextField.text = nil;
        width = 0;
        
        bpAmountTextField.text = nil;
        amount = 0;
        
        forceTextField.text = nil;
        force = 0;
        
        [lengthTextField setUserInteractionEnabled:NO];
        [pressureTextField setUserInteractionEnabled:NO];
        [bpAmountTextField setUserInteractionEnabled:NO];
        [forceTextField setUserInteractionEnabled:NO];
        
        
    } else if (textField.tag == LENGTH_TEXTFIELD) {

        lengthTextField.text = nil;
        length = 0;
        
        bpAmountTextField.text = nil;
        amount = 0;
        
        forceTextField.text = nil;
        force = 0;
        
        [widthTextField setUserInteractionEnabled:NO];
        [pressureTextField setUserInteractionEnabled:NO];
        [bpAmountTextField setUserInteractionEnabled:NO];
        [forceTextField setUserInteractionEnabled:NO];
        
        
    } else if (textField.tag == PRESSURE_TEXTFIELD) {
        
        pressureTextField.text = nil;
        pressure = 0;
        
        bpAmountTextField.text = nil;
        amount = 0;
        
        forceTextField.text = nil;
        force = 0;
        
        [widthTextField setUserInteractionEnabled:NO];
        [lengthTextField setUserInteractionEnabled:NO];
        [bpAmountTextField setUserInteractionEnabled:NO];
        [forceTextField setUserInteractionEnabled:NO];

    } else if (textField.tag == BP_TEXTFIELD) {
        
        bpAmountTextField.text = nil;
        amount = 0;
        
        pressureTextField.text = nil;
        pressure = 0;
        
        forceTextField.text = nil;
        force = 0;
        
        [widthTextField setUserInteractionEnabled:NO];
        [lengthTextField setUserInteractionEnabled:NO];
        [pressureTextField setUserInteractionEnabled:NO];
        [forceTextField setUserInteractionEnabled:NO];

        
    }   else if (textField.tag == FORCE_TEXTFIELD) {
        
        bpAmountTextField.text = nil;
        amount = 0;
        
        pressureTextField.text = nil;
        pressure = 0;
        
        forceTextField.text = nil;
        force = 0;
        
        [widthTextField setUserInteractionEnabled:NO];
        [lengthTextField setUserInteractionEnabled:NO];
        [pressureTextField setUserInteractionEnabled:NO];
        [bpAmountTextField setUserInteractionEnabled:NO];
        
    }     
    
    [count256Label setAlpha:0.25];
    [count440Label setAlpha:0.25];
    [count632Label setAlpha:0.25];

  
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    activeField = nil;
    
}
- (IBAction)updateFields:(id)sender
{
    
    [self resignAllResponders];
    [self enableTextfields];
    
    double baseWidth = 0;
    double baseLength = 0;
    double basePressure = 0;
    double baseBPAmount = 0;
    double volume = 0;
    double widthInches = 0;
    double calcArea = 0;
    
    double calcPressure = 0;
    double calcBPAmount = 0;
    double calcForce = 0;
    double baseForce = 0;

    if (!width) {
        
        width = [widthTextField.text doubleValue];
        
    }
    
    if (!length) {
        
        length = [lengthTextField.text doubleValue];
        
    }

    if (!pressure) {
        
        pressure = [pressureTextField.text doubleValue];
        
    }
    
    if (!amount) {
        
        amount = [bpAmountTextField.text doubleValue];
        
    }
    
    if (!force) {
        
        force = [forceTextField.text doubleValue];
        
    }
    
    // get base units
    baseWidth = [[widthUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:width]] doubleValue];
    baseLength = [[lengthUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:length]] doubleValue];    
    basePressure = [[pressureUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:pressure]] doubleValue];        
    baseBPAmount = [[amountUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:amount]] doubleValue];
    baseForce = [[forceUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:force]] doubleValue];
    
    // Convert meters to inches
    widthInches = baseWidth * 39.3700787;
    
    Calculator *calculator = [[Calculator alloc] init];
    
    calcArea = [calculator calcAreaOfCircle:widthInches];
    
    volume = [calculator calcVolumeFromWidth:baseWidth andLength:baseLength];
    
    // calc bpamount and force
    if (baseLength && baseWidth && basePressure && !baseBPAmount && !baseForce ) {

        // compute BP amount, then force

        calcBPAmount = [calculator calcBPFromVolume:volume andPressure:basePressure];

        amount = [[amountUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcBPAmount] toUnit:amountUnit]floatValue];
        
        bpAmountTextField.text = [NSString stringWithFormat:@"%.2f", amount];
        
        calcForce = [calculator calcForceFromArea:calcArea andPressure:basePressure];

        baseForce = [[tmpForceUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:calcForce]] doubleValue];
        
        force = [[forceUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseForce] toUnit:forceUnit]floatValue];
        
        forceTextField.text = [NSString stringWithFormat:@"%.2f", force];
        
        // convert baseForce to lbf
        poundsForce = baseForce * 0.224808943;
        
    }
    
    // compute pressure, then force.
    else if (baseLength && baseWidth && baseBPAmount && !basePressure && !baseForce) {

        // compute pressure
        calcPressure = [calculator calcPressureFromMass:baseBPAmount andVolume:volume];
        
        pressure = [[pressureUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcPressure] toUnit:pressureUnit]floatValue];
        
        pressureTextField.text = [NSString stringWithFormat:@"%.2f", pressure];
        
        // compute force
        calcForce = [calculator calcForceFromArea:calcArea andPressure:calcPressure];

        baseForce = [[tmpForceUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:calcForce]] doubleValue];
        
        force = [[forceUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseForce] toUnit:forceUnit]floatValue];
        
        forceTextField.text = [NSString stringWithFormat:@"%.2f", force];
        
        // convert baseForce to lbf
        poundsForce = baseForce * 0.224808943;
    }
    
    // calculate pressure, then bp amount
   else if (baseLength && baseWidth && baseForce && !baseBPAmount && !basePressure) {

       // compute psi, then bp amount
        calcArea = [calculator calcAreaOfCircle:widthInches];
        baseForce = [[forceUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:force]] doubleValue]; 
        calcForce = [[forceUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseForce] toUnit:tmpForceUnit]floatValue];
        
        calcPressure = [calculator calcPsiFromForce:calcForce andArea:calcArea];
       
        pressure = [[pressureUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcPressure] toUnit:pressureUnit]floatValue];
       
       pressureTextField.text = [NSString stringWithFormat:@"%.2f", pressure];
       
        // Update bp amount
        calcBPAmount = [calculator calcBPFromVolume:volume andPressure:calcPressure];
       
       amount = [[amountUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcBPAmount] toUnit:amountUnit]floatValue];
       
       bpAmountTextField.text = [NSString stringWithFormat:@"%.2f", amount];
       
       // convert baseForce to lbf
       poundsForce = baseForce * 0.224808943;
       
    }
    
    // get number shear pins
    
    // number of 2-56 screws
    numScrews256 = [calculator calcAvg256ScrewsFromForce:poundsForce];
        
    // number of 4-40 screws
    numScrews440 = [calculator calcAvg440ScrewsFromForce:poundsForce];
    
    // number of 632 screws
    numScrews632 = [calculator calcAvg632ScrewsFromForce:poundsForce];
    
    [self updateShearPinCounts];

    [calculator release];
    
    [self resetTextfieldDisplay];
}
    
- (void) updateShearPinCounts
{
        if (numScrews256 == 1) {
            
            numScrews256 = 0;
            
        } 
        
        count256Label.text = [NSString stringWithFormat:@"%d", numScrews256];
        
        if (numScrews440 == 1) {
            
            numScrews440 = 0;
            
        } 
        
        count440Label.text = [NSString stringWithFormat:@"%d", numScrews440];
                
        if (numScrews632 == 1) {
            
            numScrews632 = 0;
            
        } 
        
        count632Label.text = [NSString stringWithFormat:@"%d", numScrews632];
           
    [count256Label setAlpha:1.0];
    [count440Label setAlpha:1.0];
    [count632Label setAlpha:1.0];
    
}

    
-(IBAction) showEmailModalView:(id)sender 
{
    
    if([MFMailComposeViewController canSendMail]){
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        
        if (picker) {
   
            picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
            NSString *mailSubject = [NSString stringWithFormat:@"%@ Calculation", self.title];
            NSString *mailMsg = [self exportMsgHtml];
    
            [picker setSubject:mailSubject];
    
            [picker setMessageBody:mailMsg isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
    
            picker.navigationBar.barStyle = UIBarStyleDefault; // choose your style, unfortunately, Translucent colors behave quirky.
    
            //[self presentModalViewController:picker animated:YES];
            [self presentViewController:picker animated:YES completion:nil];
            [picker release];
            
        }
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Mail Accounts" message:@"Please set up a Mail account in order to send email." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        
        
    }

    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{ 
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-("
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
            
            break;
    }
   // [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)exportButtonAction:(id)sender
{
    if (![UIPrintInteractionController isPrintingAvailable]) {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Email Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", nil];
        
    } else {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Export Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", @"Print", nil];
    }
    // Show the sheet
    [sheet showFromTabBar:self.tabBarController.tabBar];
    [sheet release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{

    switch (buttonIndex) {
        case 0:
            [self showEmailModalView:nil];
            break;
            
        case 1:
            [self printCalculation:nil];
            break;
            
        default:
            break;
    }
}

    
#pragma mark -

#pragma mark Printing



// Print button handler that sends our print instructions to the OS

- (IBAction)printCalculation:(id)sender {
    
    // Get a reference to the singleton iOS printing concierge
    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    
    if(pic && [UIPrintInteractionController isPrintingAvailable]) {
        //  UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = self.title;
        pic.printInfo = printInfo;
        
        NSString *msg = [self exportMsgText];
        UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc]
                                                     initWithText:msg];
        textFormatter.startPage = 0;
        textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
        textFormatter.maximumContentWidth = 6 * 72.0;
        pic.printFormatter = textFormatter;
        [textFormatter release];
        pic.showsPageRange = YES;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSLog(@"Printing could not complete because of error: %@", error);
            }
        };
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [pic presentFromRect:self.tabBarController.tabBar.frame inView:self.view animated:YES completionHandler:completionHandler];
        } else {
            [pic presentAnimated:YES completionHandler:completionHandler];
        }
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Printing Error" message:@"Your device doesn't support printing" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    
}

- (NSString *) exportMsgText
{
    NSString *title = self.title;
    
    NSString *widthString = widthTextField.text;
    NSString *widthUnitString = widthUnit.name;
    
    NSString *lengthString = lengthTextField.text;
    NSString *lengthUnitString = lengthUnit.name;
    
    NSString *pressureString = pressureTextField.text;
    NSString *pressureUnitString = pressureUnit.name;
    
    NSString *bpAmountString = bpAmountTextField.text;
    NSString *bpAmountUnitString = amountUnit.name;
    
    NSString *forceString = forceTextField.text;
    NSString *forceUnitString = forceUnit.name;
    
    NSString *num256 = count256Label.text;
    NSString *num440 = count440Label.text;
    NSString *num632 = count632Label.text;
    
    NSString *msg = [NSString stringWithFormat:@"%@\n\nDeployment Section Measurements\nWidth: %@ (%@)\nLength: %@ (%@)\n\nPressure: %@ (%@)\nFFFF BP Amount: %@ (%@)\nForce Produced: %@ (%@)\n\nNumber of Nylon Screws (Shear pins):\n%@ x 2-56\nor\n%@ x 4-40\nor\n%@ x 6-32\n\nGenerated by Rocket Calculator\n\nAvailable on the Apple App Store",title,widthString, widthUnitString, lengthString, lengthUnitString, pressureString, pressureUnitString, bpAmountString, bpAmountUnitString, forceString, forceUnitString,num256,num440,num632];
    
    return msg;

}

- (NSString *) exportMsgHtml
{
    NSString *title = self.title;
    
    NSString *widthString = widthTextField.text;
    NSString *widthUnitString = widthUnit.name;
    
    NSString *lengthString = lengthTextField.text;
    NSString *lengthUnitString = lengthUnit.name;
    
    NSString *pressureString = pressureTextField.text;
    NSString *pressureUnitString = pressureUnit.name;
    
    NSString *bpAmountString = bpAmountTextField.text;
    NSString *bpAmountUnitString = amountUnit.name;
  
    NSString *forceString = forceTextField.text;
    NSString *forceUnitString = forceUnit.name;
    
    NSString *num256 = count256Label.text;
    NSString *num440 = count440Label.text;
    NSString *num632 = count632Label.text;

    NSString *msg = [NSString stringWithFormat:@"%@<br /><br />Deployment Section Measurements<br />Width: %@ (%@)<br />Length: %@ (%@)<br /><br />Pressure: %@ (%@)<br />FFFF BP Amount: %@ (%@)<br />Force Produced: %@ (%@)<br /><br />Number of Nylon Screws (Shear pins):<br />%@ x 2-56<br />or<br />%@ x 4-40<br />or<br />%@ x 6-32<br /><br />Generated by Rocket Calculator<br /><br />Available on the Apple <a href=\"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=484673201&mt=8\">App Store</a>",title,widthString, widthUnitString, lengthString, lengthUnitString, pressureString, pressureUnitString, bpAmountString, bpAmountUnitString, forceString, forceUnitString,num256,num440,num632];
    
    return msg;

}

- (void)enableTextfields
{
    
    [widthTextField setUserInteractionEnabled:YES];
    [lengthTextField setUserInteractionEnabled:YES];
    [pressureTextField setUserInteractionEnabled:YES];
    [bpAmountTextField setUserInteractionEnabled:YES];
    [forceTextField setUserInteractionEnabled:YES];
    
}

- (void) resignAllResponders
{
    
    [widthTextField resignFirstResponder];
    [lengthTextField resignFirstResponder];
    [pressureTextField resignFirstResponder];
    [bpAmountTextField resignFirstResponder];
    [forceTextField resignFirstResponder];
    
}

- (void) resetTextfieldDisplay
{
    
    // Tint required fields
    
    if (widthTextField.text == nil || [widthTextField.text isEqualToString:@""] || [widthTextField.text doubleValue] == 0) {
        
        self.widthTextField.text = nil;
        [self.widthTextField setRequired:YES];
        
        [self.pressureTextField setUserInteractionEnabled:NO];
        [self.bpAmountTextField setUserInteractionEnabled:NO];
        [self.forceTextField setUserInteractionEnabled:NO];
        
    } else {
        
        [self.widthTextField setRequired:NO];
    }
    
    if (lengthTextField.text == nil || [lengthTextField.text isEqualToString:@""] || [lengthTextField.text doubleValue] == 0) {
        
        self.lengthTextField.text = nil;
        [self.lengthTextField setRequired:YES];
        
        [self.pressureTextField setUserInteractionEnabled:NO];
        [self.bpAmountTextField setUserInteractionEnabled:NO];
        [self.forceTextField setUserInteractionEnabled:NO];

    } else {
        
        [self.lengthTextField setRequired:NO];
    }
    
    if ((widthTextField.text || width) && (lengthTextField.text || length)) {
        
        [self enableTextfields];
        
    }
    
    if ((pressureTextField.text == nil || [pressureTextField.text isEqualToString:@""] || [pressureTextField.text doubleValue] == 0) && (!bpAmountTextField.text || !amount) && (!forceTextField.text || !force)) {
        
        self.pressureTextField.text = nil;
        [self.pressureTextField setRequired:YES];
    } else {
        
        [self.pressureTextField setRequired:NO];
    }
    
    if ((bpAmountTextField.text == nil || [bpAmountTextField.text isEqualToString:@""] || [bpAmountTextField.text doubleValue] == 0) && (!pressure && !force)) {

        self.bpAmountTextField.text = nil;
        [self.bpAmountTextField setRequired:YES];
    } else {
        
        [self.bpAmountTextField setRequired:NO];
    }
    
    if ((forceTextField.text == nil || [forceTextField.text isEqualToString:@""] || [forceTextField.text doubleValue] == 0) && (!pressure && !amount)) {
        
        self.forceTextField.text = nil;
        [self.forceTextField setRequired:YES];
    } else {
        
        [self.forceTextField setRequired:NO];
    }
    
}


@end