//
//  Motor.h
//  RocketCalculator
//
//  Created by Peter Stanley on 4/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

@interface Motor : NSObject {
	NSString *code;
    NSString *modelNumber;
	NSString *mfg;
    double avgThrust;
    double burn_time;
    double Itot;
    double initWt;
    double propWt;
    int dia;
    NSString *impClass;
    NSString *pctImpClass;
    NSString *fileName;
    NSString *propellantType;
    
    NSInteger sectionNumber;
}
@property (nonatomic) NSInteger sectionNumber;
@property (nonatomic, retain) NSString *code;
@property (nonatomic, retain) NSString *modelNumber;
@property (nonatomic, retain) NSString *mfg;
@property (nonatomic) double avgThrust;
@property (nonatomic) double burn_time;
@property (nonatomic) double Itot;
@property (nonatomic) double initWt;
@property (nonatomic) double propWt;
@property (nonatomic) int dia;
@property (nonatomic, retain) NSString *impClass;
@property (nonatomic, retain) NSString *pctImpClass;
@property (nonatomic, retain) NSString *fileName;
@property (nonatomic, retain) NSString *propellantType;


@end

