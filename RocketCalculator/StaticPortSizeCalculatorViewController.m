//
//  StaticPortSizeCalculatorViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

#import "StaticPortSizeCalculatorViewController.h"
#import "RAUnit.h"
#import "RocketCalculatorAppDelegate.h"
#import "Calculator.h"
#import "GenericFormulaViewController.h"
#import "CalcTextfield.h"

@implementation StaticPortSizeCalculatorViewController

#define DIAMETER_TEXTFIELD 1
#define LENGTH_TEXTFIELD 2
#define NUM_PORTS_TEXTFIELD 3

@synthesize diameterTextField, lengthTextField, numPortsTextField, diameterUnitLabel, lengthUnitLabel, portSizeLabel, portSizeUnitLabel, scrollView, rulerScrollView, rulerImageView, rulerImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

// Update ruler image showing line
- (UIImage *)imageByDrawingLineOnImage:(UIImage *)image
{
	// begin a graphics context of sufficient size
	UIGraphicsBeginImageContext(image.size);
    
	// draw original image into the context
	[image drawAtPoint:CGPointZero];
    
	// get the context for CoreGraphics
	CGContextRef ctx = UIGraphicsGetCurrentContext();
    
	// set stroking color and draw circle
	[[UIColor redColor] setStroke];
    
    // get scaled position in inches

    scaledPosition = 458 * (portSize * 39.3700787);

    scaledXPoint = scaledPosition + 8;
    
    CGContextMoveToPoint(ctx, scaledXPoint, 0);
    CGContextAddLineToPoint(ctx, scaledXPoint, 73);
    CGContextSetLineWidth(ctx, 2);
    CGContextStrokePath(ctx);
    
	// make image out of bitmap context
	UIImage *retImage = UIGraphicsGetImageFromCurrentImageContext();
    
	// free the context
	UIGraphicsEndImageContext();
    
	return retImage;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)registerForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unRegisterForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardDidShowNotification 
                                                  object:nil]; 
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];  
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    RocketCalculatorAppDelegate *appDelegate = (RocketCalculatorAppDelegate*)[[UIApplication sharedApplication] delegate];
        
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown) {
        
        CGFloat tabBarHeight = appDelegate.tabBarController.tabBar.frame.size.height;
        
        NSDictionary* info = [aNotification userInfo];
        
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        
        self.scrollView.contentInset = contentInsets;
        
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        CGRect aRect = self.view.frame;
        
        aRect.size.height -= kbSize.height;

        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height+tabBarHeight);
            
            [scrollView setContentOffset:scrollPoint animated:YES];
            
        }
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    diameter = 0;
    length = 0;
    portSize = 0;
    numPorts = 0;
    scaledPosition = 0;
    scaledXPoint = 0;
    
    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];

    CGFloat frameHeight = self.view.frame.size.height;
    CGFloat frameWidth = self.view.frame.size.width;
    
    UIScrollView *tempScrollView=(UIScrollView *)self.view;
    tempScrollView.contentSize=CGSizeMake(frameWidth, frameHeight * 2);

    self.title = @"Static Pressure Port Size";
    
	UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateFields:)];
	gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
	[gestureRecognizer release];
    
    diameterUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_pp_width"] ofDimension:@"length"];
    
    lengthUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_pp_length"] ofDimension:@"length"];
    
    portSizeUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_pp_size"] ofDimension:@"length"];
    
    self.rulerScrollView.contentSize = CGSizeMake(950, 73);

    // Setup toolbar
    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 75.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    // Add actionsheet button
    UIButton *exportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exportButton.frame = CGRectMake(0, 0, 25, 20); //set frame for button
    
    UIImage *buttonImage = [UIImage imageNamed:@"action2.png"];
    
    [exportButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [exportButton addTarget:self action:@selector(exportButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:exportButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    self.navigationController.navigationBar.translucent = NO;
    // Create a spacer.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    bi.width = 8.0f;
    [buttons addObject:bi];
    [bi release];
    
    // Add info button
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    infoButton.frame = CGRectMake(0, 0, 20, 20); //set frame for button
    
    buttonImage = [UIImage imageNamed:@"info_g.png"];
    
    [infoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    
    [tools setItems:buttons animated:NO];
    [buttons release];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    [tools release];
    self.navigationItem.rightBarButtonItem = twoButtons;
    
    [twoButtons release];
    
    [self resetTextfieldDisplay];
    
}

- (IBAction)infoButtonAction:(id)sender {
    
    GenericFormulaViewController *infoView = [[[GenericFormulaViewController alloc] init] autorelease];
    infoView.htmlFileName = @"staticPressurePort";
    [infoView setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    //[self presentModalViewController:infoView animated:YES];
    [self presentViewController:infoView animated:YES completion:nil];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.diameterUnitLabel.text = [NSString stringWithFormat:@"(%@)", diameterUnit.name];
    self.diameterTextField.placeholder = [NSString stringWithFormat:@"%@", diameterUnit.label];
    
    self.lengthUnitLabel.text = [NSString stringWithFormat:@"(%@)", lengthUnit.name];
    self.lengthTextField.placeholder = [NSString stringWithFormat:@"%@", lengthUnit.label];
    
    self.portSizeUnitLabel.text = [NSString stringWithFormat:@"(%@)", portSizeUnit.name];

    if (numPortsTextField.text == nil || [numPortsTextField.text isEqualToString:@""] ) {
        numPortsTextField.text = @"4";
    }
    
    self.diameterTextField.text = nil;
    self.lengthTextField.text = nil;
    
    
    [self resetTextfieldDisplay];
    
}

- (void)willEnterForeground
{
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self resetTextfieldDisplay];
    [self.view setNeedsDisplay];
    
}

- (void)viewWillDisappear:(BOOL)animated
{   
    [super viewWillDisappear:animated];
    [self unRegisterForKeyboardNotifications];
    
}

- (void)viewDidUnload
{

    self.rulerImageView = nil;
    self.rulerScrollView = nil;
    self.scrollView = nil;
    self.portSizeUnitLabel = nil;
    self.portSizeLabel = nil;
    self.lengthUnitLabel = nil;
    self.diameterUnitLabel = nil;
    self.numPortsTextField = nil;
    self.lengthTextField = nil;
    self.diameterTextField = nil;
    
    [super viewDidUnload];
}

- (void)dealloc
{

    [rulerImageView release];
    [rulerScrollView release];
    [scrollView release];
    [portSizeUnitLabel release];
    [portSizeLabel release];
    [lengthUnitLabel release];
    [diameterUnitLabel release];
    [numPortsTextField release];
    [lengthTextField release];
    [diameterTextField release];
    
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if (isIpad) {
        
        return YES;
        
    } else {
        
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
        
    }
    
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    if (isIpad) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    } else {
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }

    activeField = textField;
    
    if (textField.tag == DIAMETER_TEXTFIELD) {

        diameter = 0;
        diameterTextField.text = nil;
        
        [self.lengthTextField setUserInteractionEnabled:NO];
        [self.numPortsTextField setUserInteractionEnabled:NO];
        
    } else if (textField.tag == LENGTH_TEXTFIELD) {

        length = 0;
        lengthTextField.text = nil;
        
        [self.diameterTextField setUserInteractionEnabled:NO];
        [self.numPortsTextField setUserInteractionEnabled:NO];
        
    } else if (textField.tag == NUM_PORTS_TEXTFIELD) {
        
        numPorts = 0;
        numPortsTextField.text = nil;
        
        [self.diameterTextField setUserInteractionEnabled:NO];
        [self.lengthTextField setUserInteractionEnabled:NO];
    }
    
    portSize = 0;
    rulerImage = nil;
    rulerImage = [UIImage imageNamed:@"475px-Mm_to_1_inch_fraction.png"];
    
    UIImage *lineImage = [self imageByDrawingLineOnImage:rulerImage];
    self.rulerImageView.image = nil;
    self.rulerImageView.image = lineImage;
    lineImage = nil;
    
    [portSizeLabel setAlpha:0.25];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    activeField = nil;
    
}
- (IBAction)updateFields:(id)sender
{
    
    [self resignAllResponders];
    [self enableTextfields];

    double baseDiameter = 0;
    double baseLength = 0;
    portSize = 0;
    
    if (!diameter) {
        
        diameter = [diameterTextField.text doubleValue];
        
    }
    
    if (!length) {
        
        length = [lengthTextField.text doubleValue];
        
    }
    
    if (!numPorts) {
        
        numPorts = [numPortsTextField.text intValue];

    }
        
    // get base units
    baseDiameter = [[diameterUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:diameter]] doubleValue];
    baseLength = [[lengthUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:length]] doubleValue];    
    
    Calculator *calculator = [[Calculator alloc] init];
    
    // If all values are there, calc port size
    if (diameter && length && numPorts) {
        
        portSize = [calculator calcPortSizeFromWidth:baseDiameter andLength:baseLength andNumberPorts:numPorts];
        
    }
    
    portSizeLabel.text = [NSString stringWithFormat:@"%.2f", [[portSizeUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:portSize] toUnit:portSizeUnit]floatValue]];
    
    
    [calculator release];
    
    rulerImage = nil;
    rulerImage = [UIImage imageNamed:@"475px-Mm_to_1_inch_fraction.png"];

    UIImage *lineImage = [self imageByDrawingLineOnImage:rulerImage];
    self.rulerImageView.image = nil;
    self.rulerImageView.image = lineImage;
    lineImage = nil;
    
    [portSizeLabel setAlpha:1.0];
    
    [self resetTextfieldDisplay];
    
}

-(IBAction) showEmailModalView:(id)sender 
{
    
    if([MFMailComposeViewController canSendMail]){
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        
        if (picker) {
            
            picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
            NSString *mailSubject = [NSString stringWithFormat:@"%@ Calculation", self.title];
            NSString *mailMsg = [self exportMsgHtml];
    
            [picker setSubject:mailSubject];
    
            [picker setMessageBody:mailMsg isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
    
            picker.navigationBar.barStyle = UIBarStyleDefault; // choose your style, unfortunately, Translucent colors behave quirky.
    
            //[self presentModalViewController:picker animated:YES];
            [self presentViewController:picker animated:YES completion:nil];
            [picker release];
            
        }
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Mail Accounts" message:@"Please set up a Mail account in order to send email." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        
    }
    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{ 
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-("
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
            
            break;
    }
    
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)exportButtonAction:(id)sender
{
    if (![UIPrintInteractionController isPrintingAvailable]) {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Email Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", nil];
        
    } else {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Export Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", @"Print", nil];
    }
    // Show the sheet
    [sheet showFromTabBar:self.tabBarController.tabBar];
    [sheet release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
   // NSLog(@"Button %d", buttonIndex);
    switch (buttonIndex) {
        case 0:
            [self showEmailModalView:nil];
            break;
            
        case 1:
            [self printCalculation:nil];
            break;
            
        default:
            break;
    }
}

#pragma mark -

#pragma mark Printing



// Print button handler that sends our print instructions to the OS

- (IBAction)printCalculation:(id)sender {
    
    // Get a reference to the singleton iOS printing concierge
    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    
    if(pic && [UIPrintInteractionController isPrintingAvailable]) {
        //  UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = self.title;
        pic.printInfo = printInfo;
        
        // NSString *msg = @"dsfs fs ff sdf asf asf safs";
        NSString *msg = [self exportMsgText];
        // NSLog(@"msg = %@", msg);
        UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc]
                                                     initWithText:msg];
        textFormatter.startPage = 0;
        textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
        textFormatter.maximumContentWidth = 6 * 72.0;
        pic.printFormatter = textFormatter;
        [textFormatter release];
        pic.showsPageRange = YES;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSLog(@"Printing could not complete because of error: %@", error);
            }
        };
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [pic presentFromRect:self.tabBarController.tabBar.frame inView:self.view animated:YES completionHandler:completionHandler];
        } else {
            [pic presentAnimated:YES completionHandler:completionHandler];
        }
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Printing Error" message:@"Your device doesn't support printing" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    
}

- (NSString *) exportMsgText
{
    NSString *title = self.title;
    
    NSString *diameterString = diameterTextField.text;
    NSString *diameterUnitString = diameterUnit.name;
    
    NSString *lengthString = lengthTextField.text;
    NSString *lengthUnitString = lengthUnit.name;
    
    NSString *numPortsString = numPortsTextField.text;
    
    NSString *portSizeString = portSizeLabel.text;
    NSString *portSizeUnitString = portSizeUnit.name;
    
    NSString *msg = [NSString stringWithFormat:@"%@\n\nElectronics Bay Dimensions\nInside Diameter: %@ (%@)\nLength: %@ (%@)\n\nNumber of Ports: %@\nPort Size (per port): %@ (%@)\n\n\nGenerated by Rocket Calculator\n\nAvailable on the Apple App Store",title, diameterString, diameterUnitString, lengthString, lengthUnitString, numPortsString, portSizeString, portSizeUnitString];
    
    return msg;

}

- (NSString *) exportMsgHtml
{
    NSString *title = self.title;
    
    NSString *diameterString = diameterTextField.text;
    NSString *diameterUnitString = diameterUnit.name;
    
    NSString *lengthString = lengthTextField.text;
    NSString *lengthUnitString = lengthUnit.name;
    
    NSString *numPortsString = numPortsTextField.text;
    
    NSString *portSizeString = portSizeLabel.text;
    NSString *portSizeUnitString = portSizeUnit.name;
    
    NSString *msg = [NSString stringWithFormat:@"%@<br /><br />Electronics Bay Dimensions<br />Inside Diameter: %@ (%@)<br />Length: %@ (%@)<br /><br />Number of Ports: %@<br />Port Size (per port): %@ (%@)<br /><br /><br />Generated by Rocket Calculator<br /><br />Available on the Apple <a href=\"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=484673201&mt=8\">App Store</a>",title, diameterString, diameterUnitString, lengthString, lengthUnitString, numPortsString, portSizeString, portSizeUnitString];
    
    return msg;

}

- (void)enableTextfields
{
    
    [diameterTextField setUserInteractionEnabled:YES];
    [lengthTextField setUserInteractionEnabled:YES];
    [numPortsTextField setUserInteractionEnabled:YES];
    
}

- (void) resignAllResponders
{
    
    [diameterTextField resignFirstResponder];
    [lengthTextField resignFirstResponder];
    [numPortsTextField resignFirstResponder];
    
}

- (void) resetTextfieldDisplay
{
    
    // Tint required fields
    
    if (diameterTextField.text == nil || [diameterTextField.text isEqualToString:@""] || [diameterTextField.text doubleValue] == 0) {
        
        self.diameterTextField.text = nil;
        [self.diameterTextField setRequired:YES];
    } else {
        
        [self.diameterTextField setRequired:NO];
    }
    
    if (lengthTextField.text == nil || [lengthTextField.text isEqualToString:@""] || [lengthTextField.text doubleValue] == 0) {
        
        self.lengthTextField.text = nil;
        [self.lengthTextField setRequired:YES];
    } else {
        
        [self.lengthTextField setRequired:NO];
    }
    
    if (numPortsTextField.text == nil || [numPortsTextField.text isEqualToString:@""] || [numPortsTextField.text doubleValue] == 0) {
        
        self.numPortsTextField.text = nil;
        [self.numPortsTextField setRequired:YES];
    } else {
        
        [self.numPortsTextField setRequired:NO];
    }
    
}

@end