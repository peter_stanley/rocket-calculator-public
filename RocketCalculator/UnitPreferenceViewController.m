//
//  UnitPreferenceViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 4/6/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import "UnitPreferenceViewController.h"
#import "UnitPreferenceTableViewCell.h"
#import "SettingSelectListViewController.h"
#import "RocketCalculatorAppDelegate.h"

#define SECTION_ALERT_PERF 0
#define SECTION_AIR_DENSITY 1
#define SECTION_ALT_CALC 2
#define SECTION_TVW 3
#define SECTION_DESCENT_RATE 4
#define SECTION_BP_VS_PRESSURE 5
#define SECTION_PRESSURE_PORT 6
#define SECTION_ALTITUDE_TRACKING 7

// Alert preferences section
#define AP_THRUST_TO_WEIGHT 0
#define AP_MACH 1

// Air Density section
#define AC_ELEVATION 0
#define AC_TEMPERATURE 1

// Alt Calcution Section
#define AC_TOTAL_IMPULSE 0
#define AC_AVG_THRUST 1
#define AC_BURN_TIME 2
#define AC_TOTAL_MOTOR_MASS 3
#define AC_PROPELLANT_MASS 4
#define AC_CASE_MASS 5
#define AC_ROCKET_MASS 6
#define AC_BODY_DIAMETER 7
#define AC_AREA 8
#define AC_VELOCITY 9
#define AC_ALTITUDE 10
#define AC_COAST_TIME 11

// Tvw Section
#define TVW_ROCKET_MASS 0
#define TVW_FORCE 1

// Descent Rate
#define DR_ROCKET_MASS 0
#define DD_ALTITUDE 1
#define DR_DESCENT_VELOCITY 2
#define DD_DESCENT_DURATION 3
#define DD_WIND_SPEED 4
#define DD_DRIFT_DISTANCE 5
#define DR_PARACHUTE_DIAMETER 6
#define DR_SPILL_HOLE_DIAMETER 7
#define DR_STREAMER_WIDTH 8
#define DR_STREAMER_LENGTH 9

// BP vs Pressure Section
#define BP_INSIDE_DIAMETER 0
#define BP_LENGTH 1
#define BP_PRESSURE 2
#define BP_FORCE 3
#define BP_MASS 4

// Pressure Port Section
#define PP_INSIDE_DIAMETER 0
#define PP_LENGTH 1
#define PP_PORT_DIAMETER 2

// Altitude Tracking Section
#define AT_ELEVATION_ANGLE 0
#define AT_AZIMUTH_ANGLE 1
#define AT_BASELINE_LENGTH 2
#define AT_FLIGHT_ALTITUDE 3

@implementation UnitPreferenceViewController

@synthesize unitPreferenceTableView, unitPreferenceTableViewCell, lowThrustSwitch, machSwitch, unitPreferenceArray, unitPreferenceDictionary;
@synthesize apThrustToWeightDict, apMachDict, acElevationList, acTemperatureList, acTotalImpulseList, acAvgThrustList, acBurnTimeList, acTotalMotorMassList,acPropellantMassList, acCaseMassList, acRocketMassList, acBodyDiameterList, acAreaList, acVelocityList, acAltitudeList, acCoastTimeList;
@synthesize tvwRocketMassList, tvwForceList, drRocketMassList, ddAltitudeList, drDescentVelocityList, ddDescentDurationList, ddWindSpeedList, ddDriftDistanceList, drParachuteDiameterList, drSpillHoleDiameterList, drStreamerWidthList, drStreamerLengthList;
@synthesize bpTubeWidthList, bpTubeLengthList, bpPressureList, bpForceList, bpMassList, spTubeWidthList, spTubeLengthList;
@synthesize spPortDiameterList, atElevationAngleList, atAzimuthAngleList,atBaselineLengthList, atFlightAltitudeList, popOverController;

- (void)dealloc
{
    [popOverController release];
    [atFlightAltitudeList release];
    [atBaselineLengthList release];
    [atAzimuthAngleList release];
    [atElevationAngleList release];
    [spPortDiameterList release];
    [spTubeLengthList release];
    [spTubeWidthList release];
    [bpMassList release];
    [bpForceList release];
    [bpPressureList release];
    [bpTubeLengthList release];
    [bpTubeWidthList release];
    [drStreamerLengthList release];
    [drStreamerWidthList release];
    [drSpillHoleDiameterList release];
    [drParachuteDiameterList release];
    [ddDriftDistanceList release];
    [ddWindSpeedList release];
    [ddDescentDurationList release];
    [drDescentVelocityList release];
    [ddAltitudeList release];
    [drRocketMassList release];
    [tvwForceList release];
    [tvwRocketMassList release];
    [acCoastTimeList release];
    [acAltitudeList release];
    [acVelocityList release];
    [acAreaList release];
    [acBodyDiameterList release];
    [acRocketMassList release];
    [acCaseMassList release];
    [acPropellantMassList release];
    [acTotalMotorMassList release];
    [acBurnTimeList release];
    [acAvgThrustList release];
    [acTotalImpulseList release];
    [acTemperatureList release];
    [acElevationList release];
    [apMachDict release];
    [apThrustToWeightDict release];
    
    [unitPreferenceDictionary release];
    [unitPreferenceArray release];
    [machSwitch release];
    [lowThrustSwitch release];
    [unitPreferenceTableViewCell release];
    [unitPreferenceTableView release];
    NSLog(@"dealloc seen");
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Releases the view if it doesn't have a superview.
  //  [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{

    [super viewDidLoad];
    self.title = @"App Settings";
    self.navigationController.navigationBar.translucent = NO;
    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];

    lowThrustSwitch = [[UISwitch alloc] init];
    machSwitch = [[UISwitch alloc] init];
    
    [self.lowThrustSwitch addTarget:self action:@selector(switchChanged:forEvent:) forControlEvents:(UIControlEventValueChanged | UIControlEventTouchDragInside)];
    
    [self.machSwitch addTarget:self action:@selector(switchChanged:forEvent:) forControlEvents:(UIControlEventValueChanged | UIControlEventTouchDragInside)];

    // Alerts
    apThrustToWeightDict = [[NSMutableDictionary alloc] init];
    apMachDict = [[NSMutableDictionary alloc] init];
    
    // Air Density
    acElevationList = [[NSMutableDictionary alloc] init];
    acTemperatureList = [[NSMutableDictionary alloc] init];

    // altitude prediction
    acTotalImpulseList = [[NSMutableDictionary alloc] init];
    acAvgThrustList = [[NSMutableDictionary alloc] init];
    acBurnTimeList = [[NSMutableDictionary alloc] init];
    acTotalMotorMassList = [[NSMutableDictionary alloc] init];
    acPropellantMassList = [[NSMutableDictionary alloc] init];
    acCaseMassList = [[NSMutableDictionary alloc] init];
    acRocketMassList = [[NSMutableDictionary alloc] init];
    acBodyDiameterList = [[NSMutableDictionary alloc] init];
    acAreaList = [[NSMutableDictionary alloc] init];
    acVelocityList = [[NSMutableDictionary alloc] init];
    acAltitudeList = [[NSMutableDictionary alloc] init];
    acCoastTimeList = [[NSMutableDictionary alloc] init];
    
    // thrust vs. weight
    tvwRocketMassList = [[NSMutableDictionary alloc]init];
    tvwForceList = [[NSMutableDictionary alloc] init];
    
    // descent rate and drift distance
    drRocketMassList = [[NSMutableDictionary alloc] init];
    ddAltitudeList = [[NSMutableDictionary alloc] init];
    drDescentVelocityList = [[NSMutableDictionary alloc] init];
    ddDescentDurationList = [[NSMutableDictionary alloc] init];
    ddWindSpeedList = [[NSMutableDictionary alloc] init];
    ddDriftDistanceList = [[NSMutableDictionary alloc] init];
    drParachuteDiameterList = [[NSMutableDictionary alloc] init];
    drSpillHoleDiameterList = [[NSMutableDictionary alloc] init];
    drStreamerWidthList = [[NSMutableDictionary alloc] init];
    drStreamerLengthList = [[NSMutableDictionary alloc] init];
    
    // ejection charge
    bpTubeWidthList = [[NSMutableDictionary alloc] init];
    bpTubeLengthList = [[NSMutableDictionary alloc] init];
    bpPressureList = [[NSMutableDictionary alloc] init];
    bpForceList = [[NSMutableDictionary alloc] init];
    bpMassList = [[NSMutableDictionary alloc] init];
    
    // static port size
    spTubeWidthList = [[NSMutableDictionary alloc] init];
    spTubeLengthList = [[NSMutableDictionary alloc] init];
    spPortDiameterList = [[NSMutableDictionary alloc] init];
    
    // station altitude tracking
    atElevationAngleList = [[NSMutableDictionary alloc] init];
    atAzimuthAngleList = [[NSMutableDictionary alloc] init];
    atBaselineLengthList = [[NSMutableDictionary alloc] init];
    atFlightAltitudeList = [[NSMutableDictionary alloc] init];
        
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *pListPath = [path stringByAppendingPathComponent:@"Settings.bundle/Root.plist"];
    
    NSDictionary *appPreferences = [NSDictionary dictionaryWithContentsOfFile:pListPath];
    NSMutableArray *prefsArray = [appPreferences objectForKey:@"PreferenceSpecifiers"];
    
    self.unitPreferenceArray = [NSMutableArray array];
    self.unitPreferenceDictionary = [NSMutableDictionary dictionary];
    
    for (NSDictionary *prefDict in prefsArray) {
        
        if ([prefDict objectForKey:@"Key"] && !([[prefDict objectForKey:@"Type"] isEqualToString:@"PSToggleSwitchSpecifier"])) {
            NSMutableDictionary *unitDict = [[NSMutableDictionary alloc] init];
            [unitDict setObject:[prefDict objectForKey:@"Key"] forKey:@"Key"];
            [unitDict setObject:[prefDict objectForKey:@"Title"] forKey:@"Title"];
            [unitDict setObject:[prefDict objectForKey:@"Titles"] forKey:@"Titles"];
            [unitDict setObject:[prefDict objectForKey:@"Values"] forKey:@"Values"];
            
            [self.unitPreferenceArray addObject:unitDict];
            [unitDict release];
        } else if([prefDict objectForKey:@"Key"] && [[prefDict objectForKey:@"Type"] isEqualToString:@"PSToggleSwitchSpecifier"]) {
            
            NSMutableDictionary *alertDict = [[NSMutableDictionary alloc] init];
            [alertDict setObject:[prefDict objectForKey:@"Key"] forKey:@"Key"];
            [alertDict setObject:[prefDict objectForKey:@"Title"] forKey:@"Title"];
            [self.unitPreferenceArray addObject:alertDict];
            [alertDict release];
            
        }
        [self.unitPreferenceTableView reloadData];
    }

    // Do any additional setup after loading the view from its nib.
    // iterate through unitPreferencesArray and build out selectList arrays with key/value pair dictionarys
    for (NSMutableDictionary *unitListDict in unitPreferenceArray)
    {
        // alerts setup
        if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"enableTWTAlert"]) {
            
            [self.apThrustToWeightDict setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"enableMachAlert"]) {
            
            [self.apMachDict setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            
        }
            
        // altitude prediction setup
        else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_elevation"]) {
            
            [self.acElevationList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acElevationList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acElevationList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }
        else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_temperature"]) {
            
            [self.acTemperatureList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acTemperatureList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acTemperatureList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }
        else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_total_impulse"]) {
            
            [self.acTotalImpulseList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acTotalImpulseList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acTotalImpulseList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_avg_thrust"]){
            
            [self.acAvgThrustList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acAvgThrustList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acAvgThrustList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_burn_time"]){
            
            [self.acBurnTimeList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acBurnTimeList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acBurnTimeList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_motor_mass"]){
            
            [self.acTotalMotorMassList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acTotalMotorMassList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acTotalMotorMassList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_prop_mass"]){
            
            [self.acPropellantMassList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acPropellantMassList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acPropellantMassList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_case_mass"]){
            
            [self.acCaseMassList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acCaseMassList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acCaseMassList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_rocket_mass"]){
            
            [self.acRocketMassList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acRocketMassList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acRocketMassList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_body_diameter"]){
            
            [self.acBodyDiameterList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acBodyDiameterList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acBodyDiameterList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_area"]){
            
            [self.acAreaList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acAreaList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acAreaList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_velocity"]){
            
            [self.acVelocityList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acVelocityList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acVelocityList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_altitude"]){
            
            [self.acAltitudeList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acAltitudeList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acAltitudeList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"alt_calc_coast_time"]){
            
            [self.acCoastTimeList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.acCoastTimeList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.acCoastTimeList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        // thrust vs. weight
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_tvw_rocket_mass"]) {

            [self.tvwRocketMassList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.tvwRocketMassList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.tvwRocketMassList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
        
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_tvw_thrust"]){
           
            [self.tvwForceList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.tvwForceList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.tvwForceList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];

        // descent rate and drift distance
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_rocket_mass"]){
            
            [self.drRocketMassList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.drRocketMassList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.drRocketMassList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];

        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_drift_altitude"]){
            
            [self.ddAltitudeList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.ddAltitudeList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.ddAltitudeList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];

        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_descent_velocity"]){

            [self.drDescentVelocityList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.drDescentVelocityList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.drDescentVelocityList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
        
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_drift_descent_time"]){
            
            [self.ddDescentDurationList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.ddDescentDurationList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.ddDescentDurationList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
       
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_drift_wind_speed"]){
            
            [self.ddWindSpeedList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.ddWindSpeedList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.ddWindSpeedList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
        
        } else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_drift_distance"]){
            
            [self.ddDriftDistanceList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.ddDriftDistanceList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.ddDriftDistanceList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_chute_diameter"]){

            [self.drParachuteDiameterList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.drParachuteDiameterList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.drParachuteDiameterList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
        
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_spill_hole_diameter"]){
            
            [self.drSpillHoleDiameterList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.drSpillHoleDiameterList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.drSpillHoleDiameterList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_streamer_diameter"]){

            [self.drStreamerWidthList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.drStreamerWidthList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.drStreamerWidthList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
           
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_streamer_length"]){

            [self.drStreamerLengthList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.drStreamerLengthList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.drStreamerLengthList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
         
        // ejection charge
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_bp_tube_width"]){

            [self.bpTubeWidthList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.bpTubeWidthList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.bpTubeWidthList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_bp_tube_length"]){

            [self.bpTubeLengthList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.bpTubeLengthList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.bpTubeLengthList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_bp_pressure"]){

            [self.bpPressureList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.bpPressureList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.bpPressureList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
 
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_bp_force"]){
            
            [self.bpForceList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.bpForceList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.bpForceList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];

        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_bp_amount"]){

            [self.bpMassList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.bpMassList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.bpMassList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        // pressure port size
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_pp_width"]){

            [self.spTubeWidthList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.spTubeWidthList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.spTubeWidthList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_pp_length"]){
            
            [self.spTubeLengthList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.spTubeLengthList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.spTubeLengthList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_pp_size"]){
            
            [self.spPortDiameterList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.spPortDiameterList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.spPortDiameterList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        // station alitude tracking
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_s_tracking_e_angle"]){
            
            [self.atElevationAngleList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.atElevationAngleList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.atElevationAngleList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_s_tracking_a_angle"]){
            
            [self.atAzimuthAngleList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.atAzimuthAngleList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.atAzimuthAngleList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_s_tracking_baseline"]){
            
            [self.atBaselineLengthList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.atBaselineLengthList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.atBaselineLengthList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }else if ([[unitListDict objectForKey:@"Key"] isEqualToString:@"calc_s_tracking_altitude"]){
            
            [self.atFlightAltitudeList setObject:[unitListDict objectForKey:@"Title"] forKey:@"Title"];
            [self.atFlightAltitudeList setObject:[unitListDict objectForKey:@"Titles"] forKey:@"Titles"];
            [self.atFlightAltitudeList setObject:[unitListDict objectForKey:@"Values"] forKey:@"Values"];
            
        }
        
        else{
            // Do nothing
        }
    }
}

- (void)viewDidUnload
{

    self.popOverController = nil;
    self.atFlightAltitudeList = nil;
    self.atBaselineLengthList = nil;
    self.atAzimuthAngleList = nil;
    self.atElevationAngleList = nil;
    self.spPortDiameterList = nil;
    self.spTubeLengthList = nil;
    self.spTubeWidthList = nil;
    self.bpMassList = nil;
    self.bpForceList = nil;
    self.bpPressureList = nil;
    self.bpTubeLengthList = nil;
    self.bpTubeWidthList = nil;
    self.drStreamerLengthList = nil;
    self.drStreamerWidthList = nil;
    self.drSpillHoleDiameterList = nil;
    self.drParachuteDiameterList = nil;
    self.ddDriftDistanceList = nil;
    self.ddWindSpeedList = nil;
    self.ddDescentDurationList = nil;
    self.drDescentVelocityList = nil;
    self.ddAltitudeList = nil;
    self.drRocketMassList = nil;
    self.tvwForceList = nil;
    self.tvwRocketMassList = nil;
    self.acCoastTimeList = nil;
    self.acAltitudeList = nil;
    self.acVelocityList = nil;
    self.acAreaList = nil;
    self.acBodyDiameterList = nil;
    self.acRocketMassList = nil;
    self.acCaseMassList = nil;
    self.acPropellantMassList = nil;
    self.acTotalMotorMassList = nil;
    self.acBurnTimeList = nil;
    self.acAvgThrustList = nil;
    self.acTotalImpulseList = nil;
    self.acTemperatureList = nil;
    self.acElevationList = nil;
    self.apMachDict = nil;
    self.apThrustToWeightDict = nil;
    self.unitPreferenceDictionary = nil;
    self.unitPreferenceArray = nil;
    self.machSwitch = nil;
    self.lowThrustSwitch = nil;
    self.unitPreferenceTableViewCell = nil;
    self.unitPreferenceTableView = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    [self.unitPreferenceTableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    [[NSUserDefaults standardUserDefaults] setBool:self.lowThrustSwitch.on forKey:@"enableTWTAlert"];
    [[NSUserDefaults standardUserDefaults] setBool:self.machSwitch.on forKey:@"enableMachAlert"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [super viewWillDisappear:YES];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - TableView methods
/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 51;
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
	return 7;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 
{

	switch (section) {
            
        case SECTION_ALERT_PERF:
            return @"Alerts";
            break;
            
        case SECTION_AIR_DENSITY:
            return @"Air Density";
            break;        
        
        case SECTION_ALT_CALC:
            return @"Altitude Prediction Units";
            break;
            
        case SECTION_TVW:
            return @"Thrust vs. Weight Units";
            break;
        
        case SECTION_DESCENT_RATE:
            return @"Descent Rate / Drift Distance Units";
            break;
            
        case SECTION_BP_VS_PRESSURE:
            return @"Ejection Charge Units";
            break;
            
        case SECTION_PRESSURE_PORT:
            return @"Static Pressure Port Size Units";
            break;
            
        case SECTION_ALTITUDE_TRACKING:
            return @"Altitude Tracking Units";
            break;
        
        default:
            return nil;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSInteger numberOfRows = 0;
    
	switch (section) {
            
        case SECTION_ALERT_PERF:
            numberOfRows = 2;
            break;
            
        case SECTION_AIR_DENSITY:
            numberOfRows = 2;
            break;
            
        case SECTION_ALT_CALC:
            numberOfRows = 12;
            break;
            
        case SECTION_TVW:
            numberOfRows = 2;
            break;
            
        case SECTION_DESCENT_RATE:
            numberOfRows = 10;
            break;
            
        case SECTION_BP_VS_PRESSURE:
            numberOfRows = 5;
            break;

        case SECTION_PRESSURE_PORT:
            numberOfRows = 3;
            break;

        case SECTION_ALTITUDE_TRACKING:
            numberOfRows = 4;
            break;
            
        default:
            numberOfRows = 1;
            break;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *bundleName = @"";
    
    if (isIpad) {
        bundleName = @"UnitPreferenceTableViewCell~ipad";
    } else {
        bundleName = @"UnitPreferenceTableViewCell";
    }

    static NSString *UnitPreferenceCellIdentifier = @"UnitPreference";
	static int i = 0;
    UnitPreferenceTableViewCell  *cell = (UnitPreferenceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:UnitPreferenceCellIdentifier];
    if (cell == nil) {
		//[[NSBundle mainBundle] loadNibNamed:@"UnitPreferenceTableViewCell" owner:self options:nil];
        [[NSBundle mainBundle] loadNibNamed:bundleName owner:self options:nil];

        cell = unitPreferenceTableViewCell;
		self.unitPreferenceTableViewCell = nil;
    }
    
    switch (indexPath.section) {
        
        case SECTION_ALERT_PERF:
        {
            switch (indexPath.row) {
                case AP_THRUST_TO_WEIGHT:
                {
                    cell.unitLabel.text = [apThrustToWeightDict objectForKey:@"Title"];
                    cell.preferenceLabel.text = nil;
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    self.lowThrustSwitch.on = [[NSUserDefaults standardUserDefaults]boolForKey:@"enableTWTAlert"];
                    UIView *lowThrustSwitchView = [[UIView alloc] initWithFrame:self.lowThrustSwitch.frame];
                    cell.accessoryView = lowThrustSwitchView;
                    [cell.accessoryView addSubview:self.lowThrustSwitch];
                    [lowThrustSwitchView release];
                    

                }   
                    break;
                    
                case AP_MACH:
                {
                    cell.unitLabel.text = [apMachDict objectForKey:@"Title"];
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.preferenceLabel.text = nil;
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    self.machSwitch.on = [[NSUserDefaults standardUserDefaults]boolForKey:@"enableMachAlert"];
                    UIView *machSwitchView = [[UIView alloc] initWithFrame:self.machSwitch.frame];
                    cell.accessoryView = machSwitchView;
                    [cell.accessoryView addSubview:self.machSwitch];
                    [machSwitchView release];
                }
                    
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case SECTION_AIR_DENSITY:
        {
            switch (indexPath.row) {
                    
                case AC_ELEVATION:
                    //
                    cell.unitLabel.text = [acElevationList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;
                    
                    i = 0;
                    for (NSString *unitVal in [acElevationList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_elevation"]]) {
                            
                            cell.preferenceLabel.text = [[acElevationList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    break;
                    
                case AC_TEMPERATURE:
                    //
                    cell.unitLabel.text = [acTemperatureList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;
                    
                    i = 0;
                    for (NSString *unitVal in [acTemperatureList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_temperature"]]) {
                            
                            cell.preferenceLabel.text = [[acTemperatureList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    break;
                    
                default:
                    break;
            }
        }
            break;
    
        case SECTION_ALT_CALC:
        {
            switch (indexPath.row) {

                case AC_TOTAL_IMPULSE:
                    //
                    cell.unitLabel.text = [acTotalImpulseList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [acTotalImpulseList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_total_impulse"]]) {
                            
                            cell.preferenceLabel.text = [[acTotalImpulseList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    break;
                    
                case AC_AVG_THRUST:
                    //
                    cell.unitLabel.text = [acAvgThrustList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [acAvgThrustList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_avg_thrust"]]) {
                            
                            cell.preferenceLabel.text = [[acAvgThrustList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                case AC_BURN_TIME:
                    //
                    cell.unitLabel.text = [acBurnTimeList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [acBurnTimeList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_burn_time"]]) {
                            
                            cell.preferenceLabel.text = [[acBurnTimeList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case AC_TOTAL_MOTOR_MASS:
                    //
                    cell.unitLabel.text = [acTotalMotorMassList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [acTotalMotorMassList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_motor_mass"]]) {
                            
                            cell.preferenceLabel.text = [[acTotalMotorMassList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case AC_PROPELLANT_MASS:
                    //
                    cell.unitLabel.text = [acPropellantMassList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [acPropellantMassList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_prop_mass"]]) {
                            
                            cell.preferenceLabel.text = [[acPropellantMassList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case AC_CASE_MASS:
                    //
                    cell.unitLabel.text = [acCaseMassList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [acCaseMassList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_case_mass"]]) {
                            
                            cell.preferenceLabel.text = [[acCaseMassList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case AC_ROCKET_MASS:
                    //
                    cell.unitLabel.text = [acRocketMassList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [acRocketMassList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_rocket_mass"]]) {
                            
                            cell.preferenceLabel.text = [[acRocketMassList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case AC_BODY_DIAMETER:
                    //
                    cell.unitLabel.text = [acBodyDiameterList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;
                    
                    i = 0;
                    for (NSString *unitVal in [acBodyDiameterList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_body_diameter"]]) {
                            
                            cell.preferenceLabel.text = [[acBodyDiameterList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case AC_AREA:
                    //
                    cell.unitLabel.text = [acAreaList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [acAreaList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_area"]]) {
                            
                            cell.preferenceLabel.text = [[acAreaList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case AC_VELOCITY:
                    //
                    cell.unitLabel.text = [acVelocityList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [acVelocityList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_velocity"]]) {
                            
                            cell.preferenceLabel.text = [[acVelocityList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case AC_ALTITUDE:
                    //
                    cell.unitLabel.text = [acAltitudeList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [acAltitudeList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_altitude"]]) {
                            
                            cell.preferenceLabel.text = [[acAltitudeList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case AC_COAST_TIME:
                    //
                    cell.unitLabel.text = [acCoastTimeList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [acCoastTimeList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_coast_time"]]) {
                            
                            cell.preferenceLabel.text = [[acCoastTimeList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                    
                default:
                    break;
            }
            
        }
            //
            break;

        case SECTION_TVW:
        {
            switch (indexPath.row) {
                case TVW_ROCKET_MASS:
                    //
                    cell.unitLabel.text = [tvwRocketMassList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [tvwRocketMassList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_tvw_rocket_mass"]]) {

                            cell.preferenceLabel.text = [[tvwRocketMassList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    break;
                    
                case TVW_FORCE:
                    //
                    cell.unitLabel.text = [tvwForceList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [tvwForceList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_tvw_thrust"]]) {
                            
                            cell.preferenceLabel.text = [[tvwForceList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                default:
                    break;
            }
            
        }
            //
            break;
            
        case SECTION_DESCENT_RATE:
        {
            switch (indexPath.row) {
                case DR_ROCKET_MASS:
                    //
                    cell.unitLabel.text = [drRocketMassList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [drRocketMassList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_rocket_mass"]]) {
                            
                            cell.preferenceLabel.text = [[drRocketMassList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case DD_ALTITUDE:
                    //
                    cell.unitLabel.text = [ddAltitudeList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [ddAltitudeList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_drift_altitude"]]) {
                            
                            cell.preferenceLabel.text = [[ddAltitudeList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case DR_DESCENT_VELOCITY:
                    //
                    cell.unitLabel.text = [drDescentVelocityList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [drDescentVelocityList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_descent_velocity"]]) {
                            
                            cell.preferenceLabel.text = [[drDescentVelocityList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                case DD_DESCENT_DURATION:
                    //
                    cell.unitLabel.text = [ddDescentDurationList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [ddDescentDurationList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_drift_descent_time"]]) {
                            
                            cell.preferenceLabel.text = [[ddDescentDurationList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case DD_WIND_SPEED:
                    //
                    cell.unitLabel.text = [ddWindSpeedList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [ddWindSpeedList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_drift_wind_speed"]]) {
                            
                            cell.preferenceLabel.text = [[ddWindSpeedList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case DD_DRIFT_DISTANCE:
                    //
                    cell.unitLabel.text = [ddDriftDistanceList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [ddDriftDistanceList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_drift_distance"]]) {
                            
                            cell.preferenceLabel.text = [[ddDriftDistanceList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;


                case DR_PARACHUTE_DIAMETER:
                    //
                    cell.unitLabel.text = [drParachuteDiameterList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [drParachuteDiameterList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_chute_diameter"]]) {
                            
                            cell.preferenceLabel.text = [[drParachuteDiameterList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case DR_SPILL_HOLE_DIAMETER:
                    //
                    cell.unitLabel.text = [drSpillHoleDiameterList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [drSpillHoleDiameterList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_spill_hole_diameter"]]) {
                            
                            cell.preferenceLabel.text = [[drSpillHoleDiameterList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case DR_STREAMER_WIDTH:
                    //
                    cell.unitLabel.text = [drStreamerWidthList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [drStreamerWidthList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_streamer_diameter"]]) {
                            
                            cell.preferenceLabel.text = [[drStreamerWidthList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                
                case DR_STREAMER_LENGTH:
                    //
                    cell.unitLabel.text = [drStreamerLengthList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [drStreamerLengthList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_streamer_length"]]) {
                            
                            cell.preferenceLabel.text = [[drStreamerLengthList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                    
  
                default:
                    break;
            }
        }
            break;
            
        case SECTION_BP_VS_PRESSURE:
        {
            switch (indexPath.row) {
                case BP_INSIDE_DIAMETER:
                    //
                    cell.unitLabel.text = [bpTubeWidthList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [bpTubeWidthList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_bp_tube_width"]]) {
                            
                            cell.preferenceLabel.text = [[bpTubeWidthList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                case BP_LENGTH:
                    //
                    cell.unitLabel.text = [bpTubeLengthList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [bpTubeLengthList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_bp_tube_length"]]) {
                            
                            cell.preferenceLabel.text = [[bpTubeLengthList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                case BP_PRESSURE:
                    //
                    cell.unitLabel.text = [bpPressureList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [bpPressureList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_bp_pressure"]]) {
                            
                            cell.preferenceLabel.text = [[bpPressureList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
 
                case BP_FORCE:
                    //
                    cell.unitLabel.text = [bpForceList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [bpForceList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_bp_force"]]) {
                            
                            cell.preferenceLabel.text = [[bpForceList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;

                case BP_MASS:
                    //
                    cell.unitLabel.text = [bpMassList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [bpMassList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_bp_amount"]]) {
                            
                            cell.preferenceLabel.text = [[bpMassList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                    
                default:
                    break;
            }
        }
            break;    
            
        case SECTION_PRESSURE_PORT:
        {
            switch (indexPath.row) {
                case PP_INSIDE_DIAMETER:
                    //
                    cell.unitLabel.text = [spTubeWidthList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [spTubeWidthList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_pp_width"]]) {
                            
                            cell.preferenceLabel.text = [[spTubeWidthList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                case PP_LENGTH:
                    //
                    cell.unitLabel.text = [spTubeLengthList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [spTubeLengthList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_pp_length"]]) {
                            
                            cell.preferenceLabel.text = [[spTubeLengthList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                case PP_PORT_DIAMETER:
                    //
                    cell.unitLabel.text = [spPortDiameterList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [spPortDiameterList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_pp_size"]]) {
                            
                            cell.preferenceLabel.text = [[spPortDiameterList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                default:
                    break;
            }
        }
            break;    

        case SECTION_ALTITUDE_TRACKING:
        {
            switch (indexPath.row) {
                case AT_ELEVATION_ANGLE:
                    //
                    cell.unitLabel.text = [atElevationAngleList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [atElevationAngleList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_s_tracking_e_angle"]]) {
                            
                            cell.preferenceLabel.text = [[atElevationAngleList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                case AT_AZIMUTH_ANGLE:
                    //
                    cell.unitLabel.text = [atAzimuthAngleList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [atAzimuthAngleList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_s_tracking_a_angle"]]) {
                            
                            cell.preferenceLabel.text = [[atAzimuthAngleList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                case AT_BASELINE_LENGTH:
                    //
                    cell.unitLabel.text = [atBaselineLengthList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [atBaselineLengthList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_s_tracking_baseline"]]) {
                            
                            cell.preferenceLabel.text = [[atBaselineLengthList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                case AT_FLIGHT_ALTITUDE:
                    //
                    cell.unitLabel.text = [atFlightAltitudeList objectForKey:@"Title"];
                    cell.accessoryView = nil;
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    cell.selectionStyle = UITableViewCellSelectionStyleGray;

                    i = 0;
                    for (NSString *unitVal in [atFlightAltitudeList objectForKey:@"Values"])
                    {
                        
                        if ([(NSString *)unitVal isEqualToString:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_s_tracking_altitude"]]) {
                            
                            cell.preferenceLabel.text = [[atFlightAltitudeList objectForKey:@"Titles"] objectAtIndex:i];
                        }
                        i++;
                    }
                    
                    break;
                    
                    
                default:
                    break;
            }
        }
            break;    

            
        default:
            break;
    }    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static UIViewController *nextController = nil;
    
    switch (indexPath.section) {
        case SECTION_ALERT_PERF:
        {
            
            switch (indexPath.row) {
                case AP_THRUST_TO_WEIGHT:
                    nextController = nil;
                    // Do nothing
                    break;
                    
                case AP_MACH:
                    nextController = nil;
                    // Do nothing
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
 
        case SECTION_AIR_DENSITY:
        {
            
            switch (indexPath.row) {
                case AC_ELEVATION:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acElevationList;
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_elevation";
                }
                    break;
                    
                case AC_TEMPERATURE:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acTemperatureList;
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_temperature";
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;

        case SECTION_ALT_CALC:
        {
            switch (indexPath.row) {

                case AC_TOTAL_IMPULSE:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acTotalImpulseList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_total_impulse";
                }
                    break;
                    
                case AC_AVG_THRUST:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acAvgThrustList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_avg_thrust";
                }
                    break;
                    
                case AC_BURN_TIME:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acBurnTimeList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_burn_time";
                }
                    break;
                    
                case AC_TOTAL_MOTOR_MASS:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acTotalMotorMassList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_motor_mass";
                }
                    break;
                    
                case AC_PROPELLANT_MASS:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acPropellantMassList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_prop_mass";
                }
                    break;
                    
                case AC_CASE_MASS:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acCaseMassList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_case_mass";
                }
                    break;
                    
                case AC_ROCKET_MASS:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acRocketMassList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_rocket_mass";
                }
                    break;
                    
                case AC_BODY_DIAMETER:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acBodyDiameterList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_body_diameter";
                }
                    break;
                    
                case AC_AREA:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acAreaList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_area";
                }
                    break;
                    
                case AC_VELOCITY:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acVelocityList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_velocity";
                }
                    break;
                    
                case AC_ALTITUDE:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acAltitudeList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_altitude";
                }
                    break;
                    
                case AC_COAST_TIME:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.acCoastTimeList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"alt_calc_coast_time";
                }
                    break;

                    
                default:
                    break;
            }
        }
            break;

        case SECTION_TVW:
        {
            switch (indexPath.row) {
                case TVW_ROCKET_MASS:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.tvwRocketMassList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_tvw_rocket_mass";
                }
                    break;
                    
                case TVW_FORCE:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.tvwForceList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_tvw_thrust";
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case SECTION_DESCENT_RATE:
        {
            switch (indexPath.row) {
                case DR_ROCKET_MASS: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.drRocketMassList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_rocket_mass";
                }
                    break;
                    
                case DD_ALTITUDE: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.ddAltitudeList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_drift_altitude";
                }
                    break;

                    
                case DR_DESCENT_VELOCITY: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.drDescentVelocityList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_descent_velocity";
                }
                    break;
                    
                case DD_DESCENT_DURATION: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.ddDescentDurationList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_drift_descent_time";
                }
                    break;

                case DD_WIND_SPEED: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.ddWindSpeedList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_drift_wind_speed";
                }
                    break;

                case DD_DRIFT_DISTANCE: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.ddDriftDistanceList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_drift_distance";
                }
                    break;

                    
                case DR_PARACHUTE_DIAMETER: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.drParachuteDiameterList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_chute_diameter";
                }
                    break;
                    
                case DR_SPILL_HOLE_DIAMETER: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.drSpillHoleDiameterList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_spill_hole_diameter";
                }
                    break;

                    
                case DR_STREAMER_WIDTH: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.drStreamerWidthList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_streamer_diameter";
                }
                    break;

                case DR_STREAMER_LENGTH:
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.drStreamerLengthList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_streamer_length";
                }
                    break;

                default:
                    break;
            }
        }
            break;
            
        case SECTION_BP_VS_PRESSURE:
        {
            switch (indexPath.row) {
                
                case BP_INSIDE_DIAMETER: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.bpTubeWidthList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_bp_tube_width";
                }
                    break;
                    
                case BP_LENGTH: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.bpTubeLengthList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_bp_tube_length";
                }
                    break;
                    
                case BP_PRESSURE: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.bpPressureList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_bp_pressure";
                }
                    break;

                case BP_FORCE: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.bpForceList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_bp_force";
                }
                    break;

                case BP_MASS: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.bpMassList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_bp_amount";
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
     
        case SECTION_PRESSURE_PORT:
        {
            switch (indexPath.row) {
                    
                case PP_INSIDE_DIAMETER: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.spTubeWidthList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_pp_width";
                }
                    break;
                    
                case PP_LENGTH: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.spTubeLengthList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_pp_length";
                }
                    break;
                    
                case PP_PORT_DIAMETER: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.spPortDiameterList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_pp_size";
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;

        case SECTION_ALTITUDE_TRACKING:
        {
            switch (indexPath.row) {
                    
                case AT_ELEVATION_ANGLE: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.atElevationAngleList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_s_tracking_e_angle";
                }
                    break;
                    
                case AT_AZIMUTH_ANGLE: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.atAzimuthAngleList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_s_tracking_a_angle";
                }
                    break;
                    
                case AT_BASELINE_LENGTH: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.atBaselineLengthList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_s_tracking_baseline";
                }
                    break;
                    
                case AT_FLIGHT_ALTITUDE: 
                {
                    nextController = [[SettingSelectListViewController alloc] initWithStyle:UITableViewStyleGrouped];
                    ((SettingSelectListViewController *)nextController).listDict = self.atFlightAltitudeList;           
                    ((SettingSelectListViewController *)nextController).settingKey = @"calc_s_tracking_altitude";
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;

            
        default:
            break;
    }
    
    if (nextController != nil) {
        
        if (isIpad)
        {
            
            // The device is an iPad running iPhone 3.2 or later.
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:nextController];
            
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
            [navigationController release];
            self.popOverController = popover;          
            popOverController.delegate = self;
            
            CGRect rect = [tableView convertRect:[tableView rectForRowAtIndexPath:indexPath]
                                          toView:tableView];
            
            [popOverController presentPopoverFromRect:rect 
                                               inView:self.view
                             permittedArrowDirections:UIPopoverArrowDirectionAny
                                             animated:YES];
            [popover release];
            
        }
        else
        {
            // The device is an iPhone or iPod touch.
            
            [self.navigationController pushViewController:nextController animated:YES];
        }
        
        [nextController release]; 
    }
    
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark -
#pragma mark UIPopOverController delegate methods

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    [self.unitPreferenceTableView reloadData];
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

- (void) switchChanged:(id)sender forEvent:(UIEvent *)event {
  
    [[NSUserDefaults standardUserDefaults] setBool:self.lowThrustSwitch.on forKey:@"enableTWTAlert"];
    [[NSUserDefaults standardUserDefaults] setBool:self.machSwitch.on forKey:@"enableMachAlert"];
       
    [[NSUserDefaults standardUserDefaults] synchronize];

}


@end
