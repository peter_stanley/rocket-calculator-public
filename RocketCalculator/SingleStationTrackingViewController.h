//
//  SingleStationTrackingViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

@class RAUnit;
@class CalcTextfield;

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface SingleStationTrackingViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIPrintInteractionControllerDelegate> 
{
    bool isIpad;
    
    double elevation1;
    double baselineLength;
    double alt1;
    
    CalcTextfield *elevation1Textfield;
    CalcTextfield *baselineLengthTextfield;
    
    UILabel *elevation1UnitLabel;
    UILabel *baselineLengthUnitLabel;
    UILabel *alt1Label;
    UILabel *alt1UnitLabel;
    
    RAUnit *elevationAngleUnit;
    RAUnit *baselineLengthUnit;
    RAUnit *altitudeUnit;
    
    UIScrollView *scrollView;
    // Rocket *rocket;
    // FlightLog *flightLog;
    
    UITextField *activeField;
    UIActionSheet *sheet;

}
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet CalcTextfield *elevation1Textfield;
@property (nonatomic, retain) IBOutlet CalcTextfield *baselineLengthTextfield;

@property (nonatomic, retain) IBOutlet UILabel *elevation1UnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *baselineLengthUnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *alt1Label;
@property (nonatomic, retain) IBOutlet UILabel *alt1UnitLabel;

- (IBAction)updateFields:(id)sender;
- (IBAction)infoButtonAction:(id)sender;
- (IBAction) exportButtonAction:(id)sender;
- (IBAction) showEmailModalView:(id)sender;
- (IBAction) printCalculation:(id)sender;
- (NSString *) exportMsgText;
- (NSString *) exportMsgHtml;
- (void) enableTextfields;
- (void) resignAllResponders;
- (void) resetTextfieldDisplay;

@end
