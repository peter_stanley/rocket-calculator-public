//
//  SingleSelectListViewCell.m
//  RocketCalculator
//
//  Created by Peter Stanley on 3/25/12.
//  Copyright 2012 Peter Stanley. All rights reserved.
//

#import "SingleSelectListViewCell.h"

@implementation SingleSelectListViewCell

@synthesize label, labelDetail; 

- (void)dealloc
{
    [label release];
    [labelDetail release];
    
    [super dealloc];
}

@end
