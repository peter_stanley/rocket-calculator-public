//
//  QuickReferenceRootViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 11/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "QuickReferenceRootViewController.h"
#import "GenericInfoViewController.h"
//#import "MotorBrowserViewController.h"
#import "RocketCalculatorAppDelegate.h"

@implementation QuickReferenceRootViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return 6; change after adding tra back
    //return 5;
    return 4;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    // Configure the cell...
    
    switch (indexPath.row) {
       // case 0:
      //      cell.textLabel.text = @"Motor Browser";
      //      break;

        case 0:
            cell.textLabel.text = @"Impulse Ranges";
            break;
            
        case 1:
            cell.textLabel.text = @"Drill Bit Gauge Conversion";
            break;
        
        case 2:
            cell.textLabel.text = @"NAR Model Rocket Safety Code";
            break;
            
        case 3:
            cell.textLabel.text = @"NAR High-Power Rocketry Safety Code";
            break;
            
      /*  case 4:
            cell.textLabel.text = @"Tripoli High Power Safety Code";
            break;
        */                
        default:
            break;
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // static UIViewController *nextController = nil;
    static GenericInfoViewController *infoView = nil;
   // MotorBrowserViewController *mv = nil;
    
    switch (indexPath.row) 
    {
       /* case 0:
        {
            mv = [[[MotorBrowserViewController alloc] init] autorelease];
            //mv.managedObjectContext = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] managedObjectContext];
            [self.navigationController pushViewController:mv animated:YES];
        }
            
            break;
*/
        case 0:
        {
            infoView = [[GenericInfoViewController alloc] init];
            infoView.htmlFileName = @"impulseRanges";

        }
            
            break;
            
        case 1:
        {
            infoView = [[GenericInfoViewController alloc] init];
            infoView.htmlFileName = @"drillSizes";
            
        }
            
            break;
            
        case 2:
        {
            infoView = [[GenericInfoViewController alloc] init];
            infoView.htmlFileName = @"NARModelRocketSafetyCode";
           
        }
            
            break;
            
        case 3:
        {
            infoView = [[GenericInfoViewController alloc] init];
            infoView.htmlFileName = @"NARHighPowerRocketrySafetyCode";
            
        }//TRAHighPowerSafetyCode
            
            break;
        /*
        case 4:
        {
            infoView = [[GenericInfoViewController alloc] init];
            infoView.htmlFileName = @"TRAHighPowerSafetyCode";
            
        }
            
            break;
*/
            
        default:
            
            break;
            
    }
    
    
    [self.navigationController pushViewController:infoView animated:YES];
	[infoView release];
    
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)dealloc
{

    [super dealloc];
    
}
@end
