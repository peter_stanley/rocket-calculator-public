//
//  RAUnit.m
//  RocketCalculator
//
//  Created by Peter Stanley on 5/1/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import "RAUnit.h"

#define kUnitDefaultMask @"UnitDefaults_%@"


@implementation RAUnit

@synthesize dimension, name, label, baseUnit, toBaseFactor, roundingBehavior, minPlausibleRange, maxPlausibleRange, lastValues;
@synthesize nonBase10Multiplier, leftLabel, rightLabel;


- (id) initWithDict:(NSDictionary *)dict ofDimension:(NSString *)myDimension;
{
	self = [super init];
	if(self) {
		self.dimension = myDimension;
		self.name = [dict valueForKey:@"name"];
		self.toBaseFactor = [NSDecimalNumber decimalNumberWithDecimal:[[dict valueForKey:@"toBaseFactor"] decimalValue]];
		
		self.nonBase10Multiplier = [dict valueForKey:@"nonBase10Multiplier"] ? [[dict valueForKey:@"nonBase10Multiplier"] intValue] : 0;
		self.label = [dict valueForKey:@"label"];
		
		// rounding scale
		short precision = 10;
		if([dict valueForKey:@"precision"]) {
			precision = [[dict valueForKey:@"precision"] intValue];
		}
		self.roundingBehavior = [[[NSDecimalNumberHandler alloc] initWithRoundingMode:NSRoundPlain scale:precision raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO] autorelease];
		
	}
	
	return self;
}

- (void) dealloc
{
    [dimension release];
    [name release];
   
    [label release];
    [leftLabel release];
    [rightLabel release];
   
    [baseUnit release];
    [toBaseFactor release];
    [roundingBehavior release];
    [minPlausibleRange release];
    [maxPlausibleRange release];
    [lastValues release];
	
	[super dealloc];
}
#pragma mark -



#pragma mark KVC
- (NSString *) label
{
	return label;
}
- (void) setLabel:(NSString *)newLabel
{
	if(label != newLabel) {
		[label release];
		label = [newLabel retain];
		
		if(nil != label) {
			if(nonBase10Multiplier > 0) {
				NSArray *lbls = [label componentsSeparatedByString:@"+"];
				self.leftLabel = [lbls objectAtIndex:0];
				self.rightLabel = [lbls objectAtIndex:1];
			}
		}
	}
}
#pragma mark -



#pragma mark Conversions
- (NSDecimalNumber *) convertToBaseUnit:(NSDecimalNumber *)value
{
   // NSLog(@"convert to baseunit was run");
   // NSLog(@"value passed in was %f",[value floatValue]);
  //  NSLog(@"self.name = %@",self.name);
  //  NSLog(@"self.baseunit.name = %@",self.baseUnit.name);
   // NSDecimalNumber *valDN = [[[NSDecimalNumber alloc]initWithDecimal:[value decimalValue]]autorelease];
    NSDecimalNumber *baseValDN = [NSDecimalNumber decimalNumberWithDecimal:[value decimalValue]];

	if(!baseValDN || [baseValDN isEqualToNumber:[NSDecimalNumber notANumber]]) {
		return nil;
	}
	
	if(self == self.baseUnit) {
		return baseValDN;
	}
	
	if([self.toBaseFactor isEqualToNumber:[NSDecimalNumber notANumber]]) {
		return nil;
	}
	
    if ([self.dimension isEqual: @"temperature"]) {
        if ([self.name isEqualToString:@"F"] && [self.baseUnit.name isEqualToString:@"C"])           
        {
            // convert farhenheit to celsius
            float valFloat = [baseValDN floatValue];
            valFloat = (5*(valFloat-32)/9);
            NSDecimalNumber *resDN = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:valFloat] decimalValue]];
            return resDN;
            
        } else {
            // convert celsius to farhenheit
            float valFloat = [baseValDN floatValue];
            valFloat = (9*valFloat/5)+32;
            NSDecimalNumber *resDN = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:valFloat] decimalValue]];
            return resDN;

        }
    }

    
	return [baseValDN decimalNumberByMultiplyingBy:self.toBaseFactor];
}

- (NSDecimalNumber *) convertValue:(NSDecimalNumber *)value toUnit:(RAUnit *)unit
{

    NSDecimalNumber *valDN = [NSDecimalNumber decimalNumberWithDecimal:[value decimalValue]];

    
	if(!valDN || [valDN isEqualToNumber:[NSDecimalNumber notANumber]]) {
		return nil;
	}

	// no or same unit
	if(!unit || self == unit) {
		return valDN;
	}
	
	// a shortcut
	if(unit == self.baseUnit) {
		return [self convertToBaseUnit:valDN];
	}
	
	// prevent errors
	if([toBaseFactor isEqualToNumber:[NSDecimalNumber notANumber]] ||
	   (self.baseUnit != unit.baseUnit) ||
	   [unit.toBaseFactor isEqualToNumber:[NSDecimalNumber notANumber]]) {
		return nil;
	}

	// calculate
    if ([self.dimension isEqual: @"temperature"]) {

        if ([self.baseUnit.name isEqualToString:@"C"] && [unit.name isEqualToString:@"F"])           
        {
            // convert celsius to farhenheit
            float valFloat = [valDN floatValue];
            valFloat = (9*valFloat/5)+32;
            NSDecimalNumber *resDN = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:valFloat] decimalValue]];
            return resDN;
        } else {
            // convert farhenheit to celsius
            float valFloat = [valDN floatValue];
            valFloat = (5*(valFloat-32)/9);
            NSDecimalNumber *resDN = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:valFloat] decimalValue]];
            return resDN;
        }
    } 
    
	NSDecimalNumber *multiplicationFactor = [self.toBaseFactor decimalNumberByDividingBy:unit.toBaseFactor];
    
	return [valDN decimalNumberByMultiplyingBy:multiplicationFactor];

}



// this is the same as convertValue:toUnit:, but rounds according to the target-units preference
- (NSDecimalNumber *) roundValue:(NSDecimalNumber *)value toUnit:(RAUnit *)unit
{
    
	if(!value || [value isEqualToNumber:[NSDecimalNumber notANumber]]) {
		return nil;
	}
	
	// no or same unit
	if(!unit || self == unit) {
		return [value decimalNumberByRoundingAccordingToBehavior:roundingBehavior];
	}
	
	// a shortcut
	if(unit == self.baseUnit) {
		return [[self convertToBaseUnit:value] decimalNumberByRoundingAccordingToBehavior:unit.roundingBehavior];
	}
	
	// prevent errors
	if([toBaseFactor isEqualToNumber:[NSDecimalNumber notANumber]] ||
	   (self.baseUnit != unit.baseUnit) ||
	   [unit.toBaseFactor isEqualToNumber:[NSDecimalNumber notANumber]]) {
		return nil;
	}
	
	// calculate
	NSDecimalNumber *baseValue = [self convertToBaseUnit:value];												// to base unit
	NSDecimalNumber *fromBaseFactor = [[NSDecimalNumber one] decimalNumberByDividingBy:unit.toBaseFactor];		// from base unit to new unit
	
	return [baseValue decimalNumberByMultiplyingBy:fromBaseFactor withBehavior:unit.roundingBehavior];			// rounded according to target unit
}


// this method returns YES if the value is in the defined physiologic range
- (BOOL) isPlausibleValue:(NSDecimalNumber *)value
{
	if(self.maxPlausibleRange || self.minPlausibleRange) {
		NSDecimalNumber *baseValue = [self convertToBaseUnit:value];
		BOOL overflow = NO;
		BOOL underflow = NO;
		if(maxPlausibleRange) {
			overflow = (NSOrderedDescending == [baseValue compare:maxPlausibleRange]);
		}
		if(minPlausibleRange) {
			underflow = (NSOrderedAscending == [baseValue compare:minPlausibleRange]);
		}
		
		return (!overflow && !underflow);
	}
	
	return YES;
}
#pragma mark -



#pragma mark Default Unit and Last Value Saving/Getting
- (BOOL) setLastValue:(NSDecimalNumber *)value forSpecialKey:(NSString *)specialKey
{
	if(baseUnit != self) {
		return [baseUnit setLastValue:value forSpecialKey:specialKey];
	}
	
	NSString *key = (nil == specialKey) ? @"default" : specialKey;
	NSMutableDictionary *valDict = lastValues ? lastValues : [NSMutableDictionary dictionary];
	BOOL saved = NO;
	
	if(nil != value) {
		[valDict setObject:value forKey:key];
		saved = YES;
	}
	else {
		[valDict removeObjectForKey:key];
	}
	
	self.lastValues = valDict;
	return saved;
}

- (NSDecimalNumber *) lastValueForSpecialKey:(NSString *)specialKey
{
	if(baseUnit != self) {
		return [baseUnit lastValueForSpecialKey:specialKey];
	}
	
	NSString *key = (nil == specialKey) ? @"default" : specialKey;
	if(nil != lastValues) {
		return [lastValues objectForKey:key];
	}
	return nil;
}


- (void) saveDefaultUnitForDimension
{
	NSString *dimensionKey = [[NSString alloc] initWithFormat:kUnitDefaultMask, self.dimension];
	[[NSUserDefaults standardUserDefaults] setObject:self.name forKey:dimensionKey];
	[dimensionKey release];
}

// returns the name for the default unit of this dimension
- (NSString *) defaultUnitNameForDimension
{
	NSString *dimensionKey = [[NSString alloc] initWithFormat:kUnitDefaultMask, self.dimension];
	NSString *defaultName = [[NSUserDefaults standardUserDefaults] objectForKey:dimensionKey];
	[dimensionKey release];
	
	return defaultName;
}

- (void) saveDefaultUnitForKey:(NSString *)defaultKey
{
	[[NSUserDefaults standardUserDefaults] setObject:self.name forKey:defaultKey];
}

- (NSString *) defaultUnitNameForKey:(NSString *)defaultKey
{
	NSString *defaultName = [[NSUserDefaults standardUserDefaults] objectForKey:defaultKey];
	
	return defaultName;
}
#pragma mark -



#pragma mark Utilities
- (NSString *) description
{
	return [NSString stringWithFormat:@"RAUnit <0x%X> %@ (%@)", (unsigned int)self, name, label];
}


@end
