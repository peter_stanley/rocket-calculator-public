//
//  AltitudeDataAssistViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
@protocol AltitudeDataAssistViewControllerDelegate;

@class RAUnit;
@class CalcTextfield;

#import <UIKit/UIKit.h>
#import "SingleSelectListViewController.h"
#import "MotorSelectListViewController.h"

@interface AltitudeDataAssistViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, SingleSelectListViewControllerDelegate, MotorSelectListViewControllerDelegate>

{
    id <AltitudeDataAssistViewControllerDelegate> delegate;
   
    NSArray *bodyTubesArray;
    
    bool isIpad;

    double motorCaseMass;
    double propellentMass;
    double totalMotorMass;
    double totalImpulse;
    double avgThrust;
    double burnTime;
    double frontalArea;
    double bodyDiameter;
    
    UIScrollView *scrollView;

    UITextField *activeField;
    UIActionSheet *sheet;
    
    // accessors
        
    UILabel *motorCaseMassUnitLabel;
    CalcTextfield *motorCaseMassTextfield;
    
    UILabel *propellantMassUnitLabel;
    CalcTextfield *propellantMassTextfield;
    
    UILabel *totalMotorMassUnitLabel;
    CalcTextfield *totalMotorMassTextfield;
    
    UILabel *totalImpulseUnitLabel;
    CalcTextfield *totalImpulseTextfield;
    
    UILabel *avgThrustUnitLabel;
    CalcTextfield *avgThrustTextfield;
    
    UILabel *burnTimeUnitLabel;
    CalcTextfield *burnTimeTextfield;
    
    UILabel *bodyTubeDiameterUnitLabel;
    CalcTextfield *bodyTubeDiameterTextfield;
    
    UILabel *frontalAreaUnitLabel;
    CalcTextfield *frontalAreaTextfield;
    
    UIButton *doneButton;
    UIButton *bodyTubeButton;
    UIButton *motorButton;
    
    RAUnit *caseMassUnit;
    RAUnit *propellantMassUnit;
    RAUnit *totalMotorMassUnit;
    RAUnit *impulseUnit;
    RAUnit *avgThrustUnit;
    RAUnit *burnTimeUnit;
    RAUnit *bodyDiameterUnit;
    RAUnit *areaUnit;
}

@property (nonatomic, assign) id<AltitudeDataAssistViewControllerDelegate> delegate;

@property (nonatomic, readwrite) double motorCaseMass;
@property (nonatomic, readwrite) double propellentMass;
@property (nonatomic, readwrite) double totalMotorMass;
@property (nonatomic, readwrite) double totalImpulse;
@property (nonatomic, readwrite) double avgThrust;
@property (nonatomic, readwrite) double burnTime;
@property (nonatomic, readwrite) double frontalArea;
@property (nonatomic, readwrite) double bodyDiameter;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain) NSArray *bodyTubesArray;

@property (nonatomic, retain) IBOutlet UILabel *motorCaseMassUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *motorCaseMassTextfield;

@property (nonatomic, retain) IBOutlet UILabel *propellantMassUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *propellantMassTextfield;

@property (nonatomic, retain) IBOutlet UILabel *totalMotorMassUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *totalMotorMassTextfield;

@property (nonatomic, retain) IBOutlet UILabel *totalImpulseUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *totalImpulseTextfield;

@property (nonatomic, retain) IBOutlet UILabel *avgThrustUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *avgThrustTextfield;

@property (nonatomic, retain) IBOutlet UILabel *burnTimeUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *burnTimeTextfield;

@property (nonatomic, retain) IBOutlet UILabel *bodyTubeDiameterUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *bodyTubeDiameterTextfield;

@property (nonatomic, retain) IBOutlet UILabel *frontalAreaUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *frontalAreaTextfield;

@property (nonatomic, retain) IBOutlet UIButton *doneButton;
@property (nonatomic ,retain) IBOutlet UIButton *bodyTubeButton;
@property (nonatomic, retain) IBOutlet UIButton *motorButton;

- (IBAction)updateFields:(id)sender;
- (IBAction)dismissViewController:(id)sender;
- (IBAction)openBodyTubeViewController:(id)sender;
- (IBAction)openMotorViewController:(id)sender;
- (void)showAlert:(NSString *)m;
- (void) enableTextfields;
- (void) resignAllResponders;
- (void)resetTextfieldDisplay;
- (NSArray *)loadBodyTubesArray;
@end

@protocol AltitudeDataAssistViewControllerDelegate

- (void)altitudeDataAssistViewControllerWasDismissed:(AltitudeDataAssistViewController*)altitudeDataAssistViewController;

@end

