//
//  Calculator.h
//  RocketCalculator
//
//  Created by Peter Stanley on 10/27/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//
//@class Rocket, FlightLog;

#import <Foundation/Foundation.h>

@interface Calculator : NSObject
{

   // Rocket *rocket;
   // FlightLog *flightLog;
    
}

// Shared
- (double) calcAreaFromMass:(double)mass andVelocity:(double)velocity andCd:(double)Cd andAirDensity:(double)ad;
- (double) calcDescentVelocityFromMass:(double)mass andArea:(double)area andCd:(double)cd andAirDensity:(double)ad;
- (double) calcVolumeFromWidth:(double)width andLength:(double)length;
- (double) calcAreaOfCircle:(double)width;
- (double) calcDiameterFromArea:(double)area;

// Altitude Prediction

- (NSString *) calcMotorClassFromTotalImpulse:(double)i;
- (NSString *) calcMotorClassLetterOnlyFromTotalImpulse:(double)i;
- (double) calcMotorCaseMassFromPropellantMass:(double)p andTotalMotorMass:(double)m;
- (double) calcPropellantMassFromMotorCaseMass:(double)c andTotalMotorMass:(double)m;
- (double) calcTotalMotorMassFromMotorCaseMass:(double)c andPropellantMass:(double)p;

- (double) calcAvgThrustFromTotalImpulse:(double)t andBurnTime:(double)b;
- (double) calcBurnTimeFromTotalImpulse:(double)t andAvgThrust:(double)a;
- (double) calcTotalImpulseFromAvgThrust:(double)a andBurnTime:(double)b;

- (double) calcAverageThrustingMassFromRocketMass:(double)rm andMotorCaseMass:(double)cm andPropellantMass:(double)pm;
- (double) calcAverageThrustingMassFromRocketMass:(double)rm andTotalMotorMass:(double)tmm andPropellantMass:(double)pm;

- (double) calcBurnoutMassFromRocketMass:(double)rm andMotorCaseMass:(double)cm;
- (double) calcKFromCd:(double)c andFromArea:(double)a andAirDensity:(double)ad;

- (double) calcBurnoutVelocityFromAvgThrustingMass:(double)ms andK:(double)k andAvgThrust:(double)at andBurnTime:(double)bt;
- (double) calcBurnoutVelocityZeroDragFromAvgThrustingMass:(double)ms andAvgThrust:(double)at andBurnoutAltitude:(double)alt;
- (double) calcBurnoutAltitudeFromAvgThrustingMass:(double)ms andK:(double)k andAvgThrust:(double)at andBurnoutVelocity:(double)v;
- (double) calcBurnoutAltitudeFromAvgThrustingMass:(double)ms andK:(double)k andAvgThrust:(double)at andBurnTime:(double)bt;
- (double) calcBurnoutAltitudeZeroDragFromAvgThrustingMass:(double)ms andAvgThrust:(double)at andBurnTime:(double)bt;
- (double) calcCoastAltitudeFromBurnoutMass:(double)bm andK:(double)k andBurnoutVelocity:(double)bv;
- (double) calcPeakAltitudeFromBurnoutAltitude:(double)ba andCoastAltitude:(double)ca;
- (double) calcPeakAltitudeZeroDragFromBurnoutAltitude:(double)ba andAvgThrustingMass:(double)ms andAvgThrust:(double)at;
- (double) calcCoastTimeFromBurnoutMass:(double)bm andBurnoutVelocity:(double)bv andK:(double)K;
- (double) calcCoastTimeZeroDragFromBurnoutAltitude:(double)ba andPeakAltitude:(double)pa;
- (double) calcAirDensityFromAltitudeInMeters:(double)h andTemperatureInKelvin:(double)T1;

// Thrust to weight calculations

- (double) calcMinThrustForMass:(double)mass andRatio:(int)ratio;
- (double) calcMaxMassForThrust:(double)newtons andRatio:(int)ratio;
- (double) calcThrustToWeightRatioFromThrust:(double)t andWeight:(double)w;

// Drift Distance Calcualtions

- (double) calcTimeFromAltitude:(double)a andDescentRate:(double)dr;
- (double) calcDriftDistanceFromTime:(double)t andWindSpeed:(double)ws;

// Parachute calculations

- (double) calcChuteDiameterFromMass:(double)mass andVelocity:(double)velocity andCd:(double)Cd andShape:(int)shape andAirDensity:(double)ad;
- (double )calcChuteAreaFromDiameter:(double)diameter andShape:(int)shape;
- (double)calcSpillHoleAreaFromDiameter:(double)diameter;
- (double) calcChuteDescentVelocityFromMass:(double)mass andDiameter:(double)diameter andCd:(double)Cd andAirDensity:(double)ad;

// Streamer calculations

- (double) calcStreamerWidthFromArea:(double)area;
- (double) calcStreamerLengthFromWidth:(double)width andArea:(double)area;
- (double) calcStreamerDescentVelocityFromMass:(double)mass andArea:(double)area andCd:(double)cd andAirDensity:(double)ad;
- (double) calcStreamerAreaFromMass:(double)mass;

// BP calculations

- (double) calcBPFromVolume:(double)volume andPressure:(double)pressure;
- (double) calcPressureFromMass:(double)mass andVolume:(double)volume;
- (double) calcForceFromArea:(double)a andPressure:(double)p;
- (double) calcPsiFromForce:(double)f andArea:(double)a;

- (int) calcMin256ScrewsFromForce:(double)f;
- (int) calcMax256ScrewsFromForce:(double)f;
- (int) calcAvg256ScrewsFromForce:(double)f;

- (int) calcMin440ScrewsFromForce:(double)f;
- (int) calcMax440ScrewsFromForce:(double)f;
- (int) calcAvg440ScrewsFromForce:(double)f;

- (int) calcMin632ScrewsFromForce:(double)f;
- (int) calcMax632ScrewsFromForce:(double)f;
- (int) calcAvg632ScrewsFromForce:(double)f;

// Static port size calculations

- (double) calcPortSizeFromWidth:(double)width andLength:(double)length andNumberPorts:(int)numPorts;

// Two station tracking

- (double) calcAlt1ValueFromAzimuth1:(double)az1 andAzimuth2:(double)az2 andElevation1:(double)elev1 andBaseline:(double)b;
- (double) calcAlt2ValueFromAzimuth1:(double)az1 andAzimuth2:(double)az2 andElevation2:(double)elev2 andBaseline:(double)b;
- (double) calcAverageAltFromAlt1:(double)a1 andAlt2:(double)a2;
- (double) calcClosureErrorFromAlt1:(double)a1 andAlt2:(double)a2;

// Single station tracking

- (double)calcAltitudeFromBaseline:(double)b andElevation:(double)e;

@end
