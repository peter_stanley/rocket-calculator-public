//
//  WeightToThrustCalculatorViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

@class RAUnit;
@class CalcTextfield;

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface WeightToThrustCalculatorViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIPrintInteractionControllerDelegate, UIPopoverControllerDelegate> 
{
    bool isIpad;
    
    double mass;
    double thrust;
    int ratio;
    
    CalcTextfield *massTextField;
    UILabel *massUnitLabel;
    CalcTextfield *thrustTextField;
    UILabel *thrustUnitLabel;
    CalcTextfield *ratioTextField;
    
    RAUnit *massUnit;
    RAUnit *thrustUnit;
    
    UIScrollView *scrollView;
    UIActionSheet *sheet;
    UIPopoverController *popOverController;

}

@property (nonatomic, retain) IBOutlet CalcTextfield *massTextField;
@property (nonatomic, retain) IBOutlet UILabel *massUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *thrustTextField;
@property (nonatomic, retain) IBOutlet UILabel *thrustUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *ratioTextField;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) UIPopoverController *popOverController;

- (IBAction) updateFields:(id)sender;
- (IBAction) infoButtonAction:(id)sender;
- (IBAction) exportButtonAction:(id)sender;
- (IBAction) showEmailModalView:(id)sender;
- (IBAction) printCalculation:(id)sender;
- (NSString *) exportMsgText;
- (NSString *) exportMsgHtml;
- (void) enableTextfields;
- (void) resignAllResponders;
- (void) resetTextfieldDisplay;

@end
