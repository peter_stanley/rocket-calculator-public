//
//  CalculatorRootViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 4/3/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CalculatorRootViewControllerCell;

@interface CalculatorRootViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    bool introLoaded;
    
    UITableView *calculatorTableView;
    CalculatorRootViewControllerCell *calculatorRootViewControllerCell;

}

@property (nonatomic, retain) IBOutlet UITableView *calculatorTableView;
@property (nonatomic, assign) IBOutlet CalculatorRootViewControllerCell *calculatorRootViewControllerCell;

- (IBAction)settingsButtonAction:(id)sender;
- (void)loadAboutScreen;

@end
