//
//  CalcTextfield.m
//  RocketCalculator
//
//  Created by Peter Stanley on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CalcTextfield.h"

@implementation CalcTextfield

- (void)setRequired:(BOOL)r
{
    if (r) {

       // self.layer.cornerRadius=8.0f;
        self.layer.masksToBounds=YES;
        self.layer.borderColor = [[UIColor redColor]CGColor];
        self.layer.borderWidth= 2.0f;
        
        
    }
    else {

        self.layer.borderColor=[[UIColor clearColor]CGColor];

    }
}

@end
