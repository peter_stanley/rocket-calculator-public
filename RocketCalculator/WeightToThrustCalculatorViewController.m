//
//  WeightToThrustCalculatorViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

#import "WeightToThrustCalculatorViewController.h"
#import "RAUnit.h"
#import "RocketCalculatorAppDelegate.h"
#import "Calculator.h"
#import "GenericFormulaViewController.h"
#import "CalcTextfield.h"

@implementation WeightToThrustCalculatorViewController

#define MASS_TEXTFIELD 1
#define THRUST_TEXTFIELD 2
#define RATIO_TEXTFIELD 3

@synthesize scrollView, massTextField, thrustTextField, ratioTextField, massUnitLabel, thrustUnitLabel, popOverController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mass = 0;
    thrust = 0;
    ratio = 0;

    // Check if iPad
    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];
    
    
    CGFloat frameHeight = self.view.frame.size.height;
    CGFloat frameWidth = self.view.frame.size.width;
    
    UIScrollView *tempScrollView=(UIScrollView *)self.view;
    tempScrollView.contentSize=CGSizeMake(frameWidth, frameHeight * 2);
    
    self.title = @"Thrust : Weight";
    
	UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateFields:)];
	gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
	[gestureRecognizer release];

    massUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_tvw_rocket_mass"] ofDimension:@"weight"];
    
    thrustUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_tvw_thrust"] ofDimension:@"force"];

    // Setup toolbar
    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 75.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    // Add actionsheet button
    UIButton *exportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exportButton.frame = CGRectMake(0, 0, 25, 20); //set frame for button
    
    UIImage *buttonImage = [UIImage imageNamed:@"action2.png"];
    
    [exportButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [exportButton addTarget:self action:@selector(exportButtonAction:) forControlEvents:UIControlEventTouchUpInside];

    
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:exportButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    self.navigationController.navigationBar.translucent = NO;
    // Create a spacer.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    bi.width = 8.0f;
    [buttons addObject:bi];
    [bi release];
    
    // Add info button
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    infoButton.frame = CGRectMake(0, 0, 20, 20); //set frame for button
    
    buttonImage = [UIImage imageNamed:@"info_g.png"];
    
    [infoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
 
    modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];

    [tools setItems:buttons animated:NO];
    [buttons release];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    [tools release];
    self.navigationItem.rightBarButtonItem = twoButtons;
    
    [twoButtons release];

    [self resetTextfieldDisplay];

}

- (IBAction)infoButtonAction:(id)sender {
    GenericFormulaViewController *infoView = [[[GenericFormulaViewController alloc] init] autorelease];
    infoView.htmlFileName = @"thrustToWeight";
    [infoView setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
   // [self presentModalViewController:infoView animated:YES];
    [self presentViewController:infoView animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSUserDefaults standardUserDefaults] synchronize];

    self.massUnitLabel.text = [NSString stringWithFormat:@"(%@)", massUnit.name];
    self.massTextField.placeholder = [NSString stringWithFormat:@"%@", massUnit.label];
    
    self.thrustUnitLabel.text = [NSString stringWithFormat:@"(%@)", thrustUnit.name];
    self.thrustTextField.placeholder = [NSString stringWithFormat:@"%@", thrustUnit.label];
    
    [self resetTextfieldDisplay];
    
    [self.view setNeedsDisplay];
    
}

- (void)willEnterForeground
{
    NSLog(@"will enter foreground seen");
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self resetTextfieldDisplay];
    
    [self.view setNeedsDisplay];
}


- (void)viewDidUnload
{
    self.popOverController = nil;
    self.thrustUnitLabel = nil;
    self.massUnitLabel = nil;
    self.ratioTextField = nil;
    self.thrustTextField = nil;
    self.massTextField = nil;
    self.scrollView = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc
{
    NSLog(@"weight to thruse dealloc seen");
    [popOverController release];
    [thrustUnitLabel release];
    [massUnitLabel release];
    [ratioTextField release];
    [thrustTextField release];
    [massTextField release];
    [scrollView release];
                         
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    if (isIpad) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    } else {
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }
    
    
    if (textField.tag == MASS_TEXTFIELD) {
        
        massTextField.text = nil;
        mass = 0;
        
        thrustTextField.text = nil;
        thrust = 0;
        
        [thrustTextField setUserInteractionEnabled:NO];
        [ratioTextField setUserInteractionEnabled:NO];
        
    } else if (textField.tag == THRUST_TEXTFIELD) {
             
        thrustTextField.text = nil;
        thrust = 0;
        
        massTextField.text = nil;
        mass = 0;
        
        [massTextField setUserInteractionEnabled:NO];
        [ratioTextField setUserInteractionEnabled:NO];
        
    } else if (textField.tag == RATIO_TEXTFIELD) {
        
        ratioTextField.text = nil;
        ratio = 0;
        
        thrustTextField.text = nil;
        thrust = 0;
        
        massTextField.text = nil;
        mass = 0;
        
        [massTextField setUserInteractionEnabled:NO];
        [thrustTextField setUserInteractionEnabled:NO];
        
    }
}

- (IBAction)updateFields:(id)sender
{

    [self resignAllResponders];
    [self enableTextfields];
    
    if (!mass) {
        
        mass = [massTextField.text doubleValue];
        
    }
    
    if (!thrust) {
        
        thrust = [thrustTextField.text doubleValue];
        
    }
    
    if (!ratio) {
        
        ratio = [ratioTextField.text intValue];
        
    }
    
    if (ratio == 0) {
        ratio = 5;
        ratioTextField.text = [NSString stringWithFormat:@"%d",ratio];
    }
    
    double baseMass = 0;
    double baseThrust = 0;
    double calcMass = 0;
    double calcThrust = 0;
    
    Calculator *calculator = [[Calculator alloc] init];
    
    // If thrust was entered, calc mass
    if ([massTextField.text isEqualToString: @""] || massTextField.text == nil) {

        baseThrust = [[thrustUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:thrust]] doubleValue];
        calcMass = [calculator calcMaxMassForThrust:baseThrust andRatio:ratio];
        
        mass =  [[massUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcMass] toUnit:massUnit]floatValue];
        
        massTextField.text = [NSString stringWithFormat:@"%.2f", mass];

    } else if ([thrustTextField.text isEqualToString: @""] || thrustTextField.text == nil) {
                
        baseMass = [[massUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:mass]] doubleValue];
        calcThrust = [calculator calcMinThrustForMass:baseMass andRatio:ratio];
        
        thrust = [[thrustUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcThrust] toUnit:thrustUnit]floatValue];
        
        thrustTextField.text = [NSString stringWithFormat:@"%.2f", thrust];
        
    }
    
    [calculator release];
    
    [self resetTextfieldDisplay];
}

-(IBAction) showEmailModalView:(id)sender 
{
    
    if([MFMailComposeViewController canSendMail]){
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        
        if (picker) {
            
            picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
            NSString *mailSubject = [NSString stringWithFormat:@"%@ Calculation", self.title];
            NSString *mailMsg = [self exportMsgHtml];

            [picker setSubject:mailSubject];
    
            [picker setMessageBody:mailMsg isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
    
            picker.navigationBar.barStyle = UIBarStyleDefault; // choose your style, unfortunately, Translucent colors behave quirky.
    
            //[self presentModalViewController:picker animated:YES];
            [self presentViewController:picker animated:YES completion:nil];
            [picker release];
    
        }
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Mail Accounts" message:@"Please set up a Mail account in order to send email." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        
        
    }


}
// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{ 
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-("
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
            
            break;
    }
   //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)exportButtonAction:(id)sender
{
    if (![UIPrintInteractionController isPrintingAvailable]) {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Email Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", nil];

    } else {
    sheet = [[UIActionSheet alloc] initWithTitle:@"Export Calculation"
                                        delegate:self
                               cancelButtonTitle:@"Cancel"
                          destructiveButtonTitle:nil
                               otherButtonTitles:@"E-mail", @"Print", nil];
    }
    // Show the sheet
    [sheet showFromTabBar:self.tabBarController.tabBar];
    [sheet release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
   // NSLog(@"Button %d", buttonIndex);
    switch (buttonIndex) {
        case 0:
            [self showEmailModalView:nil];
            break;

        case 1:
            [self printCalculation:nil];
            break;
            
        default:
            break;
    }
}

#pragma mark -

#pragma mark Printing



// Print button handler that sends our print instructions to the OS

- (IBAction)printCalculation:(id)sender {
    // Get a reference to the singleton iOS printing concierge
    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    
    if(pic && [UIPrintInteractionController isPrintingAvailable]) {
      //  UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = self.title;
        pic.printInfo = printInfo;
        
       // NSString *msg = @"dsfs fs ff sdf asf asf safs";
        NSString *msg = [self exportMsgText];
       // NSLog(@"msg = %@", msg);
        UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc]
                                                     initWithText:msg];
        textFormatter.startPage = 0;
        textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
        textFormatter.maximumContentWidth = 6 * 72.0;
        pic.printFormatter = textFormatter;
        [textFormatter release];
        pic.showsPageRange = YES;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSLog(@"Printing could not complete because of error: %@", error);
            }
        };
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            //[pic presentFromBarButtonItem:sender animated:YES completionHandler:completionHandler];
            [pic presentFromRect:self.tabBarController.tabBar.frame inView:self.view animated:YES completionHandler:completionHandler];
        
        } else {
            [pic presentAnimated:YES completionHandler:completionHandler];
        }
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Printing Error" message:@"Your device doesn't support printing" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }

}

- (NSString *) exportMsgText
{
    NSString *msg = [NSString stringWithFormat:@"%@\n\nSafe max liftoff weight: %.2f (%@)\nSafe minimum thrust: %.2f (%@)\n\nfor Thrust to Weight ratio %d:1\n\n\nGenerated by Rocket Calculator<br />Available on the Apple App Store",self.title, [massTextField.text doubleValue], massUnit.name, [thrustTextField.text floatValue], thrustUnit.name, [ratioTextField.text intValue]];

    return msg;
}

- (NSString *) exportMsgHtml
{
    NSString *msg = [NSString stringWithFormat:@"%@<br /><br />Safe max liftoff weight: %.2f (%@)<br />Safe minimum thrust: %.2f (%@)<br /><br />for Thrust to Weight ratio %d:1<br /><br /><br />Generated by Rocket Calculator<br />Available on the Apple <a href=\"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=484673201&mt=8\">App Store</a>",self.title, [massTextField.text doubleValue], massUnit.name, [thrustTextField.text floatValue], thrustUnit.name, [ratioTextField.text intValue]];
    
    return msg;

}

- (void)enableTextfields
{
    
    [massTextField setUserInteractionEnabled:YES];
    [thrustTextField setUserInteractionEnabled:YES];
    [ratioTextField setUserInteractionEnabled:YES];
    
}

- (void) resignAllResponders
{
    
    [massTextField resignFirstResponder];
    [thrustTextField resignFirstResponder];
    [ratioTextField resignFirstResponder];
    
}

- (void) resetTextfieldDisplay
{
    // Tint required fields
    
    if (massTextField.text == nil || [massTextField.text isEqualToString:@""] || [massTextField.text doubleValue] == 0) {
        
        self.massTextField.text = nil;
        [self.massTextField setRequired:YES];
    } else {
        
        [self.massTextField setRequired:NO];
    }
    
    if (thrustTextField.text == nil || [thrustTextField.text isEqualToString:@""] || [thrustTextField.text doubleValue] == 0) {
        
        self.thrustTextField.text = nil;
        [self.thrustTextField setRequired:YES];
        
    } else {
        
        [self.thrustTextField setRequired:NO];        
        
    }
    
    if (ratioTextField.text == nil || [ratioTextField.text isEqualToString:@""] || [ratioTextField.text doubleValue] == 0) {
        
        self.ratioTextField.text = nil;
        [self.ratioTextField setRequired:YES];
        [self.massTextField setUserInteractionEnabled:NO];
        [self.thrustTextField setUserInteractionEnabled:NO];
        
    } else {
        
        [self.ratioTextField setRequired:NO];        
        
    }

    
}

@end
