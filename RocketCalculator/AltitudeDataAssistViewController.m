//
//  AltitudeDataAssistViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AltitudePredictionViewController.h"
#import "AltitudeDataAssistViewController.h"
#import "RAUnit.h"
#import "RocketCalculatorAppDelegate.h"
#import "Calculator.h"
#import "CalcTextfield.h"
#import "SingleSelectListViewController.h"
#import "MotorSelectListViewController.h"
#import "Motor.h"

@implementation AltitudeDataAssistViewController

#define MOTOR_CASE_MASS_TEXTFIELD 1
#define PROPELLANT_MASS_TEXTFIELD 2
#define TOTAL_MOTOR_MASS_TEXTFIELD 3
#define TOTAL_IMPULSE_TEXTFIELD 4
#define AVG_THRUST_TEXTFIELD 5
#define BURN_TIME_TEXTFIELD 6
#define BODY_TUBE_DIAMETER_TEXTFIELD 7
#define AREA_TEXTFIELD 8

@synthesize motorCaseMass, propellentMass, totalMotorMass, totalImpulse, avgThrust, burnTime, frontalArea, bodyDiameter;

@synthesize scrollView;
@synthesize delegate, bodyTubesArray, motorCaseMassUnitLabel, motorCaseMassTextfield, propellantMassUnitLabel, propellantMassTextfield;
@synthesize totalMotorMassUnitLabel, totalMotorMassTextfield, totalImpulseUnitLabel, totalImpulseTextfield;
@synthesize avgThrustUnitLabel, avgThrustTextfield, burnTimeUnitLabel, burnTimeTextfield;
@synthesize bodyTubeDiameterUnitLabel, bodyTubeDiameterTextfield, frontalAreaUnitLabel, frontalAreaTextfield, doneButton, bodyTubeButton, motorButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)registerForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unRegisterForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardDidShowNotification 
                                                  object:nil]; 
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];  
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    RocketCalculatorAppDelegate *appDelegate = (RocketCalculatorAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown) {
        
        CGFloat tabBarHeight = appDelegate.tabBarController.tabBar.frame.size.height;
        
        NSDictionary* info = [aNotification userInfo];
        
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        
        self.scrollView.contentInset = contentInsets;
        
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        CGRect aRect = self.view.frame;
        
        aRect.size.height -= kbSize.height;
        
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height+tabBarHeight);
            
            [scrollView setContentOffset:scrollPoint animated:YES];
            
        }
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.bodyTubesArray = [NSArray array];
    
    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];
    
    CGFloat frameHeight = self.view.frame.size.height;
    CGFloat frameWidth = self.view.frame.size.width;
    
    UIScrollView *tempScrollView=(UIScrollView *)self.view;
    tempScrollView.contentSize=CGSizeMake(frameWidth, frameHeight * 2);
    
    self.title = @"Data Assistant";
    
	UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateFields:)];
	gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
	[gestureRecognizer release];
    // units setup
    
    impulseUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_total_impulse"] ofDimension:@"impulse"];
    
    avgThrustUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_avg_thrust"] ofDimension:@"force"];
    
    burnTimeUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_burn_time"] ofDimension:@"time"];
    
    totalMotorMassUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_motor_mass"] ofDimension:@"weight"];
    
    propellantMassUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_prop_mass"] ofDimension:@"weight"];
    
    caseMassUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_case_mass"] ofDimension:@"weight"];
    
    bodyDiameterUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_body_diameter"] ofDimension:@"length"];
    
    areaUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_area"] ofDimension:@"areaSmall"];
    
    // Setup toolbar
    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 75.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    // Add actionsheet button
    UIButton *exportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exportButton.frame = CGRectMake(0, 0, 25, 20); //set frame for button
    
    UIImage *buttonImage = [UIImage imageNamed:@"action2.png"];
    
    [exportButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [exportButton addTarget:self action:@selector(exportButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:exportButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    
    // Create a spacer.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    bi.width = 8.0f;
    [buttons addObject:bi];
    [bi release];
    
    // Add info button
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    infoButton.frame = CGRectMake(0, 0, 20, 20); //set frame for button
    
    buttonImage = [UIImage imageNamed:@"info_g.png"];
    
    [infoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    
    [tools setItems:buttons animated:NO];
    [buttons release];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    [tools release];
    self.navigationItem.rightBarButtonItem = twoButtons;
    
    [twoButtons release];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
    
    self.bodyTubesArray = [self loadBodyTubesArray];
    
    // total up total motor mass and total impulse if enough data exists
    double baseTotalMotorMass = [[totalMotorMassUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:totalMotorMass]] doubleValue];
    
    double baseMotorCaseMass = [[caseMassUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:motorCaseMass]] doubleValue];
    double basePropellentMass = [[propellantMassUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:propellentMass]] doubleValue];
    double baseAvgThrust = [[avgThrustUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:avgThrust]] doubleValue];
    double baseBurnTime = [[burnTimeUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:burnTime]] doubleValue];
    double baseArea = [[areaUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:frontalArea]] doubleValue];
    
    double baseAreaForDiameterCalc = baseArea * 0.0001;
    double calcTotalMotorMass = 0;
    double calcMotorCaseMass = 0;
    double calcPropellentMass = 0;
    double calcTotalImpulse = 0;
    double calcDiameter = 0;
    
    Calculator *calc = [[Calculator alloc] init];
    
    if (motorCaseMass && propellentMass && !totalMotorMass) {
        
        calcTotalMotorMass = [calc calcTotalMotorMassFromMotorCaseMass:baseMotorCaseMass andPropellantMass:basePropellentMass];
        totalMotorMass = [[totalMotorMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcTotalMotorMass] toUnit:totalMotorMassUnit]doubleValue];
        
    } else if (!motorCaseMass && propellentMass && totalMotorMass) {
        
        calcMotorCaseMass = [calc calcMotorCaseMassFromPropellantMass:basePropellentMass andTotalMotorMass:baseTotalMotorMass];
        
        motorCaseMass = [[caseMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcMotorCaseMass] toUnit:caseMassUnit]doubleValue];
    } else if (motorCaseMass && !propellentMass && totalMotorMass) {
        
        calcPropellentMass = [calc calcPropellantMassFromMotorCaseMass:baseMotorCaseMass andTotalMotorMass:baseTotalMotorMass];
        
        propellentMass = [[propellantMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcPropellentMass] toUnit:propellantMassUnit]doubleValue];

    }
    
    if (avgThrust && burnTime) {
        
        calcTotalImpulse = [calc calcTotalImpulseFromAvgThrust:baseAvgThrust andBurnTime:baseBurnTime];
        totalImpulse = [[impulseUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcTotalImpulse] toUnit:impulseUnit]doubleValue];
    }
    
    if (frontalArea && !bodyDiameter) {
        
        calcDiameter = [calc calcDiameterFromArea:baseAreaForDiameterCalc];
        bodyDiameter = [[bodyDiameterUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcDiameter] toUnit:bodyDiameterUnit]doubleValue];
    }
    
    [calc release];
    
    // set units
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.motorCaseMassUnitLabel.text = [NSString stringWithFormat:@"(%@)", caseMassUnit.name];
    self.motorCaseMassTextfield.placeholder = [NSString stringWithFormat:@"%@", caseMassUnit.label];
    
    self.propellantMassUnitLabel.text = [NSString stringWithFormat:@"(%@)", propellantMassUnit.name];
    self.propellantMassTextfield.placeholder = [NSString stringWithFormat:@"%@", propellantMassUnit.label];

    self.totalMotorMassUnitLabel.text = [NSString stringWithFormat:@"(%@)", totalMotorMassUnit.name];
    self.totalMotorMassTextfield.placeholder = [NSString stringWithFormat:@"%@", totalMotorMassUnit.label];

    self.totalImpulseUnitLabel.text = [NSString stringWithFormat:@"(%@)", impulseUnit.name];
    self.totalImpulseTextfield.placeholder = [NSString stringWithFormat:@"%@", impulseUnit.label];

    self.avgThrustUnitLabel.text = [NSString stringWithFormat:@"(%@)", avgThrustUnit.name];
    self.avgThrustTextfield.placeholder = [NSString stringWithFormat:@"%@",avgThrustUnit.label];
    
    self.burnTimeUnitLabel.text = [NSString stringWithFormat:@"(%@)", burnTimeUnit.name];
    self.burnTimeTextfield.placeholder = [NSString stringWithFormat:@"%@", burnTimeUnit.label];
    
    self.bodyTubeDiameterUnitLabel.text = [NSString stringWithFormat:@"(%@)", bodyDiameterUnit.name];
    self.bodyTubeDiameterTextfield.placeholder = [NSString stringWithFormat:@"%@", bodyDiameterUnit.label];

    self.frontalAreaUnitLabel.text = [NSString stringWithFormat:@"(%@)", areaUnit.name];
    self.frontalAreaTextfield.placeholder = [NSString stringWithFormat:@"%@", areaUnit.label];
    
    // populate passed in values
    self.motorCaseMassTextfield.text = [NSString stringWithFormat:@"%.3f", motorCaseMass];
    self.propellantMassTextfield.text = [NSString stringWithFormat:@"%.3f", propellentMass];
    self.totalMotorMassTextfield.text = [NSString stringWithFormat:@"%.3f", totalMotorMass];
    self.avgThrustTextfield.text = [NSString stringWithFormat:@"%.3f", avgThrust];
    self.burnTimeTextfield.text = [NSString stringWithFormat:@"%.3f", burnTime];
    self.totalImpulseTextfield.text = [NSString stringWithFormat:@"%.3f", totalImpulse];
    self.bodyTubeDiameterTextfield.text = [NSString stringWithFormat:@"%.3f", bodyDiameter];
    self.frontalAreaTextfield.text = [NSString stringWithFormat:@"%.3f", frontalArea];
    
    [self updateFields:nil];
    
    [self resetTextfieldDisplay];
}

- (void)willEnterForeground
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self resetTextfieldDisplay];
    [self.view setNeedsDisplay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unRegisterForKeyboardNotifications];
}

- (void)viewDidUnload
{
    self.motorButton = nil;
    self.bodyTubeButton = nil;
    self.doneButton = nil;
    
    self.frontalAreaTextfield = nil;
    self.frontalAreaUnitLabel = nil;
    
    self.bodyTubeDiameterTextfield = nil;
    self.bodyTubeDiameterUnitLabel = nil;
    
    self.burnTimeTextfield = nil;
    self.burnTimeUnitLabel = nil;
    
    self.avgThrustTextfield = nil;
    self.avgThrustUnitLabel = nil;
    
    self.totalImpulseTextfield = nil;
    self.totalImpulseUnitLabel = nil;
    
    self.totalMotorMassTextfield = nil;
    self.totalMotorMassUnitLabel = nil;
    
    self.propellantMassTextfield = nil;
    self.propellantMassUnitLabel = nil;
    
    self.motorCaseMassTextfield = nil;
    self.motorCaseMassUnitLabel = nil;
    self.bodyTubesArray = nil;
    
    self.scrollView = nil;
   // self.callingController = nil;
    
    [super viewDidUnload];

}

- (void)dealloc
{
    [motorButton release];
    [bodyTubeButton release];
    [doneButton release];
    
    [frontalAreaTextfield release];
    [frontalAreaUnitLabel release];
    
    [bodyTubeDiameterTextfield release];
    [bodyTubeDiameterUnitLabel release];
    
    [burnTimeTextfield release];
    [burnTimeUnitLabel release];
    
    [avgThrustTextfield release];
    [avgThrustUnitLabel release];
    
    [totalImpulseTextfield release];
    [totalImpulseUnitLabel release];
    
    [totalMotorMassTextfield release];
    [totalMotorMassUnitLabel release];
    
    [propellantMassTextfield release];
    [propellantMassUnitLabel release];
    
    [motorCaseMassTextfield release];
    [motorCaseMassUnitLabel release];
    [bodyTubesArray release];
    
    [scrollView release];
   // [callingController release];
    
    [super dealloc];
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    if (isIpad) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    } else {
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }
    
    activeField = textField;
    
    if (textField.tag == MOTOR_CASE_MASS_TEXTFIELD) {
        
        motorCaseMassTextfield.text = nil;
        motorCaseMass = 0;
        
        if (totalMotorMassTextfield.text != nil && ![totalMotorMassTextfield.text isEqualToString:@""]) {
            propellantMassTextfield.text = nil;
            propellentMass = 0;
        }
        
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [totalMotorMassTextfield setUserInteractionEnabled:NO];
        [totalImpulseTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [bodyTubeDiameterTextfield setUserInteractionEnabled:NO];
        
    } else if (textField.tag == PROPELLANT_MASS_TEXTFIELD) {
        
        propellantMassTextfield.text = nil;
        propellentMass = 0;
        
        if (totalMotorMassTextfield.text != nil && ![totalMotorMassTextfield.text isEqualToString:@""]) {
            motorCaseMassTextfield.text = nil;
            motorCaseMass = 0;
        }
        
        if (motorCaseMassTextfield.text != nil && ![motorCaseMassTextfield.text isEqualToString:@""]) {
            totalMotorMassTextfield.text = nil;
            totalMotorMass = 0;
        }

        
        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [totalMotorMassTextfield setUserInteractionEnabled:NO];
        [totalImpulseTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [bodyTubeDiameterTextfield setUserInteractionEnabled:NO];
        
    } else if (textField.tag == TOTAL_MOTOR_MASS_TEXTFIELD) {
        
        totalMotorMassTextfield.text = nil;
        totalMotorMass = 0;
        /*
        if (![motorCaseMassTextfield.text isEqualToString:@""] && ![propellantMassTextfield.text isEqualToString:@""] && motorCaseMassTextfield.text !=nil && propellantMassTextfield.text != nil) {
            motorCaseMassTextfield.text = nil;
            motorCaseMass = 0;
            
            propellantMassTextfield.text = nil;
            propellentMass = 0;
        }
        */
        if (motorCaseMassTextfield.text != nil && ![motorCaseMassTextfield.text isEqualToString:@""]) {
            propellantMassTextfield.text = nil;
            propellentMass = 0;
            
        }
        
        if (propellantMassTextfield.text != nil && ![propellantMassTextfield.text isEqualToString:@""]) {
            motorCaseMassTextfield.text = nil;
            motorCaseMass = 0;
            
        }

        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [totalImpulseTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [bodyTubeDiameterTextfield setUserInteractionEnabled:NO];
        
    } else if (textField.tag == TOTAL_IMPULSE_TEXTFIELD) {
        
        totalImpulseTextfield.text = nil;
        totalImpulse = 0;
        if (![avgThrustTextfield.text isEqualToString:@""] && ![burnTimeTextfield.text isEqualToString:@""] && avgThrustTextfield.text !=nil && burnTimeTextfield.text != nil) {
            avgThrustTextfield.text = nil;
            avgThrust = 0;
            
            burnTimeTextfield.text = nil;
            burnTime = 0;
        }

        
        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [totalMotorMassTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [bodyTubeDiameterTextfield setUserInteractionEnabled:NO];
        
    } else if (textField.tag == AVG_THRUST_TEXTFIELD) {
        
        avgThrustTextfield.text = nil;
        avgThrust = 0;
        
        if (totalImpulseTextfield.text != nil && ![totalImpulseTextfield.text isEqualToString:@""]) {
            burnTimeTextfield.text = nil;
            burnTime = 0;
        }

        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [totalMotorMassTextfield setUserInteractionEnabled:NO];
        [totalImpulseTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [bodyTubeDiameterTextfield setUserInteractionEnabled:NO];
        
    } else if (textField.tag == BURN_TIME_TEXTFIELD) {
        
        burnTimeTextfield.text = nil;
        burnTime = 0;
        
        if (totalImpulseTextfield.text != nil && ![totalImpulseTextfield.text isEqualToString:@""]) {
            avgThrustTextfield.text = nil;
            avgThrust = 0;
        }

        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [totalMotorMassTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [totalImpulseTextfield setUserInteractionEnabled:NO];
        [bodyTubeDiameterTextfield setUserInteractionEnabled:NO];
        
    } else if (textField.tag == BODY_TUBE_DIAMETER_TEXTFIELD) {
        
        bodyTubeDiameterTextfield.text = nil;
        frontalAreaTextfield.text = nil;
        
        bodyDiameter = 0;
        frontalArea = 0;
        
        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [totalMotorMassTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [totalImpulseTextfield setUserInteractionEnabled:NO];
        
    } 
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    activeField = nil;
    
}
- (IBAction)updateFields:(id)sender
{

    [self resignAllResponders];
    [self enableTextfields];

    if (!motorCaseMass) {
        motorCaseMass = [motorCaseMassTextfield.text doubleValue];

    }
    
    if (!propellentMass) {
        propellentMass = [propellantMassTextfield.text doubleValue];

    }
        
    if (!totalMotorMass) {
        totalMotorMass = [totalMotorMassTextfield.text doubleValue];
    }
    
    if (!totalImpulse) {
        totalImpulse = [totalImpulseTextfield.text doubleValue];
    }
    
    if (!avgThrust) {
        avgThrust = [avgThrustTextfield.text doubleValue];
    }
    
    if (!burnTime) {
        burnTime = [burnTimeTextfield.text doubleValue];
    }
    
    if (!bodyDiameter) {
        bodyDiameter = [bodyTubeDiameterTextfield.text doubleValue];

    }
    
    if (!frontalArea) {
        frontalArea = [frontalAreaTextfield.text doubleValue];
    }
    
    // get base units
    double baseMotorCaseMass = [[caseMassUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:motorCaseMass]] doubleValue];
    double basePropellentMass = [[propellantMassUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:propellentMass]] doubleValue];
    double baseTotalMotorMass = [[totalMotorMassUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:totalMotorMass]] doubleValue];
    double baseTotalImpulse = [[impulseUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:totalImpulse]] doubleValue];
    double baseAvgThrust = [[avgThrustUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:avgThrust]] doubleValue];
    double baseBurnTime = [[burnTimeUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:burnTime]] doubleValue];
    double baseBodyDiameter = [[bodyDiameterUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:bodyDiameter]] doubleValue];
    double baseArea = [[areaUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:frontalArea]] doubleValue];
    baseArea = baseArea * 10000;
    
    Calculator *calculator = [[Calculator alloc] init];
    
    double calcCaseMass = [calculator calcMotorCaseMassFromPropellantMass:basePropellentMass andTotalMotorMass:baseTotalMotorMass];
    double calcPropellentMass = [calculator calcPropellantMassFromMotorCaseMass:baseMotorCaseMass andTotalMotorMass:baseTotalMotorMass];
    double calcTotalMotorMass = [calculator calcTotalMotorMassFromMotorCaseMass:baseMotorCaseMass andPropellantMass:basePropellentMass];
    
    double calcTotalImpulse = [calculator calcTotalImpulseFromAvgThrust:baseAvgThrust andBurnTime:baseBurnTime];
    double calcAvgThrust = [calculator calcAvgThrustFromTotalImpulse:baseTotalImpulse andBurnTime:baseBurnTime];
    double calcBurnTime = [calculator calcBurnTimeFromTotalImpulse:baseTotalImpulse andAvgThrust:baseAvgThrust];
    double calcArea = [calculator calcAreaOfCircle:baseBodyDiameter];

    double calcBaseDiameter = [calculator calcDiameterFromArea:baseArea];

    [calculator release];
    
    // convert calcArea to cm2 to match baseunit.
    calcArea = calcArea * 10000;
    
    // Update motorCase mass textfield
    if (propellentMass && totalMotorMass && !motorCaseMass) {
        
        if (calcCaseMass < 0) {
            [self showAlert:@"Propellant and case masses can't be greater than total mass."];
            
            return;
            
        }
        
        motorCaseMass = [[caseMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcCaseMass] toUnit:caseMassUnit]doubleValue];
        
        motorCaseMassTextfield.text = [NSString stringWithFormat:@"%.3f", motorCaseMass];
        
    }
    
    // update propellant mass textfield
    if (motorCaseMass && totalMotorMass && !propellentMass) {
        
        if (calcPropellentMass < 0) {
            [self showAlert:@"Propellant and case masses can't be greater than total mass."];
            
            return;
            
        }

        propellentMass = [[propellantMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcPropellentMass] toUnit:propellantMassUnit]doubleValue];
        
        propellantMassTextfield.text = [NSString stringWithFormat:@"%.3f", propellentMass];
        
    }
    
    // update total motor mass textfield
    
    if (motorCaseMass && propellentMass && !totalMotorMass) {
        
        totalMotorMass = [[totalMotorMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcTotalMotorMass] toUnit:totalMotorMassUnit]doubleValue];
        totalMotorMassTextfield.text = [NSString stringWithFormat:@"%.3f",totalMotorMass];
        
    }
    
    // update total impulse textfield
    
    if (avgThrust && burnTime && !totalImpulse) {
        totalImpulse = [[impulseUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcTotalImpulse] toUnit:impulseUnit]doubleValue];
        
        totalImpulseTextfield.text = [NSString stringWithFormat:@"%.3f", totalImpulse];
        
    }
    
    // update avg thrust textfield
    if (totalImpulse && burnTime && !avgThrust) {
        
        avgThrust = [[avgThrustUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcAvgThrust] toUnit:avgThrustUnit]doubleValue];
        
        avgThrustTextfield.text = [NSString stringWithFormat:@"%.3f", avgThrust];
        
    }
    
    // update burntime textfield
    if (totalImpulse && avgThrust && !burnTime) {
        
        burnTime = [[burnTimeUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcBurnTime] toUnit:burnTimeUnit]doubleValue];
        
        burnTimeTextfield.text = [NSString stringWithFormat:@"%.3f", burnTime];
        
    }
    
    if (bodyDiameter) {

        frontalArea = [[areaUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcArea] toUnit:areaUnit]doubleValue];

        bodyTubeDiameterTextfield.text = [NSString stringWithFormat:@"%.3f", bodyDiameter];
        frontalAreaTextfield.text = [NSString stringWithFormat:@"%.4f", frontalArea];
        
    }
    
    if (!bodyDiameter && frontalArea) {
        bodyDiameter = [[bodyDiameterUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcBaseDiameter] toUnit:bodyDiameterUnit]doubleValue];
        bodyTubeDiameterTextfield.text = [NSString stringWithFormat:@"%.3f", bodyDiameter];
    }

    [self resetTextfieldDisplay];
}

- (void)dismissViewController:(id)sender
{

    [delegate altitudeDataAssistViewControllerWasDismissed:self];

   // [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)openBodyTubeViewController:(id)sender
{
    
    SingleSelectListViewController *selectListVC = [[SingleSelectListViewController alloc] init];
    
    [selectListVC setDelegate:self];
    
    
    selectListVC.itemList = bodyTubesArray;
    selectListVC.listName = @"Body Tube Selection";
    
    //[self presentModalViewController:selectListVC animated:YES];
    [self presentViewController:selectListVC animated:YES completion:nil];
    
    [selectListVC release];

    
}

- (void)openMotorViewController:(id)sender
{
    
    MotorSelectListViewController *motorListVC = [[MotorSelectListViewController alloc] init];
    
    [motorListVC setDelegate:self];
    
    //[self presentModalViewController:motorListVC animated:YES];
    [self presentViewController:motorListVC animated:YES completion:nil];
    
    [motorListVC release];
    
    
}


- (void)showAlert:(NSString *)m
{
    
    UIAlertView* alertView = [[[UIAlertView alloc] initWithTitle:@"Error" message:m delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
    
    [alertView show];

}

- (void)enableTextfields
{
    
    [motorCaseMassTextfield setUserInteractionEnabled:YES];
    [propellantMassTextfield setUserInteractionEnabled:YES];
    [totalMotorMassTextfield setUserInteractionEnabled:YES];
    [totalImpulseTextfield setUserInteractionEnabled:YES];
    [avgThrustTextfield setUserInteractionEnabled:YES];
    [burnTimeTextfield setUserInteractionEnabled:YES];
    [bodyTubeDiameterTextfield setUserInteractionEnabled:YES];
    
}

- (void) resignAllResponders
{
    
    [motorCaseMassTextfield resignFirstResponder];
    [propellantMassTextfield resignFirstResponder];
    [totalMotorMassTextfield resignFirstResponder];
    [totalImpulseTextfield resignFirstResponder];
    [avgThrustTextfield resignFirstResponder];
    [burnTimeTextfield resignFirstResponder];
    [bodyTubeDiameterTextfield resignFirstResponder];
    
}

- (void) resetTextfieldDisplay
{
    
    // Tint required fields
    if (motorCaseMassTextfield.text == nil || [motorCaseMassTextfield.text isEqualToString:@""] || [motorCaseMassTextfield.text doubleValue] == 0) {
        
        self.motorCaseMassTextfield.text = nil;
        [self.motorCaseMassTextfield setRequired:YES];
    } else {
        
        [self.motorCaseMassTextfield setRequired:NO];
    }
    
    if (propellantMassTextfield.text == nil || [propellantMassTextfield.text isEqualToString:@""] || [propellantMassTextfield.text doubleValue] == 0) {
        
        self.propellantMassTextfield.text = nil;
        [self.propellantMassTextfield setRequired:YES];
    } else {
        
        [self.propellantMassTextfield setRequired:NO];
    }

    if (totalMotorMassTextfield.text == nil || [totalMotorMassTextfield.text isEqualToString:@""] || [totalMotorMassTextfield.text doubleValue] == 0) {
        
        self.totalMotorMassTextfield.text = nil;
        [self.totalMotorMassTextfield setRequired:YES];
    } else {
        
        [self.totalMotorMassTextfield setRequired:NO];
    }

    if (totalImpulseTextfield.text == nil || [totalImpulseTextfield.text isEqualToString:@""] || [totalImpulseTextfield.text doubleValue] == 0) {
        
        self.totalImpulseTextfield.text = nil;
        [self.totalImpulseTextfield setRequired:YES];
    } else {
        
        [self.totalImpulseTextfield setRequired:NO];
    }

    if (avgThrustTextfield.text == nil || [avgThrustTextfield.text isEqualToString:@""] || [avgThrustTextfield.text doubleValue] == 0) {
        
        self.avgThrustTextfield.text = nil;
        [self.avgThrustTextfield setRequired:YES];
    } else {
        
        [self.avgThrustTextfield setRequired:NO];
    }
    
    if (burnTimeTextfield.text == nil || [burnTimeTextfield.text isEqualToString:@""] || [burnTimeTextfield.text doubleValue] == 0) {
        
        self.burnTimeTextfield.text = nil;
        [self.burnTimeTextfield setRequired:YES];
    } else {
        
        [self.burnTimeTextfield setRequired:NO];
    }
    
    if (bodyTubeDiameterTextfield.text == nil || [bodyTubeDiameterTextfield.text isEqualToString:@""] || [bodyTubeDiameterTextfield.text doubleValue] == 0) {

        self.bodyTubeDiameterTextfield.text = nil;
        [self.bodyTubeDiameterTextfield setRequired:YES];
    } else {
        
        [self.bodyTubeDiameterTextfield setRequired:NO];
    }
    
    if (frontalAreaTextfield.text == nil || [frontalAreaTextfield.text isEqualToString:@""] || [frontalAreaTextfield.text doubleValue] == 0) {
        
        self.frontalAreaTextfield.text = nil;
        [self.frontalAreaTextfield setRequired:YES];
    } else {
        
        [self.frontalAreaTextfield setRequired:NO];
    }
    
}

- (NSArray *)loadBodyTubesArray
{
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *pListPath = [path stringByAppendingPathComponent:@"/bodyTube.plist"];
    
    NSDictionary *bodyTubes = [NSDictionary dictionaryWithContentsOfFile:pListPath];
    
    NSMutableArray *btArray = [bodyTubes objectForKey:@"bodyTubes"];    
    
    NSSortDescriptor *manufacturerDescriptor =
    [[[NSSortDescriptor alloc]
      initWithKey:@"btManufacturer"
      ascending:YES
      selector:@selector(localizedCaseInsensitiveCompare:)] autorelease];    
    
    // Sort tubes by manufacturer
    NSArray * descriptors =
    [NSArray arrayWithObjects:manufacturerDescriptor, nil];
    NSArray * sortedArray =
    [btArray sortedArrayUsingDescriptors:descriptors];    
    
    // build array to pass to bodyTube VC
    NSMutableArray *tmpList = [[NSMutableArray alloc] init];
    
    for (NSDictionary *btDict in sortedArray) {
        
        NSString *labelHeader = [NSString stringWithFormat:@"%@ - %@", [btDict valueForKey:@"btManufacturer"], [btDict valueForKey:@"btDescription"]];
        NSString *labelDetail = [NSString stringWithFormat:@"Outer Diameter: %@", [btDict valueForKey:@"btOuterDiameter"]];
        NSNumber *outerDiameter = [btDict valueForKey:@"btOuterDiameterInches"];
        NSMutableDictionary *tmpDict = [[NSMutableDictionary alloc] init];
        [tmpDict setValue:labelHeader forKey:@"labelHeader"];
        [tmpDict setValue:labelDetail forKey:@"labelDetail"];
        [tmpDict setValue:outerDiameter forKey:@"outerDiameter"];
        [tmpList addObject:tmpDict];
        [tmpDict release];
        
    }
    
    return [tmpList autorelease];
    
}

#pragma -
#pragma mark SingleSelectListViewController Delegate methods

- (void)singleSelectListViewControllerWasDismissed:(SingleSelectListViewController*)singleSelectListViewController
{
    
    int selected = (int)singleSelectListViewController.selectedRow;
    if (selected < [bodyTubesArray count]) {
        
        double baseDiameter = ([[[bodyTubesArray objectAtIndex:selected] valueForKey:@"outerDiameter"] doubleValue]) * 0.0254;
        self.bodyDiameter = [[bodyDiameterUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseDiameter] toUnit:bodyDiameterUnit]doubleValue];

    }
    
}

#pragma -
#pragma mark MotorSelectListViewController Delegate methods

- (void)motorSelectListViewControllerWasDismissed:(MotorSelectListViewController*)motorSelectListViewController
{
   // NSLog(@"returned motor: code= %@, initWt=%f, propWt=%f, avgThrust=%f, burnTime = %f", motorSelectListViewController.selectedMotor.code, motorSelectListViewController.selectedMotor.initWt, motorSelectListViewController.selectedMotor.propWt, motorSelectListViewController.selectedMotor.avgThrust, motorSelectListViewController.selectedMotor.burn_time);
    
    // convert grams to kilograms
    double baseTotalMotorMass = motorSelectListViewController.selectedMotor.initWt * 0.001;
    double basePropellentMass = motorSelectListViewController.selectedMotor.propWt * 0.001;
    double baseMotorCaseMass = baseTotalMotorMass - basePropellentMass;
    
    double baseAvgThrust = motorSelectListViewController.selectedMotor.avgThrust;
    double baseBurnTime = motorSelectListViewController.selectedMotor.burn_time;

    self.totalMotorMass = [[totalMotorMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseTotalMotorMass] toUnit:totalMotorMassUnit]doubleValue];
    self.propellentMass = [[propellantMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:basePropellentMass] toUnit:propellantMassUnit]doubleValue];
    self.motorCaseMass = [[caseMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseMotorCaseMass] toUnit:caseMassUnit]doubleValue];
    self.avgThrust = [[avgThrustUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseAvgThrust] toUnit:avgThrustUnit]doubleValue];
    self.burnTime = [[burnTimeUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseBurnTime] toUnit:burnTimeUnit]doubleValue];
    self.totalImpulse = 0;
    
}

@end