//
//  Motor.m
//  RocketCalculator
//
//  Created by Peter Stanley on 4/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Motor.h"

@implementation Motor

@synthesize code, modelNumber, mfg, avgThrust, burn_time, Itot, initWt, propWt, dia, impClass, pctImpClass, fileName, propellantType, sectionNumber;

- (void)dealloc
{
    [propellantType release];
	[fileName release];
	[pctImpClass release];
    [impClass release];
    [mfg release];
    [modelNumber release];
    [code release];
    
	[super dealloc];
}

@end
