//
//  StreamerSizeCalculatorViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

@class RAUnit;
@class CalcTextfield;

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "DensityCalculatorViewController.h"

@interface StreamerSizeCalculatorViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIPrintInteractionControllerDelegate, DensityCalculatorViewControllerDelegate>
{
    bool isIpad;
    
    double airDensity;
    double temperature;
    double elevation;

    double mass;
    double diameter;
    double length;
    double velocity;
    double Cd;
    
    UILabel *elevationLabel;
    UILabel *elevationUnitLabel;
    
    UILabel *temperatureLabel;
    UILabel *temperatureUnitLabel;
    
    UILabel *airDensityLabel;

    CalcTextfield *massTextField;
    UILabel *massUnitLabel;
    CalcTextfield *diameterTextField;
    UILabel *diameterUnitLabel;
    CalcTextfield *lengthTextField;
    UILabel *lengthUnitLabel;
    CalcTextfield *velocityTextField;
    UILabel *velocityUnitLabel;
    UISlider *streamerMaterialSlider;
    UIButton *resetDescentSpeedButton;
    UILabel *cdValueLabel;
    UIButton *tempElevationButton;
    
    RAUnit *temperatureUnit;
    RAUnit *elevationUnit;
    RAUnit *massUnit;
    RAUnit *diameterUnit;
    RAUnit *lengthUnit;
    RAUnit *velocityUnit;
    
    UIScrollView *scrollView;
    // Rocket *rocket;
    // FlightLog *flightLog;
    
    UITextField *activeField;
    UIActionSheet *sheet;

}

@property (nonatomic, readwrite) double temperature;
@property (nonatomic, readwrite) double elevation;

@property (nonatomic, retain) IBOutlet UILabel *elevationLabel;
@property (nonatomic, retain) IBOutlet UILabel *elevationUnitLabel;

@property (nonatomic, retain) IBOutlet UILabel *temperatureLabel;
@property (nonatomic, retain) IBOutlet UILabel *temperatureUnitLabel;

@property (nonatomic, retain) IBOutlet UILabel *airDensityLabel;

@property (nonatomic, retain) IBOutlet CalcTextfield *massTextField;
@property (nonatomic, retain) IBOutlet UILabel *massUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *diameterTextField;
@property (nonatomic, retain) IBOutlet UILabel *diameterUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *lengthTextField;
@property (nonatomic, retain) IBOutlet UILabel *lengthUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *velocityTextField;
@property (nonatomic, retain) IBOutlet UILabel *velocityUnitLabel;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UISlider *streamerMaterialSlider;
@property (nonatomic, retain) IBOutlet UIButton *descentSpeedButton;
@property (nonatomic, retain) IBOutlet UILabel *cdValueLabel;
@property (nonatomic, retain) IBOutlet UIButton *tempElevationButton;

- (IBAction)openDensityCalculatorViewController:(id)sender;
- (IBAction)updateFields:(id)sender;
- (IBAction)changeStreamerMaterial:(id)sender;
- (IBAction)resetDescentSpeed:(id)sender;
- (IBAction)infoButtonAction:(id)sender;
- (IBAction) exportButtonAction:(id)sender;
- (IBAction) showEmailModalView:(id)sender;
- (IBAction) printCalculation:(id)sender;
- (NSString *) exportMsgText;
- (NSString *) exportMsgHtml;
- (void) enableTextfields;
- (void) resignAllResponders;
- (void) resetTextfieldDisplay;

@end
