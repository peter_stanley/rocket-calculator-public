//
//  UnitPreferenceViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 4/6/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UnitPreferenceTableViewCell;

@interface UnitPreferenceViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIPopoverControllerDelegate> {
    
    bool isIpad;
    NSMutableArray *unitPreferenceArray;
    NSMutableDictionary *unitPreferenceDictionary;
    
    UITableView *unitPreferenceTableView;
    UnitPreferenceTableViewCell *unitPreferenceTableViewCell;
    
    UISwitch *lowThrustSwitch;
    UISwitch *machSwitch;
    
    // Select list arrays
    
    // Alerts
    NSMutableDictionary *apThrustToWeightDict;
    NSMutableDictionary *apMachDict;
    
    // Alt Prediction
    NSMutableDictionary *acElevationList;
    NSMutableDictionary *acTemperatureList;
    NSMutableDictionary *acTotalImpulseList;
    NSMutableDictionary *acAvgThrustList;
    NSMutableDictionary *acBurnTimeList;
    NSMutableDictionary *acTotalMotorMassList;
    NSMutableDictionary *acPropellantMassList;
    NSMutableDictionary *acCaseMassList;
    NSMutableDictionary *acRocketMassList;
    NSMutableDictionary *acBodyDiameterList;
    NSMutableDictionary *acAreaList;
    NSMutableDictionary *acVelocityList;
    NSMutableDictionary *acAltitudeList;
    NSMutableDictionary *acCoastTimeList;
    
    // Thrust vs. Weight
    NSMutableDictionary *tvwRocketMassList;
    NSMutableDictionary *tvwForceList;
    
    // Descent Rate / Drift Distance
    NSMutableDictionary *drRocketMassList;
    NSMutableDictionary *ddAltitudeList;
    NSMutableDictionary *drDescentVelocityList;
    NSMutableDictionary *ddDescentDurationList;
    NSMutableDictionary *ddWindSpeedList;
    NSMutableDictionary *ddDriftDistanceList;
    NSMutableDictionary *drParachuteDiameterList;
    NSMutableDictionary *drSpillHoleDiameterList;
    NSMutableDictionary *drStreamerWidthList;
    NSMutableDictionary *drStreamerLengthList;
    
    // Ejection Charge
    NSMutableDictionary *bpTubeWidthList;
    NSMutableDictionary *bpTubeLengthList;
    NSMutableDictionary *bpPressureList;
    NSMutableDictionary *bpForceList;
    NSMutableDictionary *bpMassList;
    
    // Static port size
    NSMutableDictionary *spTubeWidthList;
    NSMutableDictionary *spTubeLengthList;
    NSMutableDictionary *spPortDiameterList;
    
    // Alitude calculation - station tracking
    NSMutableDictionary *atElevationAngleList;
    NSMutableDictionary *atAzimuthAngleList;
    NSMutableDictionary *atBaselineLengthList;
    NSMutableDictionary *atFlightAltitudeList;

    UIPopoverController *popOverController;
    
}

@property (nonatomic, retain) IBOutlet UITableView *unitPreferenceTableView;
@property (nonatomic, assign) IBOutlet UnitPreferenceTableViewCell *unitPreferenceTableViewCell;
@property (nonatomic, retain) NSMutableArray *unitPreferenceArray;
@property (nonatomic, retain) NSMutableDictionary *unitPreferenceDictionary;

@property (nonatomic, retain) UISwitch *lowThrustSwitch;
@property (nonatomic, retain) UISwitch *machSwitch;

// Select list properties

// Alerts
@property (nonatomic, retain) NSMutableDictionary *apThrustToWeightDict;
@property (nonatomic, retain) NSMutableDictionary *apMachDict;

// Alitude prediction
@property (nonatomic, retain) NSMutableDictionary *acElevationList;
@property (nonatomic, retain) NSMutableDictionary *acTemperatureList;
@property (nonatomic, retain) NSMutableDictionary *acTotalImpulseList;
@property (nonatomic, retain) NSMutableDictionary *acAvgThrustList;
@property (nonatomic, retain) NSMutableDictionary *acBurnTimeList;
@property (nonatomic, retain) NSMutableDictionary *acTotalMotorMassList;
@property (nonatomic, retain) NSMutableDictionary *acPropellantMassList;
@property (nonatomic, retain) NSMutableDictionary *acCaseMassList;
@property (nonatomic, retain) NSMutableDictionary *acRocketMassList;
@property (nonatomic, retain) NSMutableDictionary *acBodyDiameterList;
@property (nonatomic, retain) NSMutableDictionary *acAreaList;
@property (nonatomic, retain) NSMutableDictionary *acVelocityList;
@property (nonatomic, retain) NSMutableDictionary *acAltitudeList;
@property (nonatomic, retain) NSMutableDictionary *acCoastTimeList;

// Thrust vs. Weight
@property (nonatomic, retain) NSMutableDictionary *tvwRocketMassList;
@property (nonatomic, retain) NSMutableDictionary *tvwForceList;

// Descent rate / drift distance
@property (nonatomic, retain) NSMutableDictionary *drRocketMassList;
@property (nonatomic, retain) NSMutableDictionary *ddAltitudeList;
@property (nonatomic, retain) NSMutableDictionary *drDescentVelocityList;
@property (nonatomic, retain) NSMutableDictionary *ddDescentDurationList;
@property (nonatomic, retain) NSMutableDictionary *ddWindSpeedList;
@property (nonatomic, retain) NSMutableDictionary *ddDriftDistanceList;
@property (nonatomic, retain) NSMutableDictionary *drParachuteDiameterList;
@property (nonatomic, retain) NSMutableDictionary *drSpillHoleDiameterList;
@property (nonatomic, retain) NSMutableDictionary *drStreamerWidthList;
@property (nonatomic, retain) NSMutableDictionary *drStreamerLengthList;

// Ejection Charge
@property (nonatomic, retain) NSMutableDictionary *bpTubeWidthList;
@property (nonatomic, retain) NSMutableDictionary *bpTubeLengthList;
@property (nonatomic, retain) NSMutableDictionary *bpPressureList;
@property (nonatomic, retain) NSMutableDictionary *bpForceList;
@property (nonatomic, retain) NSMutableDictionary *bpMassList;

// Static port size
@property (nonatomic, retain) NSMutableDictionary *spTubeWidthList;
@property (nonatomic, retain) NSMutableDictionary *spTubeLengthList;
@property (nonatomic, retain) NSMutableDictionary *spPortDiameterList;

// station altitude tracking
@property (nonatomic, retain) NSMutableDictionary *atElevationAngleList;
@property (nonatomic, retain) NSMutableDictionary *atAzimuthAngleList;
@property (nonatomic, retain) NSMutableDictionary *atBaselineLengthList;
@property (nonatomic, retain) NSMutableDictionary *atFlightAltitudeList;

@property (nonatomic, retain) UIPopoverController *popOverController;

- (void) switchChanged:(id)sender forEvent:(UIEvent *)event;

@end
