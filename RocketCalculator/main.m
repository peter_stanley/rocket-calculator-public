//
//  main.m
//  RocketCalculator
//
//  Created by Peter Stanley on 11/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RocketCalculatorAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RocketCalculatorAppDelegate class]));
    }
}
