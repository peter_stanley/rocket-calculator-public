//
//  MotorSelectListViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 3/25/12.
//  Copyright 2012 Peter Stanley. All rights reserved.
//

@protocol MotorSelectListViewControllerDelegate;

@class MotorSelectListViewCell, Motor;

@interface MotorSelectListViewController : UIViewController < NSXMLParserDelegate, UITableViewDelegate, UITableViewDataSource>
{
    id <MotorSelectListViewControllerDelegate> delegate;
    NSMutableArray *motorsArray;
    NSMutableArray *allSUMotorsArray;
    NSMutableArray *allReloadsMotorsArray;
    
    Motor *selectedMotor;
    Motor *currentMotor;
    
    NSUInteger selectedRow;
    NSUInteger selectedSection;
    int currentMotorType;
    
    MotorSelectListViewCell *motorSelectListViewCell;

    IBOutlet UITableView *motorSelectListTableView;
    IBOutlet UISegmentedControl *motorTypeControl;
    
    BOOL isIpad;
        
    UIButton *doneButton;
}

@property (nonatomic, assign) id<MotorSelectListViewControllerDelegate> delegate;

@property (nonatomic, retain) NSMutableArray *motorsArray;
@property (nonatomic, retain) NSMutableArray *allSUMotorsArray;
@property (nonatomic, retain) NSMutableArray *allReloadsMotorsArray;
@property (nonatomic, retain) Motor *currentMotor;
@property (nonatomic, retain) Motor *selectedMotor;
@property (nonatomic, readwrite) NSUInteger selectedRow;
@property (nonatomic, readwrite) NSUInteger selectedSection;
@property (nonatomic, readwrite) int currentMotorType;
@property (nonatomic, assign) IBOutlet MotorSelectListViewCell *motorSelectListViewCell;
@property (nonatomic, retain) IBOutlet UITableView *motorSelectListTableView;
@property (nonatomic, retain) IBOutlet UIButton *doneButton;
@property (nonatomic, retain) IBOutlet UISegmentedControl *motorTypeControl;

- (IBAction)dismissViewController:(id)sender;
- (IBAction)changeMotorType:(id)sender;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)loadMotorsArray:(int)motorType;

@end

@protocol MotorSelectListViewControllerDelegate
- (void)motorSelectListViewControllerWasDismissed:(MotorSelectListViewController*)motorSelectListViewController;
@end

