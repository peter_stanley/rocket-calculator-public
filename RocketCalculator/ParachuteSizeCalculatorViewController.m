//
//  ParachuteSizeCalculatorViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

#import "ParachuteSizeCalculatorViewController.h"
#import "RAUnit.h"
#import "RocketCalculatorAppDelegate.h"
#import "Calculator.h"
#import "GenericFormulaViewController.h"
#import "CalcTextfield.h"

@implementation ParachuteSizeCalculatorViewController

#define MASS_TEXTFIELD 1
#define VELOCITY_TEXTFIELD 2
#define DIAMETER_TEXTFIELD 3
#define SPILL_HOLE_TEXTFIELD 4
#define CD_OVERRIDE_TEXTFIELD 5

#define SHAPE_CIRCLE 0
#define SHAPE_SQUARE 1
#define SHAPE_HEXAGON 2
#define SHAPE_OCTOGON 3

@synthesize elevationLabel, elevationUnitLabel, temperatureLabel, temperatureUnitLabel, airDensityLabel;
@synthesize temperature, elevation, massTextField, velocityTextField, diameterTextField, massUnitLabel, velocityUnitLabel, diameterUnitLabel, spillHoleDiameterTextField, spillHoleUnitLabel, cdOverrideTextField, scrollView, chuteTypeSegmentedControl,chuteShapeSegmentedControl, chuteShapeLabel, descentSpeedButton, tempElevationButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)registerForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unRegisterForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardDidShowNotification 
                                                  object:nil]; 
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];  
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    RocketCalculatorAppDelegate *appDelegate = (RocketCalculatorAppDelegate*)[[UIApplication sharedApplication] delegate];
        
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown) {
        
        CGFloat tabBarHeight = appDelegate.tabBarController.tabBar.frame.size.height;
        
        NSDictionary* info = [aNotification userInfo];
        
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        
        self.scrollView.contentInset = contentInsets;
        
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        CGRect aRect = self.view.frame;
        
        aRect.size.height -= kbSize.height;

        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height+tabBarHeight);
            
            [scrollView setContentOffset:scrollPoint animated:YES];
            
        }
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mass = 0;
    velocity = 0;
    Cd = 0;
    diameter = 0;
    chuteShape = 0;
    spillHoleDiameter = 0;

    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];

    CGFloat frameHeight = self.view.frame.size.height;
    CGFloat frameWidth = self.view.frame.size.width;
    
    UIScrollView *tempScrollView=(UIScrollView *)self.view;
    tempScrollView.contentSize=CGSizeMake(frameWidth, frameHeight * 2);

    self.title = @"Parachute Size & Descent Rate";
    
	UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateFields:)];
	gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
	[gestureRecognizer release];
    
    temperatureUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_temperature"] ofDimension:@"temperature"];
    
    elevationUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_elevation"] ofDimension:@"height"];

    massUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_rocket_mass"] ofDimension:@"weight"];
    
    diameterUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_chute_diameter"] ofDimension:@"length"];

    velocityUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_descent_velocity"] ofDimension:@"velocity"];
    
    spillHoleUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_spill_hole_diameter"] ofDimension:@"length"];

    
    if (self.velocityTextField.text == nil || [self.velocityTextField.text isEqualToString:@""]) {
        velocity = [[velocityUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:3] toUnit:velocityUnit]floatValue];
       // self.velocityTextField.text = [NSString stringWithFormat:@"%.2f", [[velocityUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:3] toUnit:velocityUnit]floatValue]];
        self.velocityTextField.text = [NSString stringWithFormat:@"%.2f", velocity];
    }

    if (self.cdOverrideTextField.text == nil || [self.cdOverrideTextField.text isEqualToString:@""] || !Cd) {
        Cd = .75;
        self.cdOverrideTextField.text = [NSString stringWithFormat:@"%.2f", Cd];
    }

    chuteShape = 0;

    // Setup toolbar
    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 75.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    // Add actionsheet button
    UIButton *exportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exportButton.frame = CGRectMake(0, 0, 25, 20); //set frame for button
    
    UIImage *buttonImage = [UIImage imageNamed:@"action2.png"];
    
    [exportButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [exportButton addTarget:self action:@selector(exportButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:exportButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    self.navigationController.navigationBar.translucent = NO;
    // Create a spacer.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    bi.width = 8.0f;
    [buttons addObject:bi];
    [bi release];
    
    // Add info button
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    infoButton.frame = CGRectMake(0, 0, 20, 20); //set frame for button
    
    buttonImage = [UIImage imageNamed:@"info_g.png"];
    
    [infoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    
    [tools setItems:buttons animated:NO];
    [buttons release];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    [tools release];
    self.navigationItem.rightBarButtonItem = twoButtons;
    
    [twoButtons release];
    
    [self resetTextfieldDisplay];
}

- (IBAction)infoButtonAction:(id)sender {
    GenericFormulaViewController *infoView = [[[GenericFormulaViewController alloc] init] autorelease];
    infoView.htmlFileName = @"parachuteSize";
    [infoView setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    //[self presentModalViewController:infoView animated:YES];
    [self presentViewController:infoView animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.massUnitLabel.text = [NSString stringWithFormat:@"(%@)", massUnit.name];
    self.massTextField.placeholder = [NSString stringWithFormat:@"%@", massUnit.label];
    
    self.velocityUnitLabel.text = [NSString stringWithFormat:@"(%@)", velocityUnit.name];
    self.velocityTextField.placeholder = [NSString stringWithFormat:@"%@", velocityUnit.label];
    
    self.diameterUnitLabel.text = [NSString stringWithFormat:@"(%@)", diameterUnit.name];
    self.diameterTextField.placeholder = [NSString stringWithFormat:@"%@", diameterUnit.label];
    
    self.spillHoleUnitLabel.text = [NSString stringWithFormat:@"(%@)", spillHoleUnit.name];
    self.spillHoleDiameterTextField.placeholder = [NSString stringWithFormat:@"%@", spillHoleUnit.label];
    
    if (self.velocityTextField.text == nil || [self.velocityTextField.text isEqualToString:@""]) {

        self.velocityTextField.text = [NSString stringWithFormat:@"%.2f", [[velocityUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:3] toUnit:velocityUnit]floatValue]];
    }

    self.elevationUnitLabel.text = [NSString stringWithFormat:@"%@", elevationUnit.name];
    self.temperatureUnitLabel.text = [NSString stringWithFormat:@"%@", temperatureUnit.name];

    temperature = [[[NSUserDefaults standardUserDefaults] stringForKey:@"temperature"] doubleValue];
    elevation = [[[NSUserDefaults standardUserDefaults] stringForKey:@"elevation"] doubleValue];
    
    self.temperatureLabel.text = [NSString stringWithFormat:@"%.1f", [[temperatureUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:temperature] toUnit:temperatureUnit]doubleValue]];
    self.elevationLabel.text = [NSString stringWithFormat:@"%.1f", [[elevationUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:elevation] toUnit:elevationUnit]doubleValue]];
    
    
    Calculator *calculator = [[Calculator alloc] init];
    // Adjust temp to kelvin
    double temperatureInKelvin = temperature + 273.15;
    airDensity = [calculator calcAirDensityFromAltitudeInMeters:elevation andTemperatureInKelvin:temperatureInKelvin];
    self.airDensityLabel.text = [NSString stringWithFormat:@"%.3f", airDensity];
    
    [calculator release];

    [self resetTextfieldDisplay];
}

- (void)willEnterForeground
{

    [[NSUserDefaults standardUserDefaults] synchronize];
    [self resetTextfieldDisplay];
    [self.view setNeedsDisplay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unRegisterForKeyboardNotifications];
}

- (void)viewDidUnload
{
    self.descentSpeedButton = nil;
    self.chuteShapeLabel = nil;
    self.chuteShapeSegmentedControl = nil;
    self.chuteTypeSegmentedControl = nil;
    self.scrollView = nil;
    self.cdOverrideTextField = nil;
    self.spillHoleUnitLabel = nil;
    self.spillHoleDiameterTextField = nil;
    self.diameterUnitLabel = nil;
    self.velocityUnitLabel = nil;
    self.massUnitLabel = nil;
    self.diameterTextField = nil;
    self.velocityTextField = nil;
    self.massTextField = nil;
    self.airDensityLabel = nil;
    self.temperatureUnitLabel = nil;
    self.temperatureLabel = nil;
    self.elevationUnitLabel = nil;
    self.elevationLabel = nil;

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc
{
    [descentSpeedButton release];
    [chuteShapeLabel release];
    [chuteShapeSegmentedControl release];
    [chuteTypeSegmentedControl release];
    [scrollView release];
    [cdOverrideTextField release];
    [spillHoleUnitLabel release];
    [spillHoleDiameterTextField release];
    [diameterUnitLabel release];
    [velocityUnitLabel release];
    [massUnitLabel release];
    [diameterTextField release];
    [velocityTextField release];
    [massTextField release];
    [airDensityLabel release];
    [temperatureUnitLabel release];
    [temperatureLabel release];
    [elevationUnitLabel release];
    [elevationLabel release];

    [super dealloc];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    if (isIpad) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    } else {
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }

    activeField = textField;
    
    if (textField.tag == MASS_TEXTFIELD) {
        
        massTextField.text = nil;
        mass = 0;
        
        diameterTextField.text = nil;
        diameter = 0;
        
        [diameterTextField setUserInteractionEnabled:NO];
        [velocityTextField setUserInteractionEnabled:NO];
        [spillHoleDiameterTextField setUserInteractionEnabled:NO];
        [cdOverrideTextField setUserInteractionEnabled:NO];

    } else if (textField.tag == VELOCITY_TEXTFIELD) {

        velocityTextField.text = nil;
        velocity = 0;
        
        diameterTextField.text = nil;
        diameter = 0;
        
        spillHoleDiameterTextField.text = nil;
        spillHoleDiameter = 0;
        
        [massTextField setUserInteractionEnabled:NO];
        [diameterTextField setUserInteractionEnabled:NO];
        [spillHoleDiameterTextField setUserInteractionEnabled:NO];
        [cdOverrideTextField setUserInteractionEnabled:NO];
        
        
    } else if (textField.tag == DIAMETER_TEXTFIELD) {
        
        diameterTextField.text = nil;
        diameter = 0;
        
        velocityTextField.text = nil;
        velocity = 0;
        
        [massTextField setUserInteractionEnabled:NO];
        [velocityTextField setUserInteractionEnabled:NO];
        [spillHoleDiameterTextField setUserInteractionEnabled:NO];
        [cdOverrideTextField setUserInteractionEnabled:NO];

    } else if (textField.tag == SPILL_HOLE_TEXTFIELD) {
        
        spillHoleDiameterTextField.text = nil;
        spillHoleDiameter = 0;
        
        velocityTextField.text = nil;
        velocity = 0;
        
        [massTextField setUserInteractionEnabled:NO];
        [diameterTextField setUserInteractionEnabled:NO];
        [velocityTextField setUserInteractionEnabled:NO];
        [cdOverrideTextField setUserInteractionEnabled:NO];

        
    } else if (textField.tag == CD_OVERRIDE_TEXTFIELD) {
        
        cdOverrideTextField.text = nil;
        Cd = 0;
        
        velocityTextField.text = nil;
        velocity = 0;
        
        [massTextField setUserInteractionEnabled:NO];
        [diameterTextField setUserInteractionEnabled:NO];
        [velocityTextField setUserInteractionEnabled:NO];
        [spillHoleDiameterTextField setUserInteractionEnabled:NO];

    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    activeField = nil;
    
}
- (IBAction)updateFields:(id)sender
{

    [self resignAllResponders];
    [self enableTextfields];

    if (!mass) {
        
        mass = [massTextField.text doubleValue];
        
    }

    if (!velocity) {
        
        velocity = [velocityTextField.text doubleValue];
        
    }

    if (!diameter) {
        
        diameter = [diameterTextField.text doubleValue];
        
    }
    
    if (!spillHoleDiameter && ([spillHoleDiameterTextField.text isEqualToString:@""] || spillHoleDiameterTextField.text == nil )) {
        
        spillHoleDiameter = 0;

    } else {
        
        spillHoleDiameter = [spillHoleDiameterTextField.text doubleValue];

    }

    if (!Cd && ([cdOverrideTextField.text isEqualToString:@""] || cdOverrideTextField.text == nil )) {
        
        Cd = .75;
        
    } else {
        
        Cd = [cdOverrideTextField.text doubleValue];
    }

    
    // get base units
    double baseMass = [[massUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:mass]] doubleValue];
    
    double baseVelocity = [[velocityUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:velocity]] doubleValue];

    double baseDiameter = [[diameterUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:diameter]] doubleValue];

    double baseSpillHoleDiameter = [[spillHoleUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:spillHoleDiameter]] doubleValue];

    Calculator *calculator = [[Calculator alloc] init];
    
    double calcArea = [calculator calcChuteAreaFromDiameter:baseDiameter andShape:chuteShape];
    double calcSpillHoleArea = [calculator calcSpillHoleAreaFromDiameter:baseSpillHoleDiameter];

    calcArea = calcArea - calcSpillHoleArea;
    
    double calcDiameter = [calculator calcChuteDiameterFromMass:baseMass andVelocity:baseVelocity andCd:Cd andShape:chuteShape andAirDensity:airDensity];
    double calcVelocity = [calculator calcDescentVelocityFromMass:baseMass andArea:calcArea andCd:Cd andAirDensity:airDensity];
    
    
    // If mass was entered, calculate diameter
    
    if (!diameter && mass) {
        // calculate diameter
        
        diameter = [[diameterUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcDiameter] toUnit:diameterUnit]floatValue];
        
        diameterTextField.text = [NSString stringWithFormat:@"%.2f", diameter];
        
        // If diameter was entered calc descent rate  
    } else if (!velocity && diameter) {

        velocity = [[velocityUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcVelocity] toUnit:velocityUnit]floatValue];
        
        velocityTextField.text = [NSString stringWithFormat:@"%.2f", velocity];

    }
    
    [calculator release];
    
    [self resetTextfieldDisplay];
}

- (IBAction)changeChuteType:(id)sender
{
	switch (((UISegmentedControl *)sender).selectedSegmentIndex)
    {
        case 0:
        {
            // Parasheet
            Cd = .75;
            self.cdOverrideTextField.text = [NSString stringWithFormat:@"%.2f", Cd];
            
            // show chute shape control
            chuteShapeLabel.hidden = NO;
            chuteShapeSegmentedControl.hidden = NO;
            
			break;
            
        } 
        case 1:
        {
            // Dome
                
            Cd = 1.5;
            self.cdOverrideTextField.text = [NSString stringWithFormat:@"%.2f", Cd];
            
            // hide chute shape control
            chuteShapeLabel.hidden = YES;
            chuteShapeSegmentedControl.hidden = YES;
            chuteShapeSegmentedControl.selectedSegmentIndex = SHAPE_CIRCLE;
            
            break;
        } 

        default:
        {
                
            Cd = .75;
            self.cdOverrideTextField.text = [NSString stringWithFormat:@"%.2f", Cd];
 
            chuteShapelabel.hidden = YES;
            chuteShapeSegmentedControl.hidden = NO;
            
            break;
        } 
    }
	
    diameterTextField.text = @"";
    diameter = 0;
    
    [self updateFields:nil];
	
}

- (IBAction)changeChuteShape:(id)sender
{
	switch (((UISegmentedControl *)sender).selectedSegmentIndex)
    {
        case SHAPE_CIRCLE:
        {
            chuteShape = SHAPE_CIRCLE;
			            
        } 
            break;
            
        case SHAPE_SQUARE:
        {
            chuteShape = SHAPE_SQUARE;
        } 
            break;
        
        case SHAPE_HEXAGON:
        {
            chuteShape = SHAPE_HEXAGON;
        }
            break;
            
        case SHAPE_OCTOGON:
        {
            
            chuteShape = SHAPE_OCTOGON;
        }
        default:
        {
            
            break;
        } 
    }
	
    diameterTextField.text = @"";
    diameter = 0;
    
    [self updateFields:nil];
	
}

- (IBAction)resetDescentSpeed:(id)sender
{
    
    self.velocityTextField.text = [NSString stringWithFormat:@"%.2f", [[velocityUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:3] toUnit:velocityUnit]floatValue]];
    velocity = [[velocityUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:3] toUnit:velocityUnit]floatValue];
    
    self.diameterTextField.text = @"";
    diameter = 0;
    
    self.chuteTypeSegmentedControl.selectedSegmentIndex = 0;
    
    self.chuteShapeSegmentedControl.selectedSegmentIndex = 0;
    
    Cd = .75;
    self.cdOverrideTextField.text = [NSString stringWithFormat:@"%.2f",Cd];
    
    self.spillHoleDiameterTextField.text = @"";
    spillHoleDiameter = 0;
    
    [self updateFields:nil];
    
}

-(IBAction) showEmailModalView:(id)sender 
{
    
    if([MFMailComposeViewController canSendMail]){
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        
        if (picker) {
            
            picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
            NSString *mailSubject = [NSString stringWithFormat:@"%@ Calculation", self.title];
            NSString *mailMsg = [self exportMsgHtml];
    
            [picker setSubject:mailSubject];
    
            [picker setMessageBody:mailMsg isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
    
            picker.navigationBar.barStyle = UIBarStyleDefault; // choose your style, unfortunately, Translucent colors behave quirky.
    
           // [self presentModalViewController:picker animated:YES];
            [self presentViewController:picker animated:YES completion:nil];
            [picker release];
            
        }
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Mail Accounts" message:@"Please set up a Mail account in order to send email." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        
        
    }

    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{ 
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-("
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
            
            break;
    }
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)exportButtonAction:(id)sender
{
    if (![UIPrintInteractionController isPrintingAvailable]) {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Email Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", nil];
        
    } else {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Export Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", @"Print", nil];
    }
    // Show the sheet
    [sheet showFromTabBar:self.tabBarController.tabBar];
    [sheet release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self showEmailModalView:nil];
            break;
            
        case 1:
            [self printCalculation:nil];
            break;
            
        default:
            break;
    }
}

#pragma mark -

#pragma mark Printing



// Print button handler that sends our print instructions to the OS

- (IBAction)printCalculation:(id)sender {
    
    // Get a reference to the singleton iOS printing concierge
    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    
    if(pic && [UIPrintInteractionController isPrintingAvailable]) {
        //  UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = self.title;
        pic.printInfo = printInfo;
        
        NSString *msg = [self exportMsgText];

        UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc]
                                                     initWithText:msg];
        textFormatter.startPage = 0;
        textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
        textFormatter.maximumContentWidth = 6 * 72.0;
        pic.printFormatter = textFormatter;
        [textFormatter release];
        pic.showsPageRange = YES;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSLog(@"Printing could not complete because of error: %@", error);
            }
        };
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [pic presentFromRect:self.tabBarController.tabBar.frame inView:self.view animated:YES completionHandler:completionHandler];
        } else {
            [pic presentAnimated:YES completionHandler:completionHandler];
        }
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Printing Error" message:@"Your device doesn't support printing" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    
}

- (NSString *) exportMsgText
{
    NSString *chuteShapeString = [chuteShapeSegmentedControl titleForSegmentAtIndex:chuteShapeSegmentedControl.selectedSegmentIndex];
    
    NSString *chuteTypeString = [chuteTypeSegmentedControl titleForSegmentAtIndex:chuteTypeSegmentedControl.selectedSegmentIndex];
    
    NSString *massString = massTextField.text;
    NSString *massUnitString = massUnit.name;
    
    NSString *diameterString = diameterTextField.text;
    NSString *diameterUnitString = diameterUnit.name;
    
    NSString *velocityString = velocityTextField.text;
    NSString *velocityUnitString = velocityUnit.name;
    
    NSString *spillHoleString = spillHoleDiameterTextField.text;
    NSString *spillHoleUnitString = spillHoleUnit.name;
    
    NSString *cDString = cdOverrideTextField.text;

    NSString *title = self.title;
    
    NSString *msg = [NSString stringWithFormat:@"%@\n\nRocket Mass: %@ (%@)\nChute Diameter: %@ (%@)\nSpill Hole Diameter: %@ (%@)\nCd: %@\nDescent Rate: %@ (%@)\nChute Type: %@\nChute Shape: %@\n\n\nGenerated by Rocket Calculator\n\nAvailable on the Apple App Store",title, massString, massUnitString, diameterString, diameterUnitString, spillHoleString, spillHoleUnitString, cDString, velocityString, velocityUnitString, chuteTypeString, chuteShapeString];
    
    
    return msg;
}

- (NSString *) exportMsgHtml
{
    NSString *chuteShapeString = [chuteShapeSegmentedControl titleForSegmentAtIndex:chuteShapeSegmentedControl.selectedSegmentIndex];
    
    NSString *chuteTypeString = [chuteTypeSegmentedControl titleForSegmentAtIndex:chuteTypeSegmentedControl.selectedSegmentIndex];
    
    NSString *massString = massTextField.text;
    NSString *massUnitString = massUnit.name;
    
    NSString *diameterString = diameterTextField.text;
    NSString *diameterUnitString = diameterUnit.name;
    
    NSString *velocityString = velocityTextField.text;
    NSString *velocityUnitString = velocityUnit.name;
    
    NSString *spillHoleString = spillHoleDiameterTextField.text;
    NSString *spillHoleUnitString = spillHoleUnit.name;
    
    NSString *cDString = cdOverrideTextField.text;
    
    NSString *title = self.title;
    
    NSString *msg = [NSString stringWithFormat:@"%@<br /><br />Rocket Mass: %@ (%@)<br />Chute Diameter: %@ (%@)<br />Spill Hole Diameter: %@ (%@)<br />Cd: %@<br />Descent Rate: %@ (%@)<br />Chute Type: %@<br />Chute Shape: %@<br /><br /><br />Generated by Rocket Calculator<br />Available on the Apple <a href=\"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=484673201&mt=8\">App Store</a>",title, massString, massUnitString, diameterString, diameterUnitString, spillHoleString, spillHoleUnitString, cDString, velocityString, velocityUnitString, chuteTypeString, chuteShapeString];
    
    
    return msg;
    
}

- (void)enableTextfields
{
    
    [massTextField setUserInteractionEnabled:YES];
    [diameterTextField setUserInteractionEnabled:YES];
    [velocityTextField setUserInteractionEnabled:YES];
    [spillHoleDiameterTextField setUserInteractionEnabled:YES];
    [cdOverrideTextField setUserInteractionEnabled:YES];
    
}

- (void) resignAllResponders
{
    
    [massTextField resignFirstResponder];
    [diameterTextField resignFirstResponder];
    [velocityTextField resignFirstResponder];
    [spillHoleDiameterTextField resignFirstResponder];
    [cdOverrideTextField resignFirstResponder];
    
}

- (void) resetTextfieldDisplay
{
    // Tint required fields
    
    if (massTextField.text == nil || [massTextField.text isEqualToString:@""] || [massTextField.text doubleValue] == 0) {
        
        self.massTextField.text = nil;
        [self.massTextField setRequired:YES];
    } else {
        
        [self.massTextField setRequired:NO];
    }
    
    if (diameterTextField.text == nil || [diameterTextField.text isEqualToString:@""] || [diameterTextField.text doubleValue] == 0) {
    
        self.diameterTextField.text = nil;
        [self.diameterTextField setRequired:YES];
        [self.diameterTextField setUserInteractionEnabled:YES];
    
    } else {
    
        [self.diameterTextField setRequired:NO];
        [self.diameterTextField setUserInteractionEnabled:YES];
    
    }

    if ((diameterTextField.text == nil || [diameterTextField.text isEqualToString:@""] || [diameterTextField.text doubleValue] == 0) && (!massTextField.text || !mass) ) {

        [self.diameterTextField setRequired:NO];
        [self.diameterTextField setUserInteractionEnabled:NO];
    }
    
    if (velocityTextField.text == nil || [velocityTextField.text isEqualToString:@""] || [velocityTextField.text doubleValue] == 0) {
        
        self.velocityTextField.text = nil;
        [self.velocityTextField setRequired:YES];
    } else {
        
        [self.velocityTextField setRequired:NO];
    }
    
    if (cdOverrideTextField.text == nil || [cdOverrideTextField.text isEqualToString:@""] || [cdOverrideTextField.text doubleValue] == 0) {
        
        self.cdOverrideTextField.text = nil;
        [self.cdOverrideTextField setRequired:YES];
    } else {
        
        [self.cdOverrideTextField setRequired:NO];
    }

    [self.spillHoleDiameterTextField setRequired:NO];
    
}

- (IBAction)openDensityCalculatorViewController:(id)sender
{
    
    DensityCalculatorViewController *densityVC = [[DensityCalculatorViewController alloc]init];
    
    [densityVC setDelegate:self];
    
    //[self presentModalViewController:densityVC animated:YES];
    [self presentViewController:densityVC animated:YES completion:nil];
    
    [densityVC release];
    
}

- (void)densityCalculatorViewControllerWasDismissed:(DensityCalculatorViewController*)densityCalculatorViewController
{
    
    temperature = [[[NSUserDefaults standardUserDefaults] stringForKey:@"temperature"] doubleValue];
    elevation = [[[NSUserDefaults standardUserDefaults] stringForKey:@"elevation"] doubleValue];

    Calculator *calculator = [[Calculator alloc] init];
    // Adjust temp to kelvin
    double temperatureInKelvin = temperature + 273.15;
    airDensity = [calculator calcAirDensityFromAltitudeInMeters:elevation andTemperatureInKelvin:temperatureInKelvin];
    self.airDensityLabel.text = [NSString stringWithFormat:@"%.3f", airDensity];
    
    [calculator release];

    velocity = 0;
    self.velocityTextField.text = nil;
    [self updateFields:nil];
    
}


@end

