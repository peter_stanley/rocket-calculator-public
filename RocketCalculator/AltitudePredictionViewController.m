//
//  AltitudePredictionViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AltitudePredictionViewController.h"
#import "RAUnit.h"
#import "RocketCalculatorAppDelegate.h"
#import "Calculator.h"
#import "GenericFormulaViewController.h"
#import "CalcTextfield.h"
#import "SingleSelectListViewController.h"
#import "MotorSelectListViewController.h"
#import "Motor.h"

@implementation AltitudePredictionViewController

#define TOTAL_MOTOR_MASS_TEXTFIELD 1
#define AVG_THRUST_TEXTFIELD 2
#define BURN_TIME_TEXTFIELD 3
#define PROPELLANT_MASS_TEXTFIELD 4
#define ROCKET_MASS_TEXTFIELD 5
#define NUMBER_MOTORS_TEXTFIELD 6
#define DRAG_COEFFICIENT_TEXTFIELD 7
#define AREA_TEXTFIELD 8

#define REQUIRED_COLOR redColor

@synthesize bodyTubesArray, bodyTubeButton, motorButton;
@synthesize temperature, elevation, totalMotorMass, motorCaseMass, propellentMass, avgThrust, burnTime, frontalArea, rocketMass, cd, currentMotorType, numberMotors;

@synthesize scrollView;
@synthesize elevationLabel, elevationUnitLabel, temperatureLabel, temperatureUnitLabel, airDensityLabel;
@synthesize thrustToWeightLabel, motorClassLabel, motorCaseMassUnitLabel, motorCaseMassTextfield, totalMotorMassUnitLabel, totalMotorMassTextfield, avgThrustUnitLabel,avgThrustTextfield;
@synthesize burnTimeUnitLabel, burnTimeTextfield, propellantMassUnitLabel, propellantMassTextfield;
@synthesize rocketMassUnitLabel, rocketMassTextfield, numberMotorsTextfield, dragCoefficientTextfield;
@synthesize frontalAreaUnitLabel, frontalAreaTextfield, burnoutVelocityValueLabel, burnoutVelocityUnitLabel;
@synthesize burnoutAltitudeValueLabel, burnoutAltitudeUnitLabel, peakAltitudeValueLabel, peakAltitudeUnitLabel;
@synthesize coastTimeValueLabel, coastTimeUnitLabel, dataAssistantButton, tempElevationButton, motorCodeLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)registerForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unRegisterForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardDidShowNotification 
                                                  object:nil]; 
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];  
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    RocketCalculatorAppDelegate *appDelegate = (RocketCalculatorAppDelegate*)[[UIApplication sharedApplication] delegate];
    

    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown) {
        
        CGFloat tabBarHeight = appDelegate.tabBarController.tabBar.frame.size.height;
        
        NSDictionary* info = [aNotification userInfo];
        
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        
        self.scrollView.contentInset = contentInsets;
        
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        CGRect aRect = self.view.frame;
        
        aRect.size.height -= kbSize.height;
        
            if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height+tabBarHeight);
            
            [scrollView setContentOffset:scrollPoint animated:YES];
            
        }
        
    }

}

// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.bodyTubesArray = [NSArray array];

    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];
    
    motorCaseMass =0;
    avgThrust = 0;
    burnTime = 0;
    propellentMass = 0;
    rocketMass = 0;
    frontalArea = 0;
    
    if (!motorCode || [motorCode isEqualToString:@""]) {
        
        self.motorCodeLabel.text = @"";
        
    } else {
        
        self.motorCodeLabel.text = motorCode;
        
    }
    
    CGFloat frameHeight = self.view.frame.size.height;
    CGFloat frameWidth = self.view.frame.size.width;
    
    scrollView.contentSize=CGSizeMake(frameWidth, frameHeight * 2);
    self.title = @"Altitude Prediction";
    
	UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateFields:)];
	gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
	[gestureRecognizer release];
    // units setup
    
    temperatureUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_temperature"] ofDimension:@"temperature"];
    
    elevationUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_elevation"] ofDimension:@"height"];
    
    avgThrustUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_avg_thrust"] ofDimension:@"force"];

    burnTimeUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_burn_time"] ofDimension:@"time"];

    motorMassUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_motor_mass"] ofDimension:@"weight"];
    
    rocketMassUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_rocket_mass"] ofDimension:@"weight"];
    
    propellantMassUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_prop_mass"] ofDimension:@"weight"];
    
    caseMassUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_case_mass"] ofDimension:@"weight"];
    
    areaUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_area"] ofDimension:@"areaSmall"];
    
    velocityUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_velocity"] ofDimension:@"velocity"];
    
    altitudeUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_altitude"] ofDimension:@"height"];
    
    coastTimeUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_coast_time"] ofDimension:@"time"];
    
    self.numberMotorsTextfield.text = [NSString stringWithFormat:@"%d",1];
    
    if (self.dragCoefficientTextfield.text == nil || [self.dragCoefficientTextfield.text isEqualToString:@""] || !cd) {
        cd = .75;
        
        self.dragCoefficientTextfield.text = [NSString stringWithFormat:@"%f", cd];
    }
    
    
    // Setup toolbar
    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 75.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    //tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    // Add actionsheet button
    UIButton *exportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exportButton.frame = CGRectMake(0, 0, 25, 20); //set frame for button
    
    UIImage *buttonImage = [UIImage imageNamed:@"action2.png"];
    
    [exportButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [exportButton addTarget:self action:@selector(exportButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:exportButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    self.navigationController.navigationBar.translucent = NO;
    [buttons addObject:modalButton];
    [modalButton release];
    
    // Create a spacer.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    bi.width = 8.0f;
    [buttons addObject:bi];
    [bi release];
    
    // Add info button
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    infoButton.frame = CGRectMake(0, 0, 20, 20); //set frame for button
    
    buttonImage = [UIImage imageNamed:@"info_g.png"];
    
    [infoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    
    [tools setItems:buttons animated:NO];
    [buttons release];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    [tools release];
    self.navigationItem.rightBarButtonItem = twoButtons;
    
    [twoButtons release];
    
    [self resetTextfieldDisplay];
}

- (IBAction)infoButtonAction:(id)sender {
    GenericFormulaViewController *infoView = [[[GenericFormulaViewController alloc] init] autorelease];
    infoView.htmlFileName = @"altitudePrediction";
    [infoView setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
   // [self presentModalViewController:infoView animated:YES];
    [self presentViewController:infoView animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!motorCode || [motorCode isEqualToString:@""]) {
        
        self.motorCodeLabel.text = @"";
        
    } else {
        
        self.motorCodeLabel.text = motorCode;
        
    }

    [self registerForKeyboardNotifications];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.bodyTubesArray = [self loadBodyTubesArray];
    
    self.totalMotorMassUnitLabel.text = [NSString stringWithFormat:@"(%@)", motorMassUnit.name];
    self.totalMotorMassTextfield.placeholder = [NSString stringWithFormat:@"%@", motorMassUnit.label];
    
    if (totalMotorMass) {
        self.totalMotorMassTextfield.text = [NSString stringWithFormat:@"%.3f", totalMotorMass];
    } else {
        self.totalMotorMassTextfield.text = nil;
    }

    
    self.propellantMassUnitLabel.text = [NSString stringWithFormat:@"(%@)", propellantMassUnit.name];
    self.propellantMassTextfield.placeholder = [NSString stringWithFormat:@"%@", propellantMassUnit.label];
    
    if (propellentMass){
        self.propellantMassTextfield.text = [NSString stringWithFormat:@"%.3f", propellentMass];
    } else {
        self.propellantMassTextfield.text = nil;
    }

    self.avgThrustUnitLabel.text = [NSString stringWithFormat:@"(%@)", avgThrustUnit.name];
    self.avgThrustTextfield.placeholder = [NSString stringWithFormat:@"%@",avgThrustUnit.label];
    
    if (avgThrust) {
        self.avgThrustTextfield.text = [NSString stringWithFormat:@"%.2f", avgThrust];
    } else {
        self.avgThrustTextfield.text = nil;
    }
    
    self.burnTimeUnitLabel.text = [NSString stringWithFormat:@"(%@)", burnTimeUnit.name];
    self.burnTimeTextfield.placeholder = [NSString stringWithFormat:@"%@", burnTimeUnit.label];
    
    if (burnTime) {
        self.burnTimeTextfield.text = [NSString stringWithFormat:@"%.2f", burnTime];
    } else {
        self.burnTimeTextfield.text = nil;
    }
    
    self.rocketMassUnitLabel.text = [NSString stringWithFormat:@"(%@)", rocketMassUnit.name];
    self.rocketMassTextfield.placeholder = [NSString stringWithFormat:@"%@", rocketMassUnit.label];
    
    if (rocketMass) {
        self.rocketMassTextfield.text = [NSString stringWithFormat:@"%.2f", rocketMass];
    } else {
        self.rocketMassTextfield.text = nil;
    }
    
    self.frontalAreaUnitLabel.text = [NSString stringWithFormat:@"(%@)", areaUnit.name];
    self.frontalAreaTextfield.placeholder = [NSString stringWithFormat:@"%@", areaUnit.label];
    
    if (frontalArea) {
        self.frontalAreaTextfield.text = [NSString stringWithFormat:@"%.4f", frontalArea];
    } else {
        self.frontalAreaTextfield.text = nil;
    }
    
    if (numberMotors) {
        self.numberMotorsTextfield.text = [NSString stringWithFormat:@"%d", numberMotors];
    } else {
        self.numberMotorsTextfield.text = [NSString stringWithFormat:@"%d", 1];
    }
    
    if (cd && self.dragCoefficientTextfield.text != nil) {
        self.dragCoefficientTextfield.text = [NSString stringWithFormat:@"%.2f", cd];
    } else {
       // self.dragCoefficientTextfield.text = [NSString stringWithFormat:@"%.2f", .75];
        self.dragCoefficientTextfield.text = [NSString stringWithFormat:@"%.2f", 0.0];
    }
    
    self.burnoutVelocityValueLabel.text = [NSString stringWithFormat:@"%.2f", calcBurnoutVelocity];
    self.burnoutVelocityUnitLabel.text = [NSString stringWithFormat:@"%@", velocityUnit.name];
    
    self.burnoutAltitudeValueLabel.text = [NSString stringWithFormat:@"%.2f", calcBurnoutAltitude];
    self.burnoutAltitudeUnitLabel.text = [NSString stringWithFormat:@"%@", altitudeUnit.name];
    
    self.peakAltitudeValueLabel.text = [NSString stringWithFormat:@"%.2f", calcPeakAltitude];
    self.peakAltitudeUnitLabel.text = [NSString stringWithFormat:@"%@", altitudeUnit.name];
    
    self.coastTimeValueLabel.text = [NSString stringWithFormat:@"%.2f", calcCoastTime];
    self.coastTimeUnitLabel.text = [NSString stringWithFormat:@"%@", coastTimeUnit.name];
    
    self.elevationUnitLabel.text = [NSString stringWithFormat:@"%@", elevationUnit.name];
    self.temperatureUnitLabel.text = [NSString stringWithFormat:@"%@", temperatureUnit.name];

    temperature = [[[NSUserDefaults standardUserDefaults] stringForKey:@"temperature"] doubleValue];
    elevation = [[[NSUserDefaults standardUserDefaults] stringForKey:@"elevation"] doubleValue];
    
    self.temperatureLabel.text = [NSString stringWithFormat:@"%.1f", [[temperatureUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:temperature] toUnit:temperatureUnit]doubleValue]];
    self.elevationLabel.text = [NSString stringWithFormat:@"%.1f", [[elevationUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:elevation] toUnit:elevationUnit]doubleValue]];
    
    
    Calculator *calculator = [[Calculator alloc] init];
    // Adjust temp to kelvin
    double temperatureInKelvin = temperature + 273.15;
    airDensity = [calculator calcAirDensityFromAltitudeInMeters:elevation andTemperatureInKelvin:temperatureInKelvin];
    self.airDensityLabel.text = [NSString stringWithFormat:@"%.3f", airDensity];

    [calculator release];
    
    [self updateFields:nil];
    [self resetTextfieldDisplay];

}

- (void)willEnterForeground
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self resetTextfieldDisplay];
    [self.view setNeedsDisplay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unRegisterForKeyboardNotifications];
}

- (void)viewDidUnload
{
    
    self.motorCodeLabel = nil;
    self.tempElevationButton = nil;
    self.dataAssistantButton = nil;
    
    self.coastTimeUnitLabel = nil;
    self.coastTimeValueLabel = nil;
    
    self.peakAltitudeUnitLabel = nil;
    self.peakAltitudeValueLabel = nil;
    
    self.burnoutAltitudeUnitLabel = nil;
    self.burnoutAltitudeValueLabel = nil;
    
    self.burnoutVelocityUnitLabel = nil;
    self.burnoutVelocityValueLabel = nil;
    
    self.frontalAreaTextfield = nil;
    self.frontalAreaUnitLabel = nil;
    
    self.dragCoefficientTextfield = nil;
    
    self.numberMotorsTextfield = nil;
    
    self.rocketMassTextfield = nil;
    self.rocketMassUnitLabel = nil;
    
    self.propellantMassTextfield = nil;
    self.propellantMassUnitLabel = nil;
    
    self.burnTimeTextfield = nil;
    self.burnTimeUnitLabel = nil;
    
    self.avgThrustTextfield = nil;
    self.avgThrustUnitLabel = nil;
    
    self.totalMotorMassTextfield = nil;
    self.totalMotorMassUnitLabel = nil;
    
    self.motorCaseMassTextfield = nil;
    self.motorCaseMassUnitLabel = nil;
    
    self.motorClassLabel = nil;
    self.thrustToWeightLabel = nil;
    
    self.airDensityLabel = nil;
    self.temperatureUnitLabel = nil;
    self.temperatureLabel = nil;
    self.elevationUnitLabel = nil;
    self.elevationLabel = nil;
    
    self.scrollView = nil;
    
    self.motorButton = nil;
    self.bodyTubeButton = nil;
    self.bodyTubesArray = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc
{

    [motorCodeLabel release];
    [tempElevationButton release];
    [dataAssistantButton release];
    
    [coastTimeUnitLabel release];
    [coastTimeValueLabel release];
    
    [peakAltitudeUnitLabel release];
    [peakAltitudeValueLabel release];
    
    [burnoutAltitudeUnitLabel release];
    [burnoutAltitudeValueLabel release];
    
    [burnoutVelocityUnitLabel release];
    [burnoutVelocityValueLabel release];
    
    [frontalAreaTextfield release];
    [frontalAreaUnitLabel release];
    
    [dragCoefficientTextfield release];
    
    [numberMotorsTextfield release];
    
    [rocketMassTextfield release];
    [rocketMassUnitLabel release];
    
    [propellantMassTextfield release];
    [propellantMassUnitLabel release];
    
    [burnTimeTextfield release];
    [burnTimeUnitLabel release];
    
    [avgThrustTextfield release];
    [avgThrustUnitLabel release];
    
    [totalMotorMassTextfield release];
    [totalMotorMassUnitLabel release];
    
    [motorCaseMassTextfield release];
    [motorCaseMassUnitLabel release];
    
    [motorClassLabel release];
    [thrustToWeightLabel release];
    
    [airDensityLabel release];
    [temperatureUnitLabel release];
    [temperatureLabel release];
    [elevationUnitLabel release];
    [elevationLabel release];
    
    [scrollView release];
   
    [motorButton release];
    [bodyTubeButton release];
    [bodyTubesArray release];
    
    [super dealloc];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{

    if (isIpad) {
        
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        
    } else {
        
        textField.keyboardType = UIKeyboardTypeDecimalPad;
        
    }
    
    activeField = textField;
    
    if (textField.tag == TOTAL_MOTOR_MASS_TEXTFIELD) {
        totalMotorMassTextfield.text = nil;
        totalMotorMass = 0;
        
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [rocketMassTextfield setUserInteractionEnabled:NO];
        [numberMotorsTextfield setUserInteractionEnabled:NO];
        [dragCoefficientTextfield setUserInteractionEnabled:NO];
        [frontalAreaTextfield setUserInteractionEnabled:NO];

    }
    
    if (textField.tag == PROPELLANT_MASS_TEXTFIELD) {
        propellantMassTextfield.text = nil;
        propellentMass = 0;
        
        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [rocketMassTextfield setUserInteractionEnabled:NO];
        [numberMotorsTextfield setUserInteractionEnabled:NO];
        [dragCoefficientTextfield setUserInteractionEnabled:NO];
        [frontalAreaTextfield setUserInteractionEnabled:NO];
        
    }

    if (textField.tag == AVG_THRUST_TEXTFIELD) {
        avgThrustTextfield.text = nil;
        avgThrust = 0;
        
        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [rocketMassTextfield setUserInteractionEnabled:NO];
        [numberMotorsTextfield setUserInteractionEnabled:NO];
        [dragCoefficientTextfield setUserInteractionEnabled:NO];
        [frontalAreaTextfield setUserInteractionEnabled:NO];
        
    }

    if (textField.tag == BURN_TIME_TEXTFIELD) {
        burnTimeTextfield.text = nil;
        burnTime = 0;
        
        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [rocketMassTextfield setUserInteractionEnabled:NO];
        [numberMotorsTextfield setUserInteractionEnabled:NO];
        [dragCoefficientTextfield setUserInteractionEnabled:NO];
        [frontalAreaTextfield setUserInteractionEnabled:NO];
        
    }

    if (textField.tag == ROCKET_MASS_TEXTFIELD) {
        rocketMassTextfield.text = nil;
        rocketMass = 0;
        
        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [numberMotorsTextfield setUserInteractionEnabled:NO];
        [dragCoefficientTextfield setUserInteractionEnabled:NO];
        [frontalAreaTextfield setUserInteractionEnabled:NO];
        
    }
    
    if (textField.tag == NUMBER_MOTORS_TEXTFIELD) {
        numberMotorsTextfield.text = nil;
        numberMotors = 0;
        
        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [rocketMassTextfield setUserInteractionEnabled:NO];
        [dragCoefficientTextfield setUserInteractionEnabled:NO];
        [frontalAreaTextfield setUserInteractionEnabled:NO];
        
    }

    if (textField.tag == DRAG_COEFFICIENT_TEXTFIELD) {
        dragCoefficientTextfield.text = nil;
        cd = 0;
        
        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [rocketMassTextfield setUserInteractionEnabled:NO];
        [numberMotorsTextfield setUserInteractionEnabled:NO];
        [frontalAreaTextfield setUserInteractionEnabled:NO];
        
    }
    
    if (textField.tag == AREA_TEXTFIELD) {
        frontalAreaTextfield.text = nil;
        frontalArea = 0;
        
        [motorCaseMassTextfield setUserInteractionEnabled:NO];
        [propellantMassTextfield setUserInteractionEnabled:NO];
        [avgThrustTextfield setUserInteractionEnabled:NO];
        [burnTimeTextfield setUserInteractionEnabled:NO];
        [rocketMassTextfield setUserInteractionEnabled:NO];
        [numberMotorsTextfield setUserInteractionEnabled:NO];
        [dragCoefficientTextfield setUserInteractionEnabled:NO];
        
    }

    [thrustToWeightLabel setAlpha:0.25];
    
    [motorClassLabel setAlpha:0.25];
    
    [burnoutVelocityValueLabel setAlpha:0.25];
    
    [burnoutAltitudeValueLabel setAlpha:0.25];
    
    [peakAltitudeValueLabel setAlpha:0.25];
    
    [coastTimeValueLabel setAlpha:0.25];

}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    activeField = nil;
    
}

- (IBAction)updateFields:(id)sender
{
    
    [self resignAllResponders];
    [self enableTextfields];
    
    if (!totalMotorMass) {
        
        totalMotorMass = [totalMotorMassTextfield.text doubleValue];
        
    }
    
    if (!propellentMass) {
        
        propellentMass = [propellantMassTextfield.text doubleValue];

    }
    
    if (!avgThrust) {
        
        avgThrust = [avgThrustTextfield.text doubleValue];
        
    }
    
    if (!burnTime) {
        
        burnTime = [burnTimeTextfield.text doubleValue];
        
    }
    
    if (!rocketMass) {
        
        rocketMass = [rocketMassTextfield.text doubleValue];
        
    }
    
    if (!numberMotors) {
        
        numberMotors = [numberMotorsTextfield.text intValue];
        
    }
    
   // if (!cd && ([dragCoefficientTextfield.text isEqualToString:@""] || dragCoefficientTextfield.text == nil )) {
    if ([dragCoefficientTextfield.text isEqualToString:@""] || dragCoefficientTextfield.text == nil) {
       
       // cd = 0;
        cd = .75;
        
    } else {
        
        cd = [dragCoefficientTextfield.text doubleValue];
    }

    if (!frontalArea || frontalArea == 0) {
        
        frontalArea = [frontalAreaTextfield.text doubleValue];

    }

    if (!numberMotors) {
        numberMotors = 1;
    }
    
    // Make sure all required fields are filled
    requiredCheck = 0;
    
    if (!totalMotorMass || !avgThrust || !burnTime || !propellentMass || ! rocketMass || !frontalArea) {
    
        requiredCheck = 0;
        
    } else {
        
        requiredCheck = 1;
        
    }
    
    if (requiredCheck) {
        
        // Get base units
 
        double baseTotalMotorMass = [[motorMassUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:totalMotorMass]] doubleValue];

        baseTotalMotorMass = baseTotalMotorMass * numberMotors;

        double basePropellantMass = [[propellantMassUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:propellentMass]] doubleValue];

        basePropellantMass = basePropellantMass * numberMotors;
        
        double baseMotorCaseMass = baseTotalMotorMass - basePropellantMass;

        //double baseMotorCaseMass = [[caseMassUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:motorCaseMass]] doubleValue];
        
        //baseMotorCaseMass = baseMotorCaseMass * numberMotors;

        double baseAvgThrust = [[avgThrustUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:avgThrust]] doubleValue];
        baseAvgThrust = baseAvgThrust * numberMotors;
        
        double baseBurnTime = [[burnTimeUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:burnTime]] doubleValue];
        
        double baseRocketMass = [[rocketMassUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:rocketMass]] doubleValue];
        
        double baseFrontalArea = [[areaUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:frontalArea]] doubleValue];
    
        //double baseTotalMass = baseRocketMass + baseMotorCaseMass + basePropellantMass;
        double baseTotalMass = baseRocketMass + baseTotalMotorMass;
        
        NSString *calcMotorClassString = @"";
        NSString *thrustToWeightString = @"";
        
        // Calc values
        Calculator *calculator = [[Calculator alloc] init];
        
        calcK = [calculator calcKFromCd:cd andFromArea:baseFrontalArea andAirDensity:airDensity];
        
        calcAvgThrustingMass = [calculator calcAverageThrustingMassFromRocketMass:baseRocketMass andTotalMotorMass:baseTotalMotorMass andPropellantMass:basePropellantMass];
        
        calcBurnoutMass = [calculator calcBurnoutMassFromRocketMass:baseRocketMass andMotorCaseMass:baseMotorCaseMass];
        
        if (cd > 0) {
            
            calcBurnoutAltitude = [calculator calcBurnoutAltitudeFromAvgThrustingMass:calcAvgThrustingMass andK:calcK andAvgThrust:baseAvgThrust andBurnTime:baseBurnTime];

            calcBurnoutVelocity = [calculator calcBurnoutVelocityFromAvgThrustingMass:calcAvgThrustingMass andK:calcK andAvgThrust:baseAvgThrust andBurnTime:baseBurnTime];
            
            calcCoastAltitude = [calculator calcCoastAltitudeFromBurnoutMass:calcBurnoutMass andK:calcK andBurnoutVelocity:calcBurnoutVelocity];

            calcPeakAltitude = [calculator calcPeakAltitudeFromBurnoutAltitude:calcBurnoutAltitude andCoastAltitude:calcCoastAltitude];
            
            calcCoastTime = [calculator calcCoastTimeFromBurnoutMass:calcBurnoutMass andBurnoutVelocity:calcBurnoutVelocity andK:calcK];
            

            
        } else {

            calcBurnoutAltitude = [calculator calcBurnoutAltitudeZeroDragFromAvgThrustingMass:calcAvgThrustingMass andAvgThrust:baseAvgThrust andBurnTime:baseBurnTime];
            
            calcBurnoutVelocity = [calculator calcBurnoutVelocityZeroDragFromAvgThrustingMass:calcAvgThrustingMass andAvgThrust:baseAvgThrust andBurnoutAltitude:calcBurnoutAltitude];
            
            calcPeakAltitude = [calculator calcPeakAltitudeZeroDragFromBurnoutAltitude:calcBurnoutAltitude andAvgThrustingMass:calcAvgThrustingMass andAvgThrust:baseAvgThrust];
            
            calcCoastTime = [calculator calcCoastTimeZeroDragFromBurnoutAltitude:calcBurnoutAltitude andPeakAltitude:calcPeakAltitude];

        }
        
                
        
        double calcBaseMaxMass = [calculator calcMaxMassForThrust:baseAvgThrust andRatio:5];
        
        double calcBaseTotalImpulse = [calculator calcTotalImpulseFromAvgThrust:baseAvgThrust andBurnTime:baseBurnTime];
        
        double thrustToWeight = [calculator calcThrustToWeightRatioFromThrust:baseAvgThrust andWeight:baseTotalMass];
        
        calcMotorClassString = [calculator calcMotorClassFromTotalImpulse:calcBaseTotalImpulse];
        
        thrustToWeightString = [NSString stringWithFormat:@"%.1f : 1", thrustToWeight];
        
        [calculator release];
        
        // 340.29 is Mach 1 in m/s
        if (calcBurnoutVelocity >= 340.29) {
            
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"enableMachAlert"]) {
            
               // [self showAlert:@"This is a transonic flight. The results may be inaccurate. See the info screen for more information."];
                [self showAlert:@"This is a transonic flight. The results may be inaccurate. See the info screen for more information." fromSendingAlert:@"enableMachAlert"];
            }
        }
        
        // Set values
        
        motorClassLabel.text = calcMotorClassString;

        if (thrustToWeight >= 5) {
            
            thrustToWeightLabel.textColor = [UIColor greenColor];
            
        } else if (calcBaseMaxMass < (baseRocketMass + baseMotorCaseMass + basePropellantMass)) {
            
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"enableTWTAlert"]) {
                
                //[self showAlert:@"Thrust to Weight ratio is less than 5."];
                [self showAlert:@"Thrust to Weight ratio is less than 5." fromSendingAlert:@"enableTWTAlert"];
                
            }
            
            thrustToWeightLabel.textColor = [UIColor redColor];
            
        }

        thrustToWeightLabel.text = thrustToWeightString;
        
        burnoutVelocityValueLabel.text = [NSString stringWithFormat:@"%.2f", [[velocityUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcBurnoutVelocity] toUnit:velocityUnit]doubleValue]];
        
        burnoutAltitudeValueLabel.text = [NSString stringWithFormat:@"%.2f", [[altitudeUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcBurnoutAltitude] toUnit:altitudeUnit]doubleValue]];
        
        peakAltitudeValueLabel.text = [NSString stringWithFormat:@"%.2f", [[altitudeUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcPeakAltitude] toUnit:altitudeUnit]doubleValue]];
        
        coastTimeValueLabel.text = [NSString stringWithFormat:@"%.2f", [[coastTimeUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcCoastTime] toUnit:coastTimeUnit]doubleValue]];

    } else {
        
        // Set values
        thrustToWeightLabel.text = nil;
        
        motorClassLabel.text = nil;
        
        burnoutVelocityValueLabel.text = nil;
        
        burnoutAltitudeValueLabel.text = nil;
        
        peakAltitudeValueLabel.text = nil;
        
        coastTimeValueLabel.text = nil;

        
    }
    
    [thrustToWeightLabel setAlpha:1];
    
    [motorClassLabel setAlpha:1];
    
    [burnoutVelocityValueLabel setAlpha:1];
    
    [burnoutAltitudeValueLabel setAlpha:1];
    
    [peakAltitudeValueLabel setAlpha:1];
    
    [coastTimeValueLabel setAlpha:1];
    
    [self resetTextfieldDisplay];
}

-(IBAction) showEmailModalView:(id)sender 
{
    if([MFMailComposeViewController canSendMail]){

        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        
        if (picker) {
            
            picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
            NSString *mailSubject = [NSString stringWithFormat:@"%@ Calculation", self.title];
            NSString *mailMsg = [self exportMsgHtml];
    
            [picker setSubject:mailSubject];
    
            [picker setMessageBody:mailMsg isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
    
            picker.navigationBar.barStyle = UIBarStyleDefault; // choose your style, unfortunately, Translucent colors behave quirky.
    
           // [self presentModalViewController:picker animated:YES];
            [self presentViewController:picker animated:YES completion:nil];
            [picker release];
            
        }
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Mail Accounts" message:@"Please set up a Mail account in order to send email." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];

        
    }
    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{ 
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-("
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
            
            break;
    }
   // [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)exportButtonAction:(id)sender
{
    if (![UIPrintInteractionController isPrintingAvailable]) {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Email Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", nil];
        
    } else {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Export Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", @"Print", nil];
    }
    // Show the sheet
    [sheet showFromTabBar:self.tabBarController.tabBar];
    [sheet release];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self showEmailModalView:nil];
            break;
            
        case 1:
            [self printCalculation:nil];
            break;
            
        default:
            break;
            
    }
    
}

#pragma mark -

#pragma mark Printing


// Print button handler that sends our print instructions to the OS

- (IBAction)printCalculation:(id)sender {
    
    // Get a reference to the singleton iOS printing concierge
    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    
    if(pic && [UIPrintInteractionController isPrintingAvailable]) {

        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = self.title;
        pic.printInfo = printInfo;
        
        NSString *msg = [self exportMsgText];
        
        UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc]
                                                     initWithText:msg];
        textFormatter.startPage = 0;
        textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
        textFormatter.maximumContentWidth = 6 * 72.0;
        pic.printFormatter = textFormatter;
        [textFormatter release];
        pic.showsPageRange = YES;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSLog(@"Printing could not complete because of error: %@", error);
            }
        };
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [pic presentFromRect:self.tabBarController.tabBar.frame inView:self.view animated:YES completionHandler:completionHandler];
        } else {
            [pic presentAnimated:YES completionHandler:completionHandler];
        }
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Printing Error" message:@"Your device doesn't support printing" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    
}

- (NSString *) exportMsgText
{
    NSString *motorModelNumberString = motorCode;
    
    NSString *totalMotorMassString = [NSString stringWithFormat:@"%.2f",totalMotorMass];
    NSString *totalMotorMassUnitString = motorMassUnit.name;

    NSString *propellantMassString = [NSString stringWithFormat:@"%.2f",propellentMass];
    NSString *propellantMassUnitString = propellantMassUnit.name;
    
    NSString *rocketMassString = [NSString stringWithFormat:@"%.2f",rocketMass];
    NSString *rocketMassUnitString = rocketMassUnit.name;
    
    NSString *averageThrustString = [NSString stringWithFormat:@"%.2f",avgThrust];
    NSString *averageThrustUnitString = avgThrustUnit.name;
    
    NSString *burnTimeString = [NSString stringWithFormat:@"%.2f",burnTime];
    NSString *burnTimeUnitString = burnTimeUnit.name;

    NSString *cdString = [NSString stringWithFormat:@"%.2f",cd];
 
    NSString *areaString = [NSString stringWithFormat:@"%.2f",frontalArea];
    NSString *areaUnitString = areaUnit.name;
   
    NSString *numMotorsString = [NSString stringWithFormat:@"%d",numberMotors];

    NSString *motorClassString = motorClassLabel.text;
    NSString *burnoutVelocityString = [NSString stringWithFormat:@"%.2f",[burnoutVelocityValueLabel.text doubleValue]];
    NSString *burnoutVelocityUnitString = velocityUnit.name;
    
    NSString *burnoutAltitudeString = [NSString stringWithFormat:@"%.2f",[burnoutAltitudeValueLabel.text doubleValue]];
    NSString *burnoutAltitudeUnitString = altitudeUnit.name;
    
    NSString *peakAltitudeString = [NSString stringWithFormat:@"%.2f",[peakAltitudeValueLabel.text doubleValue]];
    NSString *peakAltitudeUnitString = altitudeUnit.name;

    NSString *coastTimeString = [NSString stringWithFormat:@"%.2f",[coastTimeValueLabel.text doubleValue]];
    NSString *coastTimeUnitString = coastTimeUnit.name;
    
    NSString *title = self.title;
    
    NSString *msg = [NSString stringWithFormat:@"%@\n\nMotor: %@\nTotal Motor Mass: %@ (%@)\nPropellant Mass: %@ (%@)\nRocket Mass Without Motor: %@ (%@)\nAverage Thrust: %@ (%@)\nBurn Time: %@ (%@)\nDrag Coefficient: %@\nFrontal Area: %@ (%@)\nNumber of Motors: %@\n--------------------\nMotor Class: %@\nBurnout Velocity: %@ (%@)\nBurnout Altitude: %@ (%@)\nPeak Altitude: %@ (%@)\nCoast Time: %@ (%@)\n\n\n\nGenerated by Rocket Calculator\n\nAvailable on the Apple App Store",title,motorModelNumberString, totalMotorMassString, totalMotorMassUnitString, propellantMassString, propellantMassUnitString, rocketMassString, rocketMassUnitString, averageThrustString, averageThrustUnitString, burnTimeString, burnTimeUnitString, cdString, areaString, areaUnitString, numMotorsString, motorClassString, burnoutVelocityString, burnoutVelocityUnitString, burnoutAltitudeString, burnoutAltitudeUnitString, peakAltitudeString, peakAltitudeUnitString, coastTimeString, coastTimeUnitString];
    
    
    return msg;
}

- (NSString *) exportMsgHtml
{    
    //return msg;
    NSString *motorModelNumberString = motorCode;
    
    NSString *totalMotorMassString = [NSString stringWithFormat:@"%.2f",totalMotorMass];
    NSString *totalMotorMassUnitString = motorMassUnit.name;
    
    NSString *propellantMassString = [NSString stringWithFormat:@"%.2f",propellentMass];
    NSString *propellantMassUnitString = propellantMassUnit.name;
    
    NSString *rocketMassString = [NSString stringWithFormat:@"%.2f",rocketMass];
    NSString *rocketMassUnitString = rocketMassUnit.name;
    
    NSString *averageThrustString = [NSString stringWithFormat:@"%.2f",avgThrust];
    NSString *averageThrustUnitString = avgThrustUnit.name;
    
    NSString *burnTimeString = [NSString stringWithFormat:@"%.2f",burnTime];
    NSString *burnTimeUnitString = burnTimeUnit.name;
    
    NSString *cdString = [NSString stringWithFormat:@"%.2f",cd];
    
    NSString *areaString = [NSString stringWithFormat:@"%.2f",frontalArea];
    NSString *areaUnitString = areaUnit.name;
    
    NSString *numMotorsString = [NSString stringWithFormat:@"%d",numberMotors];
    
    NSString *motorClassString = motorClassLabel.text;
    NSString *burnoutVelocityString = [NSString stringWithFormat:@"%.2f",[burnoutVelocityValueLabel.text doubleValue]];
    NSString *burnoutVelocityUnitString = velocityUnit.name;
    
    NSString *burnoutAltitudeString = [NSString stringWithFormat:@"%.2f",[burnoutAltitudeValueLabel.text doubleValue]];
    NSString *burnoutAltitudeUnitString = altitudeUnit.name;
    
    NSString *peakAltitudeString = [NSString stringWithFormat:@"%.2f",[peakAltitudeValueLabel.text doubleValue]];
    NSString *peakAltitudeUnitString = altitudeUnit.name;
    
    NSString *coastTimeString = [NSString stringWithFormat:@"%.2f",[coastTimeValueLabel.text doubleValue]];
    NSString *coastTimeUnitString = coastTimeUnit.name;
    
    NSString *title = self.title;
    
    NSString *msg = [NSString stringWithFormat:@"%@<br /><br />Motor: %@<br />Total Motor Mass: %@ (%@)<br />Propellant Mass: %@ (%@)<br />Rocket Mass Without Motor: %@ (%@)<br />Average Thrust: %@ (%@)<br />Burn Time: %@ (%@)<br />Drag Coefficient: %@<br />Frontal Area: %@ (%@)<br />Number of Motors: %@<br />--------------------<br />Motor Class: %@<br />Burnout Velocity: %@ (%@)<br />Burnout Altitude: %@ (%@)<br />Peak Altitude: %@ (%@)<br />Coast Time: %@ (%@)<br /><br /><br /><br />Generated by Rocket Calculator<br />Available on the Apple <a href=\"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=484673201&mt=8\">App Store</a>",title, motorModelNumberString, totalMotorMassString, totalMotorMassUnitString, propellantMassString, propellantMassUnitString, rocketMassString, rocketMassUnitString, averageThrustString, averageThrustUnitString, burnTimeString, burnTimeUnitString, cdString, areaString, areaUnitString, numMotorsString, motorClassString, burnoutVelocityString, burnoutVelocityUnitString, burnoutAltitudeString, burnoutAltitudeUnitString, peakAltitudeString, peakAltitudeUnitString, coastTimeString, coastTimeUnitString];
    
    return msg;

}

- (IBAction)openDataAssistant:(id)sender
{
    AltitudeDataAssistViewController *dac = [[AltitudeDataAssistViewController alloc] init];
    
    dac.totalMotorMass = totalMotorMass;
    dac.motorCaseMass = motorCaseMass;
    dac.propellentMass = propellentMass;
    dac.avgThrust = avgThrust;
    dac.burnTime = burnTime;
    dac.frontalArea = frontalArea;
    [dac setDelegate:self];
    
   // [self presentModalViewController:dac animated:YES];
    [self presentViewController:dac animated:YES completion:nil];
    
    [dac release];
        
}

- (void)enableTextfields
{
    
    [totalMotorMassTextfield setUserInteractionEnabled:YES];
    [avgThrustTextfield setUserInteractionEnabled:YES];
    [burnTimeTextfield setUserInteractionEnabled:YES];
    [propellantMassTextfield setUserInteractionEnabled:YES];
    [rocketMassTextfield setUserInteractionEnabled:YES];
    [numberMotorsTextfield setUserInteractionEnabled:YES];
    [dragCoefficientTextfield setUserInteractionEnabled:YES];
    [frontalAreaTextfield setUserInteractionEnabled:YES];
    
}

- (void) resignAllResponders
{
    
    [totalMotorMassTextfield resignFirstResponder];
    [avgThrustTextfield resignFirstResponder];
    [burnTimeTextfield resignFirstResponder];
    [propellantMassTextfield resignFirstResponder];
    [rocketMassTextfield resignFirstResponder];
    [numberMotorsTextfield resignFirstResponder];
    [dragCoefficientTextfield resignFirstResponder];
    [frontalAreaTextfield resignFirstResponder];
    
}

- (void) resetTextfieldDisplay
{
    
    // Tint required fields
    if (totalMotorMassTextfield.text == nil || [totalMotorMassTextfield.text isEqualToString:@""] || [totalMotorMassTextfield.text doubleValue] == 0) {

        [self.totalMotorMassTextfield setRequired:YES];
        self.totalMotorMassTextfield.text = @"";
        
    } else {

        [self.totalMotorMassTextfield setRequired:NO];
    }
    
    if (avgThrustTextfield.text == nil || [avgThrustTextfield.text isEqualToString:@""] || [avgThrustTextfield.text doubleValue] == 0) {
        
        [self.avgThrustTextfield setRequired:YES];
        self.avgThrustTextfield.text = @"";
        
    } else {
        
        [self.avgThrustTextfield setRequired:NO];
    }

    if (burnTimeTextfield.text == nil || [burnTimeTextfield.text isEqualToString:@""] || [burnTimeTextfield.text doubleValue] == 0) {
        
        [self.burnTimeTextfield setRequired:YES];
        self.burnTimeTextfield.text = @"";
        
    } else {
        
        [self.burnTimeTextfield setRequired:NO];
    }

    if (propellantMassTextfield.text == nil || [propellantMassTextfield.text isEqualToString:@""] || [propellantMassTextfield.text doubleValue] == 0) {
        
        [self.propellantMassTextfield setRequired:YES];
        self.propellantMassTextfield.text = @"";
        
    } else {
        
        [self.propellantMassTextfield setRequired:NO];
    }

    if (rocketMassTextfield.text == nil || [rocketMassTextfield.text isEqualToString:@""] || [rocketMassTextfield.text doubleValue] == 0) {
        
        [self.rocketMassTextfield setRequired:YES];
        self.rocketMassTextfield.text = @"";
        
    } else {
        
        [self.rocketMassTextfield setRequired:NO];
    }

    if (numberMotorsTextfield.text == nil || [numberMotorsTextfield.text isEqualToString:@""] || [numberMotorsTextfield.text doubleValue] == 0) {
        
        [self.numberMotorsTextfield setRequired:YES];
        self.numberMotorsTextfield.text = @"";
        
    } else {
        
        [self.numberMotorsTextfield setRequired:NO];
    }

    if (dragCoefficientTextfield.text == nil || [dragCoefficientTextfield.text isEqualToString:@""]) {
        
       // [self.dragCoefficientTextfield setRequired:YES];
        [self.dragCoefficientTextfield setRequired:NO];
    } else {
        
        [self.dragCoefficientTextfield setRequired:NO];
    }

    if (frontalAreaTextfield.text == nil || [frontalAreaTextfield.text isEqualToString:@""] || [frontalAreaTextfield.text doubleValue] == 0) {
        
        [self.frontalAreaTextfield setRequired:YES];
        self.frontalAreaTextfield.text = @"";
        
    } else {
        
        [self.frontalAreaTextfield setRequired:NO];
    }

}

- (void)showAlert:(NSString *)m fromSendingAlert:(NSString *)a
{
    if (!a) {
        
        currentAlert = nil;
        
    } else {
        
        currentAlert = a;
        
    }
    
    if (currentAlert) {
        
        UIAlertView* alertView = [[[UIAlertView alloc] initWithTitle:@"Warning" message:m delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Don't Show Again", nil] autorelease];
        
        [alertView show];
    }
    
    
}

- (void)showAlert:(NSString *)m
{
    
        UIAlertView* alertView = [[[UIAlertView alloc] initWithTitle:@"Alert" message:m delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        
        [alertView show];
    
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {

    if (buttonIndex == 0) {
        
        // Do nothing

    }
    
    else if (buttonIndex == 1) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:currentAlert];
        
        [self showAlert:@"You can re-enable this alert from the app settings"];
        
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

#pragma -
#pragma mark SingleSelectListViewController Delegate methods

- (void)altitudeDataAssistViewControllerWasDismissed:(AltitudeDataAssistViewController *)altitudeDataAssistViewController
{

    self.totalMotorMass = altitudeDataAssistViewController.totalMotorMass;
    self.avgThrust = altitudeDataAssistViewController.avgThrust;
    self.burnTime = altitudeDataAssistViewController.burnTime;
    self.propellentMass = altitudeDataAssistViewController.propellentMass;
    self.frontalArea = altitudeDataAssistViewController.frontalArea;
    [self resetTextfieldDisplay];

}

- (NSArray *)loadBodyTubesArray
{
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *pListPath = [path stringByAppendingPathComponent:@"/bodyTube.plist"];
    
    NSDictionary *bodyTubes = [NSDictionary dictionaryWithContentsOfFile:pListPath];
    
    NSMutableArray *btArray = [bodyTubes objectForKey:@"bodyTubes"];    
    
    NSSortDescriptor *manufacturerDescriptor =
    [[[NSSortDescriptor alloc]
      initWithKey:@"btManufacturer"
      ascending:YES
      selector:@selector(localizedCaseInsensitiveCompare:)] autorelease];    
    
    // Sort tubes by manufacturer
    NSArray * descriptors =
    [NSArray arrayWithObjects:manufacturerDescriptor, nil];
    NSArray * sortedArray =
    [btArray sortedArrayUsingDescriptors:descriptors];    
    
    // build array to pass to bodyTube VC
    NSMutableArray *tmpList = [[NSMutableArray alloc] init];
    
    for (NSDictionary *btDict in sortedArray) {
        
        NSString *labelHeader = [NSString stringWithFormat:@"%@ - %@", [btDict valueForKey:@"btManufacturer"], [btDict valueForKey:@"btDescription"]];
        NSString *labelDetail = [NSString stringWithFormat:@"Outer Diameter: %@", [btDict valueForKey:@"btOuterDiameter"]];
        NSNumber *outerDiameter = [btDict valueForKey:@"btOuterDiameterInches"];
        NSMutableDictionary *tmpDict = [[NSMutableDictionary alloc] init];
        [tmpDict setValue:labelHeader forKey:@"labelHeader"];
        [tmpDict setValue:labelDetail forKey:@"labelDetail"];
        [tmpDict setValue:outerDiameter forKey:@"outerDiameter"];
        [tmpList addObject:tmpDict];
        [tmpDict release];
        
    }
    
    return [tmpList autorelease];
    
}

- (void)openBodyTubeViewController:(id)sender
{
    
    SingleSelectListViewController *selectListVC = [[SingleSelectListViewController alloc] init];
    
    [selectListVC setDelegate:self];
    
    
    selectListVC.itemList = bodyTubesArray;
    selectListVC.listName = @"Body Tube Selection";
    
    //[self presentModalViewController:selectListVC animated:YES];
    [self presentViewController:selectListVC animated:YES completion:nil];
    
    [selectListVC release];
    
    
}

- (void)openMotorViewController:(id)sender
{
    
    MotorSelectListViewController *motorListVC = [[MotorSelectListViewController alloc] init];
    
    [motorListVC setDelegate:self];
    motorListVC.currentMotorType = self.currentMotorType;
    
   // [self presentModalViewController:motorListVC animated:YES];
    [self presentViewController:motorListVC animated:YES completion:nil];
    
    [motorListVC release];
    
    
}

- (IBAction)openDensityCalculatorViewController:(id)sender
{

    DensityCalculatorViewController *densityVC = [[DensityCalculatorViewController alloc]init];
    
    [densityVC setDelegate:self];
    
   // [self presentModalViewController:densityVC animated:YES];
    [self presentViewController:densityVC animated:YES completion:nil];
    
    [densityVC release];
    
}

#pragma -
#pragma mark SingleSelectListViewController Delegate methods

- (void)singleSelectListViewControllerWasDismissed:(SingleSelectListViewController*)singleSelectListViewController
{
    
    int selected = (int)singleSelectListViewController.selectedRow;
    
    if (selected < [bodyTubesArray count]) {
        
        double baseDiameter = ([[[bodyTubesArray objectAtIndex:selected] valueForKey:@"outerDiameter"] doubleValue]) * 0.0254;
    
        Calculator *calculator = [[Calculator alloc] init];
        
        double calcArea = [calculator calcAreaOfCircle:baseDiameter];
        
        // convert calcArea to cm2 to match baseunit.
        calcArea = calcArea * 10000;
        
        frontalArea = [[areaUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcArea] toUnit:areaUnit]doubleValue];
        
        [calculator release];
        
    }
    
}

#pragma -
#pragma mark MotorSelectListViewController Delegate methods

- (void)motorSelectListViewControllerWasDismissed:(MotorSelectListViewController*)motorSelectListViewController
{
    
    // convert grams to kilograms
    double baseTotalMotorMass = motorSelectListViewController.selectedMotor.initWt * 0.001;
    double basePropellentMass = motorSelectListViewController.selectedMotor.propWt * 0.001;
    double baseMotorCaseMass = baseTotalMotorMass - basePropellentMass;
    
    double baseAvgThrust = motorSelectListViewController.selectedMotor.avgThrust;
    double baseBurnTime = motorSelectListViewController.selectedMotor.burn_time;
    currentMotorType = motorSelectListViewController.currentMotorType;
    totalMotorMass = [[motorMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseTotalMotorMass] toUnit:motorMassUnit]doubleValue];
    propellentMass = [[propellantMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:basePropellentMass] toUnit:propellantMassUnit]doubleValue];
    motorCaseMass = [[caseMassUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseMotorCaseMass] toUnit:caseMassUnit]doubleValue];
    avgThrust = [[avgThrustUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseAvgThrust] toUnit:avgThrustUnit]doubleValue];
    burnTime = [[burnTimeUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:baseBurnTime] toUnit:burnTimeUnit]doubleValue];
    
    if (motorSelectListViewController.currentMotor) {
        
        motorCode = motorSelectListViewController.currentMotor.modelNumber;
        
    } else {
        
        motorCode = @"";
        
    }
  
}

#pragma -
#pragma mark DensityCalculatorViewController Delegate methods

- (void)densityCalculatorViewControllerWasDismissed:(DensityCalculatorViewController*)densityCalculatorViewController
{

    
}

@end