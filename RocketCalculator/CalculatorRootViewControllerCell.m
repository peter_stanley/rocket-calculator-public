//
//  CalculatorRootViewControllerCell.m
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

#import "CalculatorRootViewControllerCell.h"

@implementation CalculatorRootViewControllerCell

@synthesize label;

- (void)dealloc
{
    [label release];
    
    [super dealloc];
}

@end
