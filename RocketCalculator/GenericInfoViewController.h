//
//  GenericInfoViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 11/17/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GenericInfoViewController : UIViewController <UIWebViewDelegate> {

    NSString *htmlFileName;
    UIWebView *webView;
}

@property (nonatomic, retain) NSString *htmlFileName;
@property (nonatomic, retain) NSString *pdfFileName;
@property (nonatomic, retain) IBOutlet UIWebView *webView;

@end
