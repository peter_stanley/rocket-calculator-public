//
//  RAUnit.h
//  RocketCalculator
//
//  Created by Peter Stanley on 5/1/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RAUnit : NSObject {
	NSString *dimension;
	NSString *name;
	NSString *label;								// used in input fields. Keep short!
	RAUnit *baseUnit;
	NSDecimalNumber *toBaseFactor;					// multiply a value from this unit by this value to get the value in the base unit
	
	NSInteger nonBase10Multiplier;
	NSString *leftLabel;
	NSString *rightLabel;
	
	NSDecimalNumberHandler *roundingBehavior;
	NSDecimalNumber *minPlausibleRange;
	NSDecimalNumber *maxPlausibleRange;
	
	NSMutableDictionary *lastValues;				// saves the last entered values as NSDecimalNumber, converted to baseUnit (!)
}

@property (nonatomic, retain) NSString *dimension;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *label;
@property (nonatomic, retain) RAUnit *baseUnit;
@property (nonatomic, retain) NSDecimalNumber *toBaseFactor;

@property (nonatomic, assign) NSInteger nonBase10Multiplier;
@property (nonatomic, retain) NSString *leftLabel;
@property (nonatomic, retain) NSString *rightLabel;

@property (nonatomic, retain) NSDecimalNumberHandler *roundingBehavior;
@property (nonatomic, retain) NSDecimalNumber *minPlausibleRange;
@property (nonatomic, retain) NSDecimalNumber *maxPlausibleRange;

@property (nonatomic, retain) NSMutableDictionary *lastValues;

- (id) initWithDict:(NSDictionary *)dict ofDimension:(NSString *)myDimension;

- (NSDecimalNumber *) convertToBaseUnit:(NSDecimalNumber *)value;
- (NSDecimalNumber *) convertValue:(NSDecimalNumber *)value toUnit:(RAUnit *)unit;

- (NSDecimalNumber *) roundValue:(NSDecimalNumber *)value toUnit:(RAUnit *)unit;
- (BOOL) isPlausibleValue:(NSDecimalNumber *)value;

- (BOOL) setLastValue:(NSDecimalNumber *)value forSpecialKey:(NSString *)specialKey;
- (NSDecimalNumber *) lastValueForSpecialKey:(NSString *)specialKey;
- (void) saveDefaultUnitForDimension;
- (NSString *) defaultUnitNameForDimension;
- (void) saveDefaultUnitForKey:(NSString *)defaultKey;
- (NSString *) defaultUnitNameForKey:(NSString *)defaultKey;

@end