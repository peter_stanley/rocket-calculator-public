//
//  CalcTextfield.h
//  RocketCalculator
//
//  Created by Peter Stanley on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CalcTextfield : UITextField
{
    BOOL isRequired;
}
- (void)setRequired:(BOOL)r;

@end
