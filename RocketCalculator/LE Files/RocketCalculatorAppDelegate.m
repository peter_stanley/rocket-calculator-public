//
//  RocketCalculatorAppDelegate.m
//  RocketCalculator
//
//  Created by Peter Stanley on 11/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "RocketCalculatorAppDelegate.h"
#import "CalculatorRootViewController.h"
#import "RAUnit.h"

@interface RocketCalculatorAppDelegate (Private)

- (NSString *) documentPathOfFile:(NSString *)filename;
- (NSString *) resourcePathOfPlist:(NSString *)filename;
- (NSArray *) loadUnitsOfDimension:(NSString *)dimension;

@end

@implementation RocketCalculatorAppDelegate

@synthesize window = _window, tabBarController;
@synthesize calculatorRootViewController, infoListRootViewController;
@synthesize unitDict, unitLabels;

+ (void)initialize
{
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *pListPath = [path stringByAppendingPathComponent:@"Settings.bundle/Root.plist"];
    NSDictionary *pList = [NSDictionary dictionaryWithContentsOfFile:pListPath];
    
    NSMutableArray *prefsArray = [pList objectForKey:@"PreferenceSpecifiers"];
    NSMutableDictionary *regDictionary = [NSMutableDictionary dictionary];
    for (NSDictionary *dict in prefsArray) {
        NSString *key = [dict objectForKey:@"Key"];
        if (key) {
            id value = [dict objectForKey:@"DefaultValue"];
            [regDictionary setObject:value forKey:key];
        } 
        
    }
     
    [[NSUserDefaults standardUserDefaults] registerDefaults:regDictionary];
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],@"enableTWTAlert",[NSNumber numberWithBool:YES],@"enableMachAlert",nil]];

}

#pragma mark -
#pragma mark Application's documents directory

/**
 Returns the path to the application's documents directory.
 */
- (NSString *)applicationDocumentsDirectory {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (void)dealloc
{
    [unitLabels release];
    [unitDict release];
    [infoListRootViewController release];
    [calculatorRootViewController release];
    [tabBarController release];
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if (![prefs stringForKey:@"temperature"]) {
        
        [prefs setValue:@"15" forKey:@"temperature"];
        
    }
    
    if (![prefs stringForKey:@"elevation"]) {
        
        [prefs setValue:@"0" forKey:@"elevation"];
        
    }

    /*
    NSError *error;
    
    NSManagedObjectContext *context = [self managedObjectContext];
    //NSManagedObjectContext *context = [self managedObjectContext];
    
    if (!context) {
        
        // Handle the error.
    }
    
    // Check to see if initial import was done
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Motor" inManagedObjectContext:context];
    
    [request setEntity:entity];
    
    [request setResultType:NSDictionaryResultType];
   
    int recordCount = 0;
    recordCount = [context countForFetchRequest:request error:&error];
    NSLog(@"recordCount motors = %d",recordCount);
    
    if (!recordCount) {
        [self loadInitialData];
        
        NSLog(@"initial data loaded 1st time");
    } else {
        NSLog(@"initial data already loaded");
        
        NSString *mfilePath = [self documentPathOfFile:@"mdata_ver.txt"];

       //  Read in the file content. 
        NSStringEncoding enc;
        NSString *content = [NSString
                             stringWithContentsOfFile:mfilePath
                             usedEncoding:&enc
                             error:&error];

        NSLog(@"motorData version is %@", content);
    }
    
    [request release];
    *//*
    NSError * error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *motorsPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"motor_files"];
    NSLog(@"motorsPath = %@",motorsPath);

    NSArray *directoryContents = [fileManager contentsOfDirectoryAtPath:motorsPath error:&error];

    NSLog(@"directoryContents = %@", directoryContents);
   */
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = self.tabBarController;
    
   [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

#pragma mark -
#pragma mark Utilities

// returns an array of all units of desired dimension
- (NSArray *) unitsOfDimension:(NSString *)dimension
{
	// if unit dimension is already loaded, we will find it here
	if(self.unitDict) {
		NSArray *unitsOfDimension = [unitDict objectForKey:dimension];
		if(unitsOfDimension) {
			return unitsOfDimension;
		}
	}
	
	// this will load the desired dimension and return it's units (if available)
	return [self loadUnitsOfDimension:dimension];
}


// returns the desired unit and loads units of the dimension if it cannot be found
- (RAUnit *) unitWithName:(NSString *)name ofDimension:(NSString *)dimension
{
	if(!name || !dimension) {
		return nil;
	}
	
	NSArray *unitsOfDimension = [self unitsOfDimension:dimension];
	
	// the dimension exists, the unit must be here if it is a valid unit
	if(unitsOfDimension) {
		for(RAUnit *unit in unitsOfDimension) {
			if([unit.name isEqualToString:name]) {
				return unit;
			}
		}
		
		NSLog(@"Error: Requested unit with name \"%@\" of dimension \"%@\" does not exist!", name, dimension);
	}
	
	return nil;
}


// returns YES when the units are there (no matter if they were there already or have just been loaded)
- (NSArray *) loadUnitsOfDimension:(NSString *)dimension
{
	if(dimension) {
		if(!unitDict) {
			self.unitDict = [NSMutableDictionary dictionary];
		}
		if(!unitLabels) {
			self.unitLabels = [NSDictionary dictionaryWithContentsOfFile:[self resourcePathOfPlist:@"Unit_labels"]];
		}
		
		NSString *unitResource = [self resourcePathOfPlist:[NSString stringWithFormat:@"Units_%@", dimension]];
		NSDictionary *unitInfoDict = [NSDictionary dictionaryWithContentsOfFile:unitResource];
		if(unitInfoDict) {
			NSString *baseUnitName = [unitInfoDict objectForKey:@"baseUnit"];
			NSArray *unitsOfDimension = [unitInfoDict objectForKey:@"units"];
			float minRange = 0;
			float maxRange = 99999999;
			if([unitInfoDict valueForKey:@"minPlausibleRange"]) {
				minRange = [[unitInfoDict valueForKey:@"minPlausibleRange"] floatValue];
			}
			if([unitInfoDict valueForKey:@"maxPlausibleRange"]) {
				maxRange = [[unitInfoDict valueForKey:@"maxPlausibleRange"] floatValue];
			}
			
			// We got units, prepare to instantiate them
			if(unitsOfDimension) {
				RAUnit *newBaseUnit = nil;
				NSMutableArray *newUnits = [NSMutableArray arrayWithCapacity:[unitsOfDimension count]];
				
				// create RAUnit objects for each unit we find
				for(NSDictionary *uDict in unitsOfDimension) {
					RAUnit *unit = [[RAUnit alloc] initWithDict:uDict ofDimension:dimension];
					
					NSString *locLabel = [unitLabels objectForKey:unit.name];
					if(nil != locLabel) {
						unit.label = locLabel;
					}
					
					if([unit.name isEqualToString:baseUnitName]) {
						newBaseUnit = unit;
					}
					
					[newUnits addObject:unit];
					[unit release];
				}
				
				// assign the base unit to all units and return
				if(newBaseUnit) {
					NSDecimalNumber *minPRange = (NSDecimalNumber *)[NSDecimalNumber numberWithFloat:minRange];
					NSDecimalNumber *maxPRange = (NSDecimalNumber *)[NSDecimalNumber numberWithFloat:maxRange];
					for(RAUnit *unit in newUnits) {
						unit.baseUnit = newBaseUnit;
						unit.minPlausibleRange = minPRange;
						unit.maxPlausibleRange = maxPRange;
					}
					
					// save unitDict
					NSArray *dimensionArray = [NSArray arrayWithArray:newUnits];
					[unitDict setObject:dimensionArray forKey:dimension];
					
					return dimensionArray;
				}
				else {
					NSLog(@"Error: Created units, but base unit named %@ was not amongst them! NOT KEEPING UNITS!", baseUnitName);
				}
			}
			else {
				NSLog(@"Error: There are no units for dimension \"%@\"", dimension);
			}
		}
		else {
			NSLog(@"Error: There is no dict for dimension \"%@\"", dimension);
		}
	}
	
	return nil;
}
/*
#pragma mark -
#pragma mark Core Data stack


// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 
- (NSManagedObjectContext *)managedObjectContext {
	
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
	
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [NSManagedObjectContext new];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return managedObjectContext;
}



// Returns the managed object model for the application.
// If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 
- (NSManagedObjectModel *)managedObjectModel {
	
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
	
	NSString *path = [[NSBundle mainBundle] pathForResource:@"RocketCalculator" ofType:@"momd"];
	NSLog(@"path: %@",path);
	NSURL *momURL = [NSURL fileURLWithPath:path];
	
	managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:momURL];
	
    // managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];    
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    
	if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
	
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"RocketCalculator.sqlite"]];
    
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
	NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
        
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
    }    
	
    return persistentStoreCoordinator;
	
}
*/


- (NSString *) documentPathOfFile:(NSString *)filename
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [[paths objectAtIndex:0] stringByAppendingPathComponent:filename];
}

- (NSString *) resourcePathOfPlist:(NSString *)filename
{
	NSBundle *thisBundle = [NSBundle bundleForClass:[self class]];
	return [thisBundle pathForResource:filename ofType:@"plist"];
}

- (BOOL) deviceIsIpad {
    BOOL isIpad = NO;
    if ([UIDevice instancesRespondToSelector:@selector(userInterfaceIdiom)]) {
        UIUserInterfaceIdiom idiom = [[UIDevice currentDevice] userInterfaceIdiom];
        
        if (idiom == UIUserInterfaceIdiomPad) {
            isIpad = YES;
        }
    }
    return isIpad;
}
/*


- (void)loadInitialData
{
    NSLog(@"getting ready to load initial data");
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    NSMutableArray *records = [[[NSMutableArray alloc] init] autorelease];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"motor" ofType:@"tsv"];
    
    char buffer[1000];
    FILE* file = fopen([path UTF8String], "r");
    
    //////////////////////////////////////////////////////////////////////////////////////
    // Get Motors
    ////////////////////////////////////////////////////////////////////

        
    while(fgets(buffer, 1000, file) != NULL)
    {   
        NSString *string = [[NSString alloc] initWithUTF8String:buffer];
        NSArray *parts = [string componentsSeparatedByString:@"\t"];
        NSMutableDictionary *newRecord = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                          [parts objectAtIndex:0], @"motor_id", 
                                          [parts objectAtIndex:1], @"manufacturer",
                                          [parts objectAtIndex:2], @"manufacturer_abbrev",
                                          [parts objectAtIndex:3], @"designation",
                                          [parts objectAtIndex:4], @"brand_name",
                                          [parts objectAtIndex:5], @"common_name",
                                          [parts objectAtIndex:6], @"impulse_class",
                                          [parts objectAtIndex:7], @"diameter",
                                          [parts objectAtIndex:8], @"length",
                                          [parts objectAtIndex:9], @"type",
                                          [parts objectAtIndex:10], @"cert_org",
                                          [parts objectAtIndex:11], @"avg_thrust_n",
                                          [parts objectAtIndex:12], @"max_thrust_n",
                                          [parts objectAtIndex:13], @"tot_impulse_ns",
                                          [parts objectAtIndex:14], @"burn_time_s",
                                          [parts objectAtIndex:15], @"data_files",
                                          [parts objectAtIndex:16], @"info_url",
                                          [parts objectAtIndex:17], @"prop_weight_g",
                                          [parts objectAtIndex:18], @"updated_on",
                                          [parts objectAtIndex:19], @"delays",
                                          [parts objectAtIndex:20], @"total_weight_g",
                                          [parts objectAtIndex:21], @"case_info",
                                          [parts objectAtIndex:22], @"prop_info",
                                          nil];

        [self addMotorRecord:newRecord];
        
        [records addObject:newRecord];
        [newRecord release];
        [string release];
    }
    fclose(file);
    
    NSLog(@"Done initial load");
    
    NSError *error; 
    if (![managedObjectContext save:&error])
        NSLog(@"Error saving: %@", error);
    
    NSString *mfilePath = [self documentPathOfFile:@"mdata_ver.txt"];

    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
   
    NSLog(@"version = %@", version);
        
    // String to write
    NSString *str = version;
        
    // Write the file
    [str writeToFile:mfilePath atomically:YES 
            encoding:NSUTF8StringEncoding error:&error];
        
    // Read in the file content. 
    NSStringEncoding enc;
    NSString *content = [NSString
                         stringWithContentsOfFile:mfilePath
                         usedEncoding:&enc
                         error:&error];
    if (!content) {
        NSLog(@"%s: Error reading content of \"%@\": %@", __func__, path, error);
    }    
    NSLog(@"mdata_ver.txt content is %@", content);
    
    [pool drain];
}    
    
- (void)addMotorRecord:(id)aRecord
{
	
	//NSManagedObjectContext *moc = [[AppController sharedAppController] managedObjectContext];
    NSManagedObjectContext *moc = [self managedObjectContext];
	// Create and configure a new instance of the Motor entity 
	Motor *motor = [NSEntityDescription insertNewObjectForEntityForName:@"Motor" 
                                                    inManagedObjectContext:moc];
	
	[motor setMotor_id:[aRecord objectForKey:@"motor_id"]];
	[motor setManufacturer:[aRecord objectForKey:@"manufacturer"]];
    [motor setManufacturer_abbrev:[aRecord objectForKey:@"manufacturer_abbrev"]];
	[motor setDesignation:[aRecord objectForKey:@"designation"]];
	[motor setBrand_name:[aRecord objectForKey:@"brand_name"]];
	[motor setCommon_name:[aRecord objectForKey:@"common_name"]];
	[motor setImpulse_class:[aRecord objectForKey:@"impulse_class"]];
	[motor setDiameter:[NSNumber numberWithDouble:[[aRecord valueForKey:@"diameter"] doubleValue]]];
    [motor setLength:[NSNumber numberWithDouble:[[aRecord valueForKey:@"length"] doubleValue]]];
    [motor setType:[aRecord objectForKey:@"type"]];
	[motor setCert_org:[aRecord objectForKey:@"cert_org"]];
	[motor setAvg_thrust_n:[NSNumber numberWithDouble:[[aRecord valueForKey:@"avg_thrust_n"] doubleValue]]];
	[motor setMax_thrust_n:[NSNumber numberWithDouble:[[aRecord valueForKey:@"max_thrust_n"] doubleValue]]];
	[motor setTot_impulse_ns:[NSNumber numberWithDouble:[[aRecord valueForKey:@"tot_impulse_ns"] doubleValue]]];
	[motor setBurn_time_s:[NSNumber numberWithDouble:[[aRecord valueForKey:@"burn_time_s"] doubleValue]]];
	[motor setData_files:[NSNumber numberWithInt:[[aRecord valueForKey:@"data_files"] intValue]]];
	[motor setInfo_url:[aRecord objectForKey:@"info_url"]];
    [motor setProp_weight_g:[NSNumber numberWithDouble:[[aRecord valueForKey:@"prop_weight_g"] doubleValue]]];
    [motor setUpdated_on:[aRecord objectForKey:@"updated_on"]];
    [motor setDelays:[aRecord objectForKey:@"delays"]];
    [motor setTotal_weight_g:[NSNumber numberWithDouble:[[aRecord valueForKey:@"total_weight_g"] doubleValue]]];
    [motor setCase_info:[aRecord objectForKey:@"case_info"]];
    [motor setProp_info:[aRecord objectForKey:@"prop_info"]];

	
}

#pragma mark -

#pragma mark Saving

//

// Performs the save action for the application, which is to send the save:

// message to the application's managed object context.

//
- (void)save{
    
    NSError *error;
    
    if (![[self managedObjectContext] save:&error]) {
        
        // Update to handle the error appropriately.
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        
        exit(-1);  // Fail
        
    }
    
}*/

@end