//
//  RocketCalculatorAppDelegate.h
//  RocketCalculator
//
//  Created by Peter Stanley on 11/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
//#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

@class CalculatorRootViewController;
@class InformationListViewController;

@class RAUnit;

@interface RocketCalculatorAppDelegate : UIResponder <UIApplicationDelegate>
{/*
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;	    
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
*/
    UITabBarController *tabBarController;

    CalculatorRootViewController *calculatorRootViewController;
    InformationListViewController *infoListRootViewController;
    
    NSMutableDictionary *unitDict;
	NSDictionary *unitLabels;

}

// Class method for convenience
//+ (RocketCalculatorAppDelegate *)sharedAppDelegate;
/*
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
*/
@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;

@property (nonatomic, retain) IBOutlet CalculatorRootViewController *calculatorRootViewController;
@property (nonatomic, retain) IBOutlet InformationListViewController *infoListRootViewController;

@property (nonatomic, retain) NSMutableDictionary *unitDict;
@property (nonatomic, retain) NSDictionary *unitLabels;

- (NSString *)applicationDocumentsDirectory;
- (NSArray *) unitsOfDimension:(NSString *)dimension;
- (RAUnit *) unitWithName:(NSString *)name ofDimension:(NSString *)dimension;
- (BOOL) deviceIsIpad;
/*
// Used to load initial data
- (void)save;
- (void)loadInitialData;
- (void)addMotorRecord:(id)aRecord;
*/
@end
