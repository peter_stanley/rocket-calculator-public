//
//  CalculatorRootViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 4/3/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import "AltitudePredictionViewController.h"
#import "CalculatorRootViewController.h"
#import "CalculatorRootViewControllerCell.h"
//#import "BPCalculatorViewController.h"
#import "ParachuteSizeCalculatorViewController.h"
//#import "StreamerSizeCalculatorViewController.h"
//#import "StreamerSizeCalculator2ViewController.h"
#import "WeightToThrustCalculatorViewController.h"
//#import "SingleStationTrackingViewController.h"
//#import "TwoStationTrackingViewController.h"
//#import "StaticPortSizeCalculatorViewController.h"
#import "GenericFormulaViewController.h"
#import "UnitPreferenceViewController.h"
#import "DriftDistanceCalculatorViewController.h"

// Rocket Section
#define ALTITUDE_PREDICTION_CALC 0
#define WEIGHT_TO_THRUST_CALC 1
#define PARACHUTE_SIZE_CALC 2
#define DRIFT_DISTANCE_CALC 3
//#define STREAMER_SIZE_CALC 4
//#define STREAMER_SIZE_CALC2 5
//#define BP_CALC 6
//#define STATIC_PORT_CALC 7
//#define SINGLE_STATION_TRACKING_CALC 8
//#define TWO_STATION_TRACKING_CALC 9

@implementation CalculatorRootViewController

@synthesize calculatorTableView, calculatorRootViewControllerCell;

- (id)init {
	
	if (self = [super initWithNibName:@"CalculatorRootViewController" bundle:nil]) {
		
		self.title = @"Calculators";
                
	}
    
    self.calculatorTableView.allowsSelection = YES;
    
	return self;
	
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"initWithNibName called");
	return [self init];
}

- (void)dealloc
{

    [calculatorRootViewControllerCell release];
    [calculatorTableView release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    //  [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{

    introLoaded = NO;
    
    [super viewDidLoad];
    self.title = @"Calculators";
    UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    settingsButton.frame = CGRectMake(0, 0, 25, 25); //set frame for button
    
    UIImage *buttonImage = [UIImage imageNamed:@"gear-2.png"];
    
    [settingsButton setBackgroundImage:buttonImage forState:UIControlStateNormal];

    [settingsButton addTarget:self action:@selector(settingsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:settingsButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    self.navigationController.navigationBar.translucent = NO;
    [modalButton release];

    [self.calculatorTableView reloadData];
    
    
}
    
- (IBAction)settingsButtonAction:(id)sender {
    UnitPreferenceViewController *settingsView = [[[UnitPreferenceViewController alloc] init] autorelease];
    [self.navigationController pushViewController:settingsView animated:YES];

}

- (void)viewDidUnload
{
    self.calculatorRootViewControllerCell = nil;
    self.calculatorTableView = nil;
    
    [super viewDidUnload];

}

-(void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:YES];
    [self.calculatorTableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    if (!introLoaded) {
        [self loadAboutScreen];
        introLoaded = YES;
    }
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
    // Return YES for supported orientations
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}

#pragma mark - TableView methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
	return 1;
}
/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 
{
    
    return @"Calculators";
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CalculatorCellIdentifier = @"CalculatorCell";

    CalculatorRootViewControllerCell  *cell = (CalculatorRootViewControllerCell *)[tableView dequeueReusableCellWithIdentifier:CalculatorCellIdentifier];
    if (cell == nil) {
		[[NSBundle mainBundle] loadNibNamed:@"CalculatorRootViewControllerCell" owner:self options:nil];
        cell = calculatorRootViewControllerCell;
		self.calculatorRootViewControllerCell = nil;
    }
    
    switch (indexPath.row) 
    {
        case ALTITUDE_PREDICTION_CALC:
        {
            cell.label.text = @"Altitude Prediction";
        }
            
            break;
            
        case WEIGHT_TO_THRUST_CALC:
        {
            cell.label.text = @"Thrust to Weight Ratio";
            
        }
        
            break;
            
        case PARACHUTE_SIZE_CALC:
        {
            cell.label.text = @"Parachute Size & Descent Rate";
            
        }
                    
            break;
            
        case DRIFT_DISTANCE_CALC:
        {
            cell.label.text = @"Drift Distance";
            
        }
            
            break;
                   
        
//        case STREAMER_SIZE_CALC:
//        {
//            cell.label.text = @"Streamer Size Method #1";
//            
//        }
            
//            break;
                    
     //   case STREAMER_SIZE_CALC2:
     //   {
     //       cell.label.text = @"Streamer Size Method #2";
            
     //   }
            
      //      break;

      ////  case BP_CALC:
      //  {
      //      cell.label.text = @"Ejection Charge";
            
      //  }
                    
     //       break;
                    
      //  case STATIC_PORT_CALC:
      //  {
      //      cell.label.text = @"Static Pressure Port Size";
            
      //  }
       //
       //     break;
            
     //   case SINGLE_STATION_TRACKING_CALC:
     //   {
      //      cell.label.text = @"Single-Station Altitude Tracking";
            
     //   }
     //       break;
            
        //case TWO_STATION_TRACKING_CALC:
       // {
       //     cell.label.text = @"Two-Station Altitude Tracking";
            
       // }
       //     break;
                 
        default:
                    
            break;
            
    }
            
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static UIViewController *nextController = nil;
    
    switch (indexPath.row) 
    {
        case ALTITUDE_PREDICTION_CALC:
        {
            nextController = [[AltitudePredictionViewController alloc] init];
            
        }
            
            break;

        case WEIGHT_TO_THRUST_CALC:
        {
            nextController = [[WeightToThrustCalculatorViewController alloc] init];
            
        }
            
            break;
            
        case PARACHUTE_SIZE_CALC:
        {
            nextController = [[ParachuteSizeCalculatorViewController alloc] init];
            
        }
            
            break;
            
        case DRIFT_DISTANCE_CALC:
        {
            nextController = [[DriftDistanceCalculatorViewController alloc] init];
            
        }
            
            break;

            
//        case STREAMER_SIZE_CALC:
//        {
//            nextController = [[StreamerSizeCalculatorViewController alloc] init];
//            
//        }
//            
//            break;
            
//        case STREAMER_SIZE_CALC2:
//        {
//            nextController = [[StreamerSizeCalculator2ViewController alloc] init];
//            
//        }
//            
//            break;

            
//        case BP_CALC:
//        {
//            nextController = [[BPCalculatorViewController alloc] init];
//            
//        }
//            
//            break;
//            
//        case STATIC_PORT_CALC:
//        {
//            nextController = [[StaticPortSizeCalculatorViewController alloc] init];
//            
//        }
//            
//            break;
//            
//        case SINGLE_STATION_TRACKING_CALC:
//        {
//            nextController = [[SingleStationTrackingViewController alloc] init];
//            
//        }
//            break;
//            
//        case TWO_STATION_TRACKING_CALC:
//        {
//            nextController = [[TwoStationTrackingViewController alloc] init];
//            
//        }
//            break;
            
        default:
            
            break;
            
    }

    
    [self.navigationController pushViewController:nextController animated:YES];
	[nextController release];
    
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

- (void)loadAboutScreen {
    GenericFormulaViewController *infoView = [[[GenericFormulaViewController alloc] init] autorelease];
    infoView.htmlFileName = @"about";
    [infoView setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    //[self presentModalViewController:infoView animated:YES];
    [self presentViewController:infoView animated:YES completion:nil];
}

@end
