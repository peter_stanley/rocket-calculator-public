//
//  AltitudePredictionViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

@class RAUnit;
@class CalcTextfield;
@class SingleSelectListViewController;

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AltitudeDataAssistViewController.h"
#import "DensityCalculatorViewController.h"

@interface AltitudePredictionViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIPrintInteractionControllerDelegate, AltitudeDataAssistViewControllerDelegate, SingleSelectListViewControllerDelegate, MotorSelectListViewControllerDelegate, DensityCalculatorViewControllerDelegate>
{

    bool isIpad;
    bool requiredCheck;
    bool showLowThrustAlert;
    bool showMachAlert;
    
    NSString *currentAlert;
    
    double airDensity;
    double temperature;
    double elevation;
    
    double rocketMass;
    double totalMotorMass;
    double propellentMass;

    double motorCaseMass;
    int numberMotors;
    int currentMotorType;
    NSString *motorCode;
    
    double cd;
    double frontalArea;
    double avgThrust;
    double burnTime;
    
    double calcK;
    double calcAvgThrustingMass;
    double calcBurnoutMass;
    double calcBurnoutVelocity;
    double calcBurnoutAltitude;
    double calcCoastAltitude;
    double calcPeakAltitude;
    double calcCoastTime;
    
    NSArray *bodyTubesArray;

    UIScrollView *scrollView;

    UITextField *activeField;
    UIActionSheet *sheet;
    
    // accessor properties
    
    UILabel *elevationLabel;
    UILabel *elevationUnitLabel;
    
    UILabel *temperatureLabel;
    UILabel *temperatureUnitLabel;
    
    UILabel *airDensityLabel;
    
    UILabel *thrustToWeightLabel;
    UILabel *motorClassLabel;
    
    UILabel *motorCaseMassUnitLabel;
    CalcTextfield *motorCaseMassTextfield;
    
    UILabel *totalMotorMassUnitLabel;
    CalcTextfield *totalMotorMassTextfield;
    
    UILabel *avgThrustUnitLabel;
    CalcTextfield *avgThrustTextfield;
    
    UILabel *burnTimeUnitLabel;
    CalcTextfield *burnTimeTextfield;
    
    UILabel *propellantMassUnitLabel;
    CalcTextfield *propellantMassTextfield;
    
    UILabel *rocketMassUnitLabel;
    CalcTextfield *rocketMassTextfield;
    
    CalcTextfield *numberMotorsTextfield;
    
    CalcTextfield *dragCoefficientTextfield;
    
    UILabel *frontalAreaUnitLabel;
    CalcTextfield *frontalAreaTextfield;
    
    UILabel *burnoutVelocityValueLabel;
    UILabel *burnoutVelocityUnitLabel;
    
    UILabel *burnoutAltitudeValueLabel;
    UILabel *burnoutAltitudeUnitLabel;
    
    UILabel *peakAltitudeValueLabel;
    UILabel *peakAltitudeUnitLabel;
    
    UILabel *coastTimeValueLabel;
    UILabel *coastTimeUnitLabel;
    
    UIButton *bodyTubeButton;
    UIButton *motorButton;

    UIButton *dataAssistantButton;
    UIButton *tempElevationButton;
    
    UILabel *motorCodeLabel;
    
    RAUnit *temperatureUnit;
    RAUnit *elevationUnit;
    RAUnit *avgThrustUnit;
    RAUnit *burnTimeUnit;
    RAUnit *motorMassUnit;
    RAUnit *rocketMassUnit;
    RAUnit *propellantMassUnit;
    RAUnit *areaUnit;
    RAUnit *caseMassUnit;
    
    RAUnit *velocityUnit;
    RAUnit *altitudeUnit;
    RAUnit *coastTimeUnit;
}

@property (nonatomic, retain) NSArray *bodyTubesArray;
@property (nonatomic ,retain) IBOutlet UIButton *bodyTubeButton;
@property (nonatomic, retain) IBOutlet UIButton *motorButton;

@property (nonatomic, readwrite) double temperature;
@property (nonatomic, readwrite) double elevation;

@property (nonatomic, readwrite) double totalMotorMass;
@property (nonatomic, readwrite) double motorCaseMass;
@property (nonatomic, readwrite) double propellentMass;
@property (nonatomic, readwrite) double avgThrust;
@property (nonatomic, readwrite) double burnTime;
@property (nonatomic, readwrite) double frontalArea;
@property (nonatomic, readwrite) int numberMotors;

@property (nonatomic, readwrite) double rocketMass;
@property (nonatomic, readwrite) double cd;
@property (nonatomic, readwrite) int currentMotorType;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain) IBOutlet UILabel *elevationLabel;
@property (nonatomic, retain) IBOutlet UILabel *elevationUnitLabel;

@property (nonatomic, retain) IBOutlet UILabel *temperatureLabel;
@property (nonatomic, retain) IBOutlet UILabel *temperatureUnitLabel;

@property (nonatomic, retain) IBOutlet UILabel *airDensityLabel;

@property (nonatomic, retain) IBOutlet UILabel *thrustToWeightLabel;
@property (nonatomic, retain) IBOutlet UILabel *motorClassLabel;

@property (nonatomic, retain) IBOutlet UILabel *motorCaseMassUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *motorCaseMassTextfield;

@property (nonatomic, retain) IBOutlet UILabel *totalMotorMassUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *totalMotorMassTextfield;

@property (nonatomic, retain) IBOutlet UILabel *avgThrustUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *avgThrustTextfield;

@property (nonatomic, retain) IBOutlet UILabel *burnTimeUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *burnTimeTextfield;

@property (nonatomic, retain) IBOutlet UILabel *propellantMassUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *propellantMassTextfield;

@property (nonatomic, retain) IBOutlet UILabel *rocketMassUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *rocketMassTextfield;

@property (nonatomic, retain) IBOutlet CalcTextfield *numberMotorsTextfield;

@property (nonatomic, retain) IBOutlet CalcTextfield *dragCoefficientTextfield;

@property (nonatomic, retain) IBOutlet UILabel *frontalAreaUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *frontalAreaTextfield;

@property (nonatomic, retain) IBOutlet UILabel *burnoutVelocityValueLabel;
@property (nonatomic, retain) IBOutlet UILabel *burnoutVelocityUnitLabel;

@property (nonatomic, retain) IBOutlet UILabel *burnoutAltitudeValueLabel;
@property (nonatomic, retain) IBOutlet UILabel *burnoutAltitudeUnitLabel;

@property (nonatomic, retain) IBOutlet UILabel *peakAltitudeValueLabel;
@property (nonatomic, retain) IBOutlet UILabel *peakAltitudeUnitLabel;

@property (nonatomic, retain) IBOutlet UILabel *coastTimeValueLabel;
@property (nonatomic, retain) IBOutlet UILabel *coastTimeUnitLabel;

@property (nonatomic, retain) IBOutlet UIButton *dataAssistantButton;
@property (nonatomic, retain) IBOutlet UIButton *tempElevationButton;

@property (nonatomic, retain) IBOutlet UILabel *motorCodeLabel;

- (IBAction)openBodyTubeViewController:(id)sender;
- (IBAction)openMotorViewController:(id)sender;
- (NSArray *)loadBodyTubesArray;
- (IBAction)openDataAssistant:(id)sender;
- (IBAction)openDensityCalculatorViewController:(id)sender;

- (IBAction)updateFields:(id)sender;
- (IBAction)infoButtonAction:(id)sender;
- (IBAction) exportButtonAction:(id)sender;
- (IBAction) showEmailModalView:(id)sender;
- (IBAction) printCalculation:(id)sender;
- (NSString *) exportMsgText;
- (NSString *) exportMsgHtml;
- (void) enableTextfields;
- (void) resignAllResponders;
- (void) resetTextfieldDisplay;
- (void)showAlert:(NSString *)m;
- (void)showAlert:(NSString *)m fromSendingAlert:(NSString *)a;

@end
