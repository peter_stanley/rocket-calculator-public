//
//  MotorSelectListViewCell.h
//  RocketCalculator
//
//  Created by Peter Stanley on 03/25/12.
//  Copyright 2012 Peter Stanley. All rights reserved.
//


@interface MotorSelectListViewCell : UITableViewCell {
    
    UILabel *label;
    UILabel *labelDetail;
}

@property (nonatomic, retain) IBOutlet UILabel *label;
@property (nonatomic, retain) IBOutlet UILabel *modelNumber;
@property (nonatomic, retain) IBOutlet UILabel *labelDetail;
@property (nonatomic, retain) IBOutlet UILabel *propellantType;

@end
