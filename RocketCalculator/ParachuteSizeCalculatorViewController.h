//
//  ParachuteSizeCalculatorViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

@class RAUnit;
@class CalcTextfield;

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "DensityCalculatorViewController.h"

@interface ParachuteSizeCalculatorViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIPrintInteractionControllerDelegate, DensityCalculatorViewControllerDelegate>
{
    bool isIpad;
    
    double airDensity;
    double temperature;
    double elevation;

    double mass;
    double velocity;
    double Cd;
    double diameter;
    int chuteShape;
    double spillHoleDiameter;
    
    UILabel *elevationLabel;
    UILabel *elevationUnitLabel;
    
    UILabel *temperatureLabel;
    UILabel *temperatureUnitLabel;
    
    UILabel *airDensityLabel;

    CalcTextfield *massTextField;
    UILabel *massUnitLabel;
    CalcTextfield *velocityTextField;
    UILabel *velocityUnitLabel;
    CalcTextfield *diameterTextField;
    UILabel *diameterUnitLabel;
    CalcTextfield *spillHoleDiameterTextField;
    UILabel *spillHoleUnitLabel;
    CalcTextfield *cdOverrideTextField;
    UISegmentedControl *chuteTypeSegmentedControl;
    UISegmentedControl *chuteShapeSegmentedControl;
    UILabel *chuteShapelabel;
    UIButton *resetDescentSpeedButton;
    UIButton *tempElevationButton;
    
    RAUnit *temperatureUnit;
    RAUnit *elevationUnit;
    RAUnit *massUnit;
    RAUnit *velocityUnit;
    RAUnit *diameterUnit;
    RAUnit *spillHoleUnit;
    
    UIScrollView *scrollView;
    // Rocket *rocket;
    // FlightLog *flightLog;
    
    UITextField *activeField;
    UIActionSheet *sheet;

}

@property (nonatomic, readwrite) double temperature;
@property (nonatomic, readwrite) double elevation;

@property (nonatomic, retain) IBOutlet UILabel *elevationLabel;
@property (nonatomic, retain) IBOutlet UILabel *elevationUnitLabel;

@property (nonatomic, retain) IBOutlet UILabel *temperatureLabel;
@property (nonatomic, retain) IBOutlet UILabel *temperatureUnitLabel;

@property (nonatomic, retain) IBOutlet UILabel *airDensityLabel;

@property (nonatomic, retain) IBOutlet CalcTextfield *massTextField;
@property (nonatomic, retain) IBOutlet UILabel *massUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *velocityTextField;
@property (nonatomic, retain) IBOutlet UILabel *velocityUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *diameterTextField;
@property (nonatomic, retain) IBOutlet UILabel *diameterUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *spillHoleDiameterTextField;
@property (nonatomic, retain) IBOutlet UILabel *spillHoleUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *cdOverrideTextField;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UISegmentedControl *chuteTypeSegmentedControl;
@property (nonatomic, retain) IBOutlet UISegmentedControl *chuteShapeSegmentedControl;
@property (nonatomic, retain) IBOutlet UILabel *chuteShapeLabel;
@property (nonatomic, retain) IBOutlet UIButton *descentSpeedButton;
@property (nonatomic, retain) IBOutlet UIButton *tempElevationButton;

- (IBAction)openDensityCalculatorViewController:(id)sender;
- (IBAction)updateFields:(id)sender;
- (IBAction)changeChuteType:(id)sender;
- (IBAction)changeChuteShape:(id)sender;
- (IBAction)resetDescentSpeed:(id)sender;
- (IBAction)infoButtonAction:(id)sender;
- (IBAction) exportButtonAction:(id)sender;
- (IBAction) showEmailModalView:(id)sender;
- (IBAction) printCalculation:(id)sender;
- (NSString *) exportMsgText;
- (NSString *) exportMsgHtml;
- (void) enableTextfields;
- (void) resignAllResponders;
- (void) resetTextfieldDisplay;

@end
