//
//  DensityCalculatorViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DensityCalculatorViewController.h"
#import "RAUnit.h"
#import "RocketCalculatorAppDelegate.h"
#import "Calculator.h"
#import "CalcTextfield.h"

@implementation DensityCalculatorViewController

#define ALTITUDE_TEXTFIELD 1
#define TEMPERATURE_TEXTFIELD 2

#define REQUIRED_COLOR redColor

@synthesize altitude, temperature, delegate, scrollView, altitudeUnitLabel, altitudeTextfield, temperatureUnitLabel, temperatureTextfield, densityLabel, doneButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)registerForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unRegisterForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    RocketCalculatorAppDelegate *appDelegate = (RocketCalculatorAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown) {
        
        CGFloat tabBarHeight = appDelegate.tabBarController.tabBar.frame.size.height;
        
        NSDictionary* info = [aNotification userInfo];
        
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        
        self.scrollView.contentInset = contentInsets;
        
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        CGRect aRect = self.view.frame;
        
        aRect.size.height -= kbSize.height;
        
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height+tabBarHeight);
            
            [scrollView setContentOffset:scrollPoint animated:YES];
            
        }
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];
    
    temperature = 0;
    altitude = 0;
    
    CGFloat frameHeight = self.view.frame.size.height;
    CGFloat frameWidth = self.view.frame.size.width;
    
    scrollView.contentSize=CGSizeMake(frameWidth, frameHeight /2);

    self.title = @"Air Density Calculator";
    
	UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateFields:)];
	gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
	[gestureRecognizer release];
    // units setup
    
    temperatureUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_temperature"] ofDimension:@"temperature"];
    
    altitudeUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"alt_calc_elevation"] ofDimension:@"height"];
                    
    [self resetTextfieldDisplay];
    [self enableTextfields];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
        
    temperature = [[[NSUserDefaults standardUserDefaults] valueForKey:@"temperature"] doubleValue];
        
    altitude = [[[NSUserDefaults standardUserDefaults] stringForKey:@"elevation"] doubleValue];
    
    // unit labels/placeholders, etc... setup
    
    self.altitudeUnitLabel.text = [NSString stringWithFormat:@"(%@)", altitudeUnit.name];
    self.altitudeTextfield.placeholder = [NSString stringWithFormat:@"%@", altitudeUnit.label];
        
    self.temperatureUnitLabel.text = [NSString stringWithFormat:@"(%@)", temperatureUnit.name];
    self.temperatureTextfield.placeholder = [NSString stringWithFormat:@"%@", temperatureUnit.label];

    self.temperatureTextfield.text = [NSString stringWithFormat:@"%.2f", [[temperatureUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:temperature] toUnit:temperatureUnit]doubleValue]];
    self.altitudeTextfield.text = [NSString stringWithFormat:@"%.2f", [[altitudeUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:altitude] toUnit:altitudeUnit]doubleValue]];

    [self updateFields:nil];
    [self resetTextfieldDisplay];
    [self enableTextfields];
    
}

- (void)willEnterForeground
{
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self resetTextfieldDisplay];
    [self.view setNeedsDisplay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unRegisterForKeyboardNotifications];
}

- (void)viewDidUnload
{
    
    self.doneButton = nil;
    
    self.densityLabel = nil;
    
    self.temperatureTextfield = nil;
    self.temperatureUnitLabel = nil;
    
    self.altitudeTextfield = nil;
    self.altitudeUnitLabel = nil;
    
    self.scrollView = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc
{
    
    [doneButton release];
        
    [densityLabel release];
    
    [temperatureTextfield release];
    [temperatureUnitLabel release];
    
    [altitudeTextfield release];
    [altitudeUnitLabel release];
    
    [scrollView release];
    
    [super dealloc];
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (isIpad) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    } else {
        //textField.keyboardType = UIKeyboardTypeDecimalPad;
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }
    
    activeField = textField;
    
    
    if (textField.tag == ALTITUDE_TEXTFIELD) {

        altitudeTextfield.text = nil;
        altitude = 0;
        
        [temperatureTextfield setUserInteractionEnabled:NO];
        
    } else if (textField.tag == TEMPERATURE_TEXTFIELD) {
        
        temperatureTextfield.text = nil;
        temperature = 0;
        
        [altitudeTextfield setUserInteractionEnabled:NO];
        
    }
    
    [densityLabel setAlpha:0.25];
        
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    activeField = nil;
    
}

- (IBAction)updateFields:(id)sender
{

    [self resignAllResponders];
    [self enableTextfields];
        
    // Make sure all required fields are filled
    requiredCheck = 0;
    
    if ((altitudeTextfield.text == nil || [altitudeTextfield.text isEqualToString:@""]) || temperatureTextfield.text == nil || [temperatureTextfield.text isEqualToString:@""]) {
        
        requiredCheck = 0;
        
    } else {
        
        requiredCheck = 1;
        
    }
    
    if (requiredCheck) {
        
        altitude = [altitudeTextfield.text doubleValue];
        temperature = [temperatureTextfield.text doubleValue];
        //        coastTimeValueLabel.text = [NSString stringWithFormat:@"%.2f", [[coastTimeUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcCoastTime] toUnit:coastTimeUnit]doubleValue]];

        
        // Get base units
        double baseAltitude = [[altitudeUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:altitude]] doubleValue];
        
        double baseTemperature = [[temperatureUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:temperature]] doubleValue];
                        
        double temperatureInKelvin = baseTemperature + 273.15;
        
        // Calc values
        Calculator *calculator = [[Calculator alloc] init];
        
        double calcDensity = [calculator calcAirDensityFromAltitudeInMeters:baseAltitude andTemperatureInKelvin:temperatureInKelvin];
                
        [calculator release];
                
        // Set values
        
        densityLabel.text = [NSString stringWithFormat:@"%.3f",calcDensity];
            
        [densityLabel setAlpha:1];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setValue:[NSString stringWithFormat:@"%f", baseTemperature] forKey:@"temperature"];
        [prefs setValue:[NSString stringWithFormat:@"%f", baseAltitude] forKey:@"elevation"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];

        
    }
    
    [self resetTextfieldDisplay];
}

- (void)enableTextfields
{

    [altitudeTextfield setUserInteractionEnabled:YES];
    [temperatureTextfield setUserInteractionEnabled:YES];
    
}

- (void) resignAllResponders
{
    
    [altitudeTextfield resignFirstResponder];
    [temperatureTextfield resignFirstResponder];
    
}

- (void) resetTextfieldDisplay
{

    // Tint required fields
    if (altitudeTextfield.text == nil || [altitudeTextfield.text isEqualToString:@""]) {
        
        [self.altitudeTextfield setRequired:YES];
    } else {
        
        [self.altitudeTextfield setRequired:NO];
    }
    
    if (temperatureTextfield.text == nil || [temperatureTextfield.text isEqualToString:@""]) {
        
        [self.temperatureTextfield setRequired:YES];
    } else {
        
        [self.temperatureTextfield setRequired:NO];
    }
    
}

- (void)dismissViewController:(id)sender
{
    
    [delegate densityCalculatorViewControllerWasDismissed:self];
    
   // [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end