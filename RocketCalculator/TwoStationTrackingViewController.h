//
//  TwoStationTrackingViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

@class RAUnit;
@class CalcTextfield;

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface TwoStationTrackingViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIPrintInteractionControllerDelegate> 
{
    bool isIpad;
    
    double elevation1;
    double azimuth1;
    double elevation2;
    double azimuth2;
    double baselineLength;
    double alt1;
    double alt2;
    double avgAlt;
    
    CalcTextfield *elevation1Textfield;
    CalcTextfield *azimuth1Textfield;
    CalcTextfield *elevation2Textfield;
    CalcTextfield *azimuth2Textfield;
    CalcTextfield *baselineLengthTextfield;
    
    UILabel *elevation1UnitLabel;
    UILabel *azimuth1UnitLabel;
    UILabel *elevation2UnitLabel;
    UILabel *azimuth2UnitLabel;
    UILabel *baselineLengthUnitLabel;
    UILabel *alt1Label;
    UILabel *alt1UnitLabel;
    UILabel *alt2Label;
    UILabel *alt2UnitLabel;
    UILabel *avtAltLabel;
    UILabel *avgAltUnitLabel;
    UILabel *closureErrorLabel;
    
    RAUnit *elevationAngleUnit;
    RAUnit *azimuthAngleUnit;
    RAUnit *baselineLengthUnit;
    RAUnit *altitudeUnit;
    
    UIScrollView *scrollView;
    // Rocket *rocket;
    // FlightLog *flightLog;
    
    UITextField *activeField;
    UIActionSheet *sheet;

}

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet CalcTextfield *elevation1Textfield;
@property (nonatomic, retain) IBOutlet CalcTextfield *azimuth1Textfield;
@property (nonatomic, retain) IBOutlet CalcTextfield *elevation2Textfield;
@property (nonatomic, retain) IBOutlet CalcTextfield *azimuth2Textfield;
@property (nonatomic, retain) IBOutlet CalcTextfield *baselineLengthTextfield;

@property (nonatomic, retain) IBOutlet UILabel *elevation1UnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *azimuth1UnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *elevation2UnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *azimuth2UnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *baselineLengthUnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *alt1Label;
@property (nonatomic, retain) IBOutlet UILabel *alt1UnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *alt2Label;
@property (nonatomic, retain) IBOutlet UILabel *alt2UnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *avtAltLabel;
@property (nonatomic, retain) IBOutlet UILabel *avgAltUnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *closureErrorLabel;

- (IBAction)updateFields:(id)sender;
- (IBAction)infoButtonAction:(id)sender;
- (IBAction) exportButtonAction:(id)sender;
- (IBAction) showEmailModalView:(id)sender;
- (IBAction) printCalculation:(id)sender;
- (NSString *) exportMsgText;
- (NSString *) exportMsgHtml;
- (void) enableTextfields;
- (void) resignAllResponders;
- (void) resetTextfieldDisplay;

@end
