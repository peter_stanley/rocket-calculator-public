//
//  DensityCalculatorViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 1/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

@protocol DensityCalculatorViewControllerDelegate;

@class RAUnit;
@class CalcTextfield;

#import <UIKit/UIKit.h>

@interface DensityCalculatorViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate>
{

    id <DensityCalculatorViewControllerDelegate> delegate;

    double altitude;
    double temperature;
    
    bool isIpad;
    bool requiredCheck;
    
    UIScrollView *scrollView;

    UITextField *activeField;
    
    UILabel *densityLabel;
    
    UILabel *altitudeUnitLabel;
    CalcTextfield *altitudeTextfield;
    
    UILabel *temperatureUnitLabel;
    CalcTextfield *temperatureTextfield;
        
    RAUnit *altitudeUnit;
    RAUnit *temperatureUnit;
    
    UIButton *doneButton;
    
}

@property (nonatomic, assign) id<DensityCalculatorViewControllerDelegate> delegate;

@property (nonatomic, readwrite) double altitude;
@property (nonatomic, readwrite) double temperature;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain) IBOutlet UILabel *densityLabel;

@property (nonatomic, retain) IBOutlet UILabel *altitudeUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *altitudeTextfield;

@property (nonatomic, retain) IBOutlet UILabel *temperatureUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *temperatureTextfield;

@property (nonatomic, retain) IBOutlet UIButton *doneButton;

- (IBAction)dismissViewController:(id)sender;
- (IBAction)updateFields:(id)sender;

- (void) enableTextfields;
- (void) resignAllResponders;
- (void) resetTextfieldDisplay;

@end

@protocol DensityCalculatorViewControllerDelegate

- (void)densityCalculatorViewControllerWasDismissed:(DensityCalculatorViewController*)densityCalculatorViewController;

@end
