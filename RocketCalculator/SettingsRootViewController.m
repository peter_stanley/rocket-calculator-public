//
//  SettingsRootViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 4/3/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import "SettingsRootViewController.h"
#import "UnitPreferenceViewController.h"

@implementation SettingsRootViewController

@synthesize settingsTableView;

- (id)init {
	
    self = [super init];
    
	if (self == [super initWithNibName:@"SettingsRootViewController" bundle:nil]) {
		
		self.title = @"App Info";
	}
	
	return self;
	
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	return [self init];
}

- (void)dealloc
{

    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    //[super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"App Info";
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - TableView methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

	return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *RootSettingsCellIdentifier = @"rootSettings";
	
    UITableViewCell  *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:RootSettingsCellIdentifier];

    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:RootSettingsCellIdentifier] autorelease];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    cell.textLabel.text = @"Unit Preferences";
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        {
            UnitPreferenceViewController *unitPreferenceViewController = [[UnitPreferenceViewController alloc] init];
                        
            [self.navigationController pushViewController:unitPreferenceViewController animated:YES];
            [unitPreferenceViewController release];

        }
            break;
            
        default:
            break;
    }
    
    // Deselect the row.
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}


@end
