//
//  SettingSelectListViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 4/30/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import "SettingSelectListViewController.h"

@implementation SettingSelectListViewController

@synthesize listDict, settingKey;

#pragma mark -
/*
- (id)initWithStyle:(UITableViewStyle)style {
    if ( (self = [super initWithStyle:style]) ) {
        self.title = [listDict objectForKey:@"Title"];
        // initialSelection = -1;
    }
    return self;
}
*/
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [listDict objectForKey:@"Title"];
}

- (void)viewDidUnload {
    self.settingKey = nil;
    self.listDict = nil;
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated 
{
    // Get list
    
    [self.tableView reloadData];
    [super viewWillAppear:animated];
    
}


- (void)dealloc 
{
    [settingKey release];
    [listDict release];
    
    [super dealloc];
}
#pragma mark -
#pragma mark Tableview methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    return [[listDict objectForKey:@"Values"] count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *SelectionListCellIdentifier = @"SelectionListCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SelectionListCellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SelectionListCellIdentifier] autorelease];
    }
    
    if ([[[listDict objectForKey:@"Values"] objectAtIndex:indexPath.row] isEqualToString:
       [[NSUserDefaults standardUserDefaults] stringForKey:settingKey]]) {
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
     
    cell.textLabel.text = [[listDict objectForKey:@"Titles"] objectAtIndex:indexPath.row]; 

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // If there was a previous selection, unset the accessory view for its cell.
    if ([NSUserDefaults standardUserDefaults]) {
        
    NSInteger index = [[listDict objectForKey:@"Values"] indexOfObject:[[NSUserDefaults standardUserDefaults] stringForKey:settingKey]];
    NSIndexPath *selectionIndexPath = [NSIndexPath indexPathForRow:index inSection:indexPath.section];
    UITableViewCell *checkedCell = [tableView cellForRowAtIndexPath:selectionIndexPath];
    checkedCell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    // Set the checkmark accessory for the selected row.
    [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
    
    // Update the userdefaults with new value
    [[NSUserDefaults standardUserDefaults] setObject:[[listDict objectForKey:@"Values"] objectAtIndex:indexPath.row] forKey:settingKey];
    [[NSUserDefaults standardUserDefaults] synchronize];

    // Deselect the row
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end