//
//  GenericFormulaViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 11/17/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GenericFormulaViewController : UIViewController <UIWebViewDelegate>
{
    NSString *htmlFileName;
    UIWebView *webView;
    UIButton *dismissButton;
}

@property (nonatomic, retain) NSString *htmlFileName;
@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet UIButton *dismissButton;

- (IBAction)dismissView:(id)sender;

@end
