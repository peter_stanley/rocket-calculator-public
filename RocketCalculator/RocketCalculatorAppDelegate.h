//
//  RocketCalculatorAppDelegate.h
//  RocketCalculator
//
//  Created by Peter Stanley on 11/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//
//#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

@class CalculatorRootViewController;
@class InformationListViewController;

@class RAUnit;

@interface RocketCalculatorAppDelegate : UIResponder <UIApplicationDelegate>
{
    UITabBarController *tabBarController;

    CalculatorRootViewController *calculatorRootViewController;
    InformationListViewController *infoListRootViewController;
    
    NSMutableDictionary *unitDict;
	NSDictionary *unitLabels;

}

// Class method for convenience

@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;

@property (nonatomic, retain) IBOutlet CalculatorRootViewController *calculatorRootViewController;
@property (nonatomic, retain) IBOutlet InformationListViewController *infoListRootViewController;

@property (nonatomic, retain) NSMutableDictionary *unitDict;
@property (nonatomic, retain) NSDictionary *unitLabels;

- (NSString *)applicationDocumentsDirectory;
- (NSArray *) unitsOfDimension:(NSString *)dimension;
- (RAUnit *) unitWithName:(NSString *)name ofDimension:(NSString *)dimension;
- (BOOL) deviceIsIpad;

@end
