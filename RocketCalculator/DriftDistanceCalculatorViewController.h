//
//  DriftDistanceCalculatorViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

@class RAUnit;
@class CalcTextfield;

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface DriftDistanceCalculatorViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIPrintInteractionControllerDelegate> 
{
    bool isIpad;
    
    double altitude;
    double descentRate;
    double windSpeed;
    double descentDuration;
    
    CalcTextfield *altitudeTextField;
    UILabel *altitudeUnitLabel;
    CalcTextfield *descentRateTextField;
    UILabel *descentRateUnitLabel;
    CalcTextfield *windSpeedTextField;
    UILabel *windSpeedUnitLabel;
    UILabel *descentTimeLabel;
    UILabel *descentTimeUnitLabel;
    UILabel *driftDistanceLabel;
    UILabel *driftDistanceUnitLabel;
    
    RAUnit *altitudeUnit;
    RAUnit *descentRateUnit;
    RAUnit *windSpeedUnit;
    RAUnit *descentTimeUnit;
    RAUnit *driftDistanceUnit;
    
    UIScrollView *scrollView;
    // Rocket *rocket;
    // FlightLog *flightLog;
    
    UITextField *activeField;
    UIActionSheet *sheet;

}

@property (nonatomic, retain) IBOutlet CalcTextfield *altitudeTextField;
@property (nonatomic, retain) IBOutlet UILabel *altitudeUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *descentRateTextField;
@property (nonatomic, retain) IBOutlet UILabel *descentRateUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *windSpeedTextField;
@property (nonatomic, retain) IBOutlet UILabel *windSpeedUnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *descentTimeLabel;
@property (nonatomic, retain) IBOutlet UILabel *descentTimeUnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *driftDistanceLabel;
@property (nonatomic, retain) IBOutlet UILabel *driftDistanceUnitLabel;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

- (IBAction)updateFields:(id)sender;
- (IBAction)infoButtonAction:(id)sender;
- (IBAction) exportButtonAction:(id)sender;
- (IBAction) showEmailModalView:(id)sender;
- (IBAction) printCalculation:(id)sender;
- (NSString *) exportMsgText;
- (NSString *) exportMsgHtml;
- (void) enableTextfields;
- (void) resignAllResponders;
- (void) resetTextfieldDisplay;

@end