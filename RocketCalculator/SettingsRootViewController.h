//
//  SettingsRootViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 4/3/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SettingsRootViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
@private

    UITableView *settingsTableView;
}

@property (nonatomic, retain) IBOutlet UITableView *settingsTableView;

@end
