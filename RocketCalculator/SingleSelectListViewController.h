//
//  SingleSelectListViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 3/25/12.
//  Copyright 2012 Peter Stanley. All rights reserved.
//

@protocol SingleSelectListViewControllerDelegate;

@class SingleSelectListViewCell;

@interface SingleSelectListViewController : UIViewController < UITableViewDelegate, UITableViewDataSource>
{
    id <SingleSelectListViewControllerDelegate> delegate;
    
    NSString *listName;
    NSUInteger selectedRow;
    
    SingleSelectListViewCell *singleSelectListViewCell;
	NSArray	*itemList;

    IBOutlet UITableView *singleSelectListTableView;
    BOOL isIpad;
        
    UIButton *doneButton;
}

@property (nonatomic, assign) id<SingleSelectListViewControllerDelegate> delegate;
@property (nonatomic, readwrite) NSUInteger selectedRow;
@property (nonatomic, retain) NSString *listName;
@property (nonatomic, assign) IBOutlet SingleSelectListViewCell *singleSelectListViewCell;
@property (nonatomic, retain) NSArray *itemList;
@property (nonatomic, retain) IBOutlet UITableView *singleSelectListTableView;
@property (nonatomic, retain) IBOutlet UIButton *doneButton;

- (IBAction)dismissViewController:(id)sender;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end

@protocol SingleSelectListViewControllerDelegate
- (void)singleSelectListViewControllerWasDismissed:(SingleSelectListViewController*)singleSelectListViewController;
@end

