//
//  SingleStationTrackingViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

#import "SingleStationTrackingViewController.h"
#import "RAUnit.h"
#import "RocketCalculatorAppDelegate.h"
#import "Calculator.h"
#import "GenericFormulaViewController.h"
#import "CalcTextfield.h"

@implementation SingleStationTrackingViewController

#define ELEVATION1_TEXTFIELD 1
#define BASELINELENGTH_TEXTFIELD 2


@synthesize scrollView, elevation1Textfield, baselineLengthTextfield;
@synthesize elevation1UnitLabel, baselineLengthUnitLabel;
@synthesize alt1Label, alt1UnitLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)registerForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unRegisterForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardDidShowNotification 
                                                  object:nil]; 
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];  
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    RocketCalculatorAppDelegate *appDelegate = (RocketCalculatorAppDelegate*)[[UIApplication sharedApplication] delegate];
        
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown) {
        
        CGFloat tabBarHeight = appDelegate.tabBarController.tabBar.frame.size.height;
        
        NSDictionary* info = [aNotification userInfo];
        
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        
        self.scrollView.contentInset = contentInsets;
        
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        CGRect aRect = self.view.frame;
        
        aRect.size.height -= kbSize.height;
        
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height+tabBarHeight);
            
            [scrollView setContentOffset:scrollPoint animated:YES];
            
        }
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    elevation1 = 0;
    baselineLength = 0;
    
    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];

    CGFloat frameHeight = self.view.frame.size.height;
    CGFloat frameWidth = self.view.frame.size.width;
    
    UIScrollView *tempScrollView=(UIScrollView *)self.view;
    tempScrollView.contentSize=CGSizeMake(frameWidth, frameHeight * 2);

    self.title = @"Single-Station Tracking";
    
	UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateFields:)];
	gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
	[gestureRecognizer release];
    
    elevationAngleUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_s_tracking_e_angle"] ofDimension:@"angle"];
    
    baselineLengthUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_s_tracking_baseline"] ofDimension:@"length"];
    
    altitudeUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_s_tracking_altitude"] ofDimension:@"height"];

    // Setup toolbar
    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 75.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    // Add actionsheet button
    UIButton *exportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exportButton.frame = CGRectMake(0, 0, 25, 20); //set frame for button
    
    UIImage *buttonImage = [UIImage imageNamed:@"action2.png"];
    
    [exportButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [exportButton addTarget:self action:@selector(exportButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:exportButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    
    // Create a spacer.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    bi.width = 8.0f;
    [buttons addObject:bi];
    [bi release];
    
    // Add info button
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    infoButton.frame = CGRectMake(0, 0, 20, 20); //set frame for button
    
    buttonImage = [UIImage imageNamed:@"info_g.png"];
    
    [infoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    self.navigationController.navigationBar.translucent = NO;
    [tools setItems:buttons animated:NO];
    [buttons release];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    [tools release];
    self.navigationItem.rightBarButtonItem = twoButtons;
    
    [twoButtons release];
    
    [self resetTextfieldDisplay];
}

- (IBAction)infoButtonAction:(id)sender {
    
    GenericFormulaViewController *infoView = [[[GenericFormulaViewController alloc] init] autorelease];
    infoView.htmlFileName = @"singleStationTracking";
    [infoView setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
   // [self presentModalViewController:infoView animated:YES];
    [self presentViewController:infoView animated:YES completion:nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.elevation1UnitLabel.text = [NSString stringWithFormat:@"(%@)", elevationAngleUnit.name];
    self.elevation1Textfield.placeholder = [NSString stringWithFormat:@"%@", elevationAngleUnit.label];    
    self.baselineLengthUnitLabel.text = [NSString stringWithFormat:@"(%@)", baselineLengthUnit.name];
    self.baselineLengthTextfield.placeholder = [NSString stringWithFormat:@"%@", baselineLengthUnit.label];
    self.alt1UnitLabel.text = [NSString stringWithFormat:@"(%@)", altitudeUnit.name];
    
    [self resetTextfieldDisplay];
}

- (void)willEnterForeground
{

    [[NSUserDefaults standardUserDefaults] synchronize];
    [self resetTextfieldDisplay];
    [self.view setNeedsDisplay];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unRegisterForKeyboardNotifications];
    
}

- (void)viewDidUnload
{
    self.alt1UnitLabel = nil;
    self.alt1Label = nil;
    self.baselineLengthUnitLabel = nil;
    self.elevation1UnitLabel = nil;
    self.baselineLengthTextfield = nil;
    self.elevation1Textfield = nil;
    self.scrollView = nil;
    
    [super viewDidUnload];

}

- (void)dealloc
{
    
    [alt1UnitLabel release];
    [alt1Label release];
    [baselineLengthUnitLabel release];
    [elevation1UnitLabel release];
    [baselineLengthTextfield release];
    [elevation1Textfield release];
    [scrollView release];
    
    [super dealloc];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    
    if (isIpad) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    } else {
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }

    activeField = textField;
    
    switch (textField.tag) {
        case ELEVATION1_TEXTFIELD:
            
            elevation1 = 0;
            elevation1Textfield.text = nil;
            [self.baselineLengthTextfield setUserInteractionEnabled:NO];
            
            break;
                        
        case BASELINELENGTH_TEXTFIELD:
            
            baselineLength = 0;
            baselineLengthTextfield.text = nil;
            [self.elevation1Textfield setUserInteractionEnabled:NO];
            
            break;
            
        default:
            break;
    }
    
    [alt1Label setAlpha:0.25];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    activeField = nil;
    
}
- (IBAction)updateFields:(id)sender
{
    
    [self resignAllResponders];
    [self enableTextfields];
    
    if (!elevation1) {
        
        elevation1 = [elevation1Textfield.text doubleValue];
        
    }
    
    if (!baselineLength) {
        
        baselineLength = [baselineLengthTextfield.text doubleValue];
        
    }
    
    double calcAlt1 = 0;
    
    // get base units
    double baseElevation1 = [[elevationAngleUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:elevation1]] doubleValue];
        
    double baseBaselineLength = [[baselineLengthUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:baselineLength]] doubleValue];
    
    Calculator *calculator = [[Calculator alloc] init];
    
    calcAlt1 = [calculator calcAltitudeFromBaseline:baseBaselineLength andElevation:baseElevation1];
    
    alt1Label.text = [NSString stringWithFormat:@"%.2f", [[altitudeUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcAlt1] toUnit:altitudeUnit]floatValue]];

    [calculator release];
    
    [alt1Label setAlpha:1.0];
    
    [self resetTextfieldDisplay];
    
}

-(IBAction) showEmailModalView:(id)sender 
{
    
    if([MFMailComposeViewController canSendMail]){
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        
        if (picker) {
    
            picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
            NSString *mailSubject = [NSString stringWithFormat:@"%@ Calculation", self.title];
            NSString *mailMsg = [self exportMsgHtml];
    
            [picker setSubject:mailSubject];
    
            [picker setMessageBody:mailMsg isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
    
            picker.navigationBar.barStyle = UIBarStyleDefault; // choose your style, unfortunately, Translucent colors behave quirky.
    
            //[self presentModalViewController:picker animated:YES];
            [self presentViewController:picker animated:YES completion:nil];
            [picker release];
            
        }
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Mail Accounts" message:@"Please set up a Mail account in order to send email." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        
        
    }

    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{ 
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-("
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
            
            break;
    }
   // [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)exportButtonAction:(id)sender
{
    if (![UIPrintInteractionController isPrintingAvailable]) {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Email Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", nil];
        
    } else {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Export Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", @"Print", nil];
    }
    // Show the sheet
    [sheet showFromTabBar:self.tabBarController.tabBar];
    [sheet release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    //NSLog(@"Button %d", buttonIndex);
    switch (buttonIndex) {
        case 0:
            [self showEmailModalView:nil];
            break;
            
        case 1:
            [self printCalculation:nil];
            break;
            
        default:
            break;
    }
}

#pragma mark -

#pragma mark Printing



// Print button handler that sends our print instructions to the OS

- (IBAction)printCalculation:(id)sender {
    
    // Get a reference to the singleton iOS printing concierge
    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    
    if(pic && [UIPrintInteractionController isPrintingAvailable]) {
        //  UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = self.title;
        pic.printInfo = printInfo;
        
        // NSString *msg = @"dsfs fs ff sdf asf asf safs";
        NSString *msg = [self exportMsgText];
        // NSLog(@"msg = %@", msg);
        UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc]
                                                     initWithText:msg];
        textFormatter.startPage = 0;
        textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
        textFormatter.maximumContentWidth = 6 * 72.0;
        pic.printFormatter = textFormatter;
        [textFormatter release];
        pic.showsPageRange = YES;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSLog(@"Printing could not complete because of error: %@", error);
            }
        };
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [pic presentFromRect:self.tabBarController.tabBar.frame inView:self.view animated:YES completionHandler:completionHandler];
        } else {
            [pic presentAnimated:YES completionHandler:completionHandler];
        }
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Printing Error" message:@"Your device doesn't support printing" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    
}

- (NSString *) exportMsgText
{
    NSString *title = self.title;
    
    NSString *elevationString = elevation1Textfield.text;
    NSString *elevationUnitString = elevationAngleUnit.name;
    
    NSString *baselineLengthString = baselineLengthTextfield.text;
    NSString *baselineLengthUnitString = baselineLengthUnit.name;
    
    NSString *altitudeString = alt1Label.text;
    NSString *altitudeUnitString = altitudeUnit.name;
    
    NSString *msg = [NSString stringWithFormat:@"%@\n\nElevation Angle: %@ (%@)\nBaseline Length: %@ (%@)\nAltitude: %@ (%@)\n\n\nGenerated by Rocket Calculator\n\nAvailable on the Apple App Store",title, elevationString, elevationUnitString, baselineLengthString, baselineLengthUnitString, altitudeString, altitudeUnitString];
    
    return msg;

}

- (NSString *) exportMsgHtml
{
    NSString *title = self.title;
    
    NSString *elevationString = elevation1Textfield.text;
    NSString *elevationUnitString = elevationAngleUnit.name;
    
    NSString *baselineLengthString = baselineLengthTextfield.text;
    NSString *baselineLengthUnitString = baselineLengthUnit.name;
    
    NSString *altitudeString = alt1Label.text;
    NSString *altitudeUnitString = altitudeUnit.name;
    
    NSString *msg = [NSString stringWithFormat:@"%@<br /><br />Elevation Angle: %@ (%@)<br />Baseline Length: %@ (%@)<br />Altitude: %@ (%@)<br /><br /><br />Generated by Rocket Calculator<br /><br />Available on the Apple <a href=\"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=484673201&mt=8\">App Store</a>",title, elevationString, elevationUnitString, baselineLengthString, baselineLengthUnitString, altitudeString, altitudeUnitString];
    
    return msg;

}

- (void)enableTextfields
{
    
    [elevation1Textfield setUserInteractionEnabled:YES];
    [baselineLengthTextfield setUserInteractionEnabled:YES];
    
}

- (void) resignAllResponders
{
    
    [elevation1Textfield resignFirstResponder];
    [baselineLengthTextfield resignFirstResponder];
    
}

- (void) resetTextfieldDisplay
{
    
    // Tint required fields
    
    if (elevation1Textfield.text == nil || [elevation1Textfield.text isEqualToString:@""] || [elevation1Textfield.text doubleValue] == 0) {
        
        self.elevation1Textfield.text = nil;
        [self.elevation1Textfield setRequired:YES];
    } else {
        
        [self.elevation1Textfield setRequired:NO];
    }
    
    if (baselineLengthTextfield.text == nil || [baselineLengthTextfield.text isEqualToString:@""] || [baselineLengthTextfield.text doubleValue] == 0) {
        
        self.baselineLengthTextfield.text = nil;
        [self.baselineLengthTextfield setRequired:YES];
    } else {
        
        [self.baselineLengthTextfield setRequired:NO];
    }
        
}


@end