//
//  SettingSelectListViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 4/30/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingSelectListViewController : UITableViewController 
{
	NSDictionary	*listDict;
    NSString        *settingKey;

}

@property (nonatomic, retain) NSDictionary *listDict;
@property (nonatomic, retain) NSString *settingKey;

@end