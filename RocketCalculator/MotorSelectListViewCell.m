//
//  MotorSelectListViewCell.m
//  RocketCalculator
//
//  Created by Peter Stanley on 3/25/12.
//  Copyright 2012 Peter Stanley. All rights reserved.
//

#import "MotorSelectListViewCell.h"

@implementation MotorSelectListViewCell

@synthesize label, labelDetail, modelNumber, propellantType;

- (void)dealloc
{

    [propellantType release];
    [modelNumber release];
    [labelDetail release];
    [label release];
    
    [super dealloc];
}

@end
