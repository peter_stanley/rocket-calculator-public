//
//  DriftDistanceCalculatorViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "DriftDistanceCalculatorViewController.h"
#import "RAUnit.h"
#import "RocketCalculatorAppDelegate.h"
#import "Calculator.h"
#import "GenericFormulaViewController.h"
#import "CalcTextfield.h"

@implementation DriftDistanceCalculatorViewController

#define ALTITUDE_TEXTFIELD 1
#define DESCENT_RATE_TEXTFIELD 2
#define WIND_SPEED_TEXTFIELD 3

@synthesize altitudeTextField, altitudeUnitLabel, descentRateTextField, descentRateUnitLabel;
@synthesize windSpeedTextField, windSpeedUnitLabel, descentTimeLabel, descentTimeUnitLabel;
@synthesize driftDistanceLabel, driftDistanceUnitLabel, scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)registerForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unRegisterForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardDidShowNotification 
                                                  object:nil]; 
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                    name:UIKeyboardWillHideNotification 
                                                  object:nil];  
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    RocketCalculatorAppDelegate *appDelegate = (RocketCalculatorAppDelegate*)[[UIApplication sharedApplication] delegate];
        
    if ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait || [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown) {
        
        CGFloat tabBarHeight = appDelegate.tabBarController.tabBar.frame.size.height;
        
        NSDictionary* info = [aNotification userInfo];
        
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        
        self.scrollView.contentInset = contentInsets;
        
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        CGRect aRect = self.view.frame;
        
        aRect.size.height -= kbSize.height;

        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y-kbSize.height+tabBarHeight);
            
            [scrollView setContentOffset:scrollPoint animated:YES];
            
        }
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    altitude = 0;
    descentRate = 0;
    windSpeed = 0;
    descentDuration = 0;

    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];
    
    CGFloat frameHeight = self.view.frame.size.height;
    CGFloat frameWidth = self.view.frame.size.width;
    
    UIScrollView *tempScrollView=(UIScrollView *)self.view;
    tempScrollView.contentSize=CGSizeMake(frameWidth, frameHeight * 2);
    
    self.title = @"Drift Distance";
    
	UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(updateFields:)];
	gestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:gestureRecognizer];
	[gestureRecognizer release];
    
    altitudeUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_drift_altitude"] ofDimension:@"height"];
    
    descentRateUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_descent_velocity"] ofDimension:@"velocity"];
    
    windSpeedUnit =  [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_drift_wind_speed"] ofDimension:@"velocity"];
    
    descentTimeUnit = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_drift_descent_time"] ofDimension:@"time"];
    
    driftDistanceUnit = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] unitWithName:[[NSUserDefaults standardUserDefaults] stringForKey:@"calc_drift_distance"] ofDimension:@"length"];
  
    UIToolbar *tools = [[UIToolbar alloc]
                        initWithFrame:CGRectMake(0.0f, 0.0f, 75.0f, 44.01f)]; // 44.01 shifts it up 1px for some reason
    tools.clearsContextBeforeDrawing = NO;
    tools.clipsToBounds = NO;
    tools.tintColor = [UIColor colorWithWhite:0.305f alpha:0.0f]; // closest I could get by eye to black, translucent style.
    
    tools.barStyle = -1; // clear background
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    // Add actionsheet button
    UIButton *exportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exportButton.frame = CGRectMake(0, 0, 25, 20); //set frame for button
    
    UIImage *buttonImage = [UIImage imageNamed:@"action2.png"];
    
    [exportButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [exportButton addTarget:self action:@selector(exportButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:exportButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    self.navigationController.navigationBar.translucent = NO;
    // Create a spacer.
    UIBarButtonItem *bi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    bi.width = 8.0f;
    [buttons addObject:bi];
    [bi release];
    
    // Add info button
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    infoButton.frame = CGRectMake(0, 0, 20, 20); //set frame for button
    
    buttonImage = [UIImage imageNamed:@"info_g.png"];
    
    [infoButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    modalButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    [self.navigationItem setRightBarButtonItem:modalButton animated:YES];
    [buttons addObject:modalButton];
    [modalButton release];
    
    [tools setItems:buttons animated:NO];
    [buttons release];
    UIBarButtonItem *twoButtons = [[UIBarButtonItem alloc] initWithCustomView:tools];
    [tools release];
    self.navigationItem.rightBarButtonItem = twoButtons;
    
    [twoButtons release];
    
}

- (IBAction)infoButtonAction:(id)sender {
    GenericFormulaViewController *infoView = [[[GenericFormulaViewController alloc] init] autorelease];
    infoView.htmlFileName = @"driftDistance";
    [infoView setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    //[self presentModalViewController:infoView animated:YES];
    [self presentViewController:infoView animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.altitudeUnitLabel.text = [NSString stringWithFormat:@"(%@)", altitudeUnit.name];
    self.altitudeTextField.placeholder = [NSString stringWithFormat:@"%@", altitudeUnit.label];
    
    self.descentRateUnitLabel.text = [NSString stringWithFormat:@"(%@)", descentRateUnit.name];
    self.descentRateTextField.placeholder = [NSString stringWithFormat:@"%@", descentRateUnit.label];
    
    self.windSpeedUnitLabel.text = [NSString stringWithFormat:@"(%@)", windSpeedUnit.name];
    self.windSpeedTextField.placeholder = [NSString stringWithFormat:@"%@", windSpeedUnit.label];
    
    self.descentTimeUnitLabel.text = [NSString stringWithFormat:@"(%@)", descentTimeUnit.name];
    self.driftDistanceUnitLabel.text = [NSString stringWithFormat:@"(%@)", driftDistanceUnit.name];
    
    [self resetTextfieldDisplay];
}

- (void)willEnterForeground
{
    NSLog(@"will enter foreground seen");
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self resetTextfieldDisplay];
    [self.view setNeedsDisplay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unRegisterForKeyboardNotifications];
}
- (void)viewDidUnload
{

    self.scrollView = nil;
    self.driftDistanceUnitLabel = nil;
    self.driftDistanceLabel = nil;
    self.descentTimeUnitLabel = nil;
    self.descentTimeLabel = nil;
    self.windSpeedUnitLabel = nil;
    self.windSpeedTextField = nil;
    self.descentRateUnitLabel = nil;
    self.descentRateTextField = nil;
    self.altitudeUnitLabel = nil;
    self.altitudeTextField = nil;
    
    [super viewDidUnload];
}

- (void)dealloc
{

    [scrollView release];
    [driftDistanceUnitLabel release];
    [driftDistanceLabel release];
    [descentTimeUnitLabel release];
    [descentTimeLabel release];
    [windSpeedUnitLabel release];
    [windSpeedTextField release];
    [descentRateUnitLabel release];
    [descentRateTextField release];
    [altitudeUnitLabel release];
    [altitudeTextField release];
    
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
    if (isIpad) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    } else {
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }
    
    activeField = textField;
    
    if (textField.tag == ALTITUDE_TEXTFIELD) {
        
        altitudeTextField.text = nil;
        altitude = 0;
                
        [descentRateTextField setUserInteractionEnabled:NO];
        [windSpeedTextField setUserInteractionEnabled:NO];
        
    } else if (textField.tag == DESCENT_RATE_TEXTFIELD) {
        
        descentRateTextField.text = nil;
        descentRate = 0;
                
        [altitudeTextField setUserInteractionEnabled:NO];
        [windSpeedTextField setUserInteractionEnabled:NO];        
        
    } else if (textField.tag == WIND_SPEED_TEXTFIELD) {
        
        windSpeedTextField.text = nil;
        windSpeed = 0;
        
        [altitudeTextField setUserInteractionEnabled:NO];
        [descentRateTextField setUserInteractionEnabled:NO];
        
    }  

    [descentTimeLabel setAlpha:0.25];
    [driftDistanceLabel setAlpha:0.25];

    
    textField.text = nil;

}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    activeField = nil;
    
}
- (IBAction)updateFields:(id)sender
{

    [self resignAllResponders];
    [self enableTextfields];

    double baseAltitude = 0;
    double baseDescentRate = 0;
    double baseWindSpeed = 0;
    double calcTime = 0;
    double calcDriftDistance = 0;
    
    if (!altitude) {
        
        altitude = [altitudeTextField.text doubleValue];
        
    }
    
    if (!descentRate) {
        
        descentRate = [descentRateTextField.text doubleValue];
        
    }
    
    if (!windSpeed) {
        
        windSpeed = [windSpeedTextField.text doubleValue];
        
    }
    
    // get base units
    baseAltitude = [[altitudeUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:altitude]] doubleValue];
    baseDescentRate = [[descentRateUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:descentRate]] doubleValue];    
    baseWindSpeed = [[windSpeedUnit convertToBaseUnit:(NSDecimalNumber *)[NSNumber numberWithDouble:windSpeed]] doubleValue];        
        
    Calculator *calculator = [[Calculator alloc] init];
        
    calcTime = [calculator calcTimeFromAltitude:baseAltitude andDescentRate:baseDescentRate];
    
    calcDriftDistance = [calculator calcDriftDistanceFromTime:calcTime andWindSpeed:baseWindSpeed];

    descentTimeLabel.text = [NSString stringWithFormat:@"%.2f", [[descentTimeUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcTime] toUnit:descentTimeUnit]floatValue]];
    
    driftDistanceLabel.text = [NSString stringWithFormat:@"%.2f", [[driftDistanceUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcDriftDistance] toUnit:driftDistanceUnit]floatValue]];
    NSString *test = [NSString stringWithFormat:@"%.2f", [[driftDistanceUnit.baseUnit convertValue:(NSDecimalNumber *)[NSNumber numberWithDouble:calcDriftDistance] toUnit:driftDistanceUnit]floatValue]];
    
    NSLog(@"driftDistanceLabel.text = %@, calcDriftDistance = %f test = %@",driftDistanceLabel.text,calcDriftDistance, test);

    [calculator release];
    
    [descentTimeLabel setAlpha:1.0];
    [driftDistanceLabel setAlpha:1.0];
    
    [self resetTextfieldDisplay];
}

-(IBAction) showEmailModalView:(id)sender 
{
    
    if([MFMailComposeViewController canSendMail]){
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        
        if (picker) {
            
            picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
            NSString *mailSubject = [NSString stringWithFormat:@"%@ Calculation", self.title];
            NSString *mailMsg = [self exportMsgHtml];
    
            [picker setSubject:mailSubject];
    
            [picker setMessageBody:mailMsg isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
    
            picker.navigationBar.barStyle = UIBarStyleDefault; // choose your style, unfortunately, Translucent colors behave quirky.
    
            //[self presentModalViewController:picker animated:YES];
            [self presentViewController:picker animated:YES completion:nil];
            [picker release];
            
        }
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Mail Accounts" message:@"Please set up a Mail account in order to send email." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
        
        
    }

    
}

// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{ 
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-("
                                                           delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
        }
            
            break;
    }
   // [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)exportButtonAction:(id)sender
{
    if (![UIPrintInteractionController isPrintingAvailable]) {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Email Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", nil];
        
    } else {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Export Calculation"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"E-mail", @"Print", nil];
    }
    // Show the sheet
    [sheet showFromTabBar:self.tabBarController.tabBar];
    [sheet release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    //NSLog(@"Button %d", buttonIndex);
    switch (buttonIndex) {
        case 0:
            [self showEmailModalView:nil];
            break;
            
        case 1:
            [self printCalculation:nil];
            break;
            
        default:
            break;
    }
}

#pragma mark -

#pragma mark Printing



// Print button handler that sends our print instructions to the OS

- (IBAction)printCalculation:(id)sender {
    
    // Get a reference to the singleton iOS printing concierge
    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    
    if(pic && [UIPrintInteractionController isPrintingAvailable]) {
        //  UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = self.title;
        pic.printInfo = printInfo;
        
        // NSString *msg = @"dsfs fs ff sdf asf asf safs";
        NSString *msg = [self exportMsgText];
        // NSLog(@"msg = %@", msg);
        UISimpleTextPrintFormatter *textFormatter = [[UISimpleTextPrintFormatter alloc]
                                                     initWithText:msg];
        textFormatter.startPage = 0;
        textFormatter.contentInsets = UIEdgeInsetsMake(72.0, 72.0, 72.0, 72.0); // 1 inch margins
        textFormatter.maximumContentWidth = 6 * 72.0;
        pic.printFormatter = textFormatter;
        [textFormatter release];
        pic.showsPageRange = YES;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSLog(@"Printing could not complete because of error: %@", error);
            }
        };
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [pic presentFromRect:self.tabBarController.tabBar.frame inView:self.view animated:YES completionHandler:completionHandler];
        } else {
            [pic presentAnimated:YES completionHandler:completionHandler];
        }
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Printing Error" message:@"Your device doesn't support printing" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    
}

- (NSString *) exportMsgText
{    
    NSString *title = self.title;
    
    NSString *altitudeString = altitudeTextField.text;
    NSString *altitudeUnitString = altitudeUnit.name;
    
    NSString *descentRateString = descentRateTextField.text;
    NSString *descentRateUnitString = descentRateUnit.name;
    
    NSString *windSpeedString = windSpeedTextField.text;
    NSString *windSpeedUnitString = windSpeedUnit.name;
    
    NSString *descentTimeString = descentTimeLabel.text;
    NSString *descentTimeUnitString = descentTimeUnitLabel.text;
    
    NSString *driftDistanceString = driftDistanceLabel.text;
    NSString *driftDistanceUnitString = driftDistanceUnitLabel.text;
    
    NSString *msg = [NSString stringWithFormat:@"%@\n\nAltitude: %@ (%@)\nDescent Rate: %@ (%@)\nWind Speed: %@ (%@)\nDescent Time: %@ %@\nDrift Distance: %@ %@\n\n\nGenerated by Rocket Calculator\n\nAvailable on the Apple App Store",title,altitudeString, altitudeUnitString, descentRateString, descentRateUnitString, windSpeedString, windSpeedUnitString, descentTimeString, descentTimeUnitString, driftDistanceString, driftDistanceUnitString];
    
    
    return msg;

}

- (NSString *) exportMsgHtml
{
    NSString *title = self.title;
    
    NSString *altitudeString = altitudeTextField.text;
    NSString *altitudeUnitString = altitudeUnit.name;
    
    NSString *descentRateString = descentRateTextField.text;
    NSString *descentRateUnitString = descentRateUnit.name;
    
    NSString *windSpeedString = windSpeedTextField.text;
    NSString *windSpeedUnitString = windSpeedUnit.name;
    
    NSString *descentTimeString = descentTimeLabel.text;
    NSString *descentTimeUnitString = descentTimeUnitLabel.text;
    
    NSString *driftDistanceString = driftDistanceLabel.text;
    NSString *driftDistanceUnitString = driftDistanceUnitLabel.text;
    
    NSString *msg = [NSString stringWithFormat:@"%@<br /><br />Altitude: %@ (%@)<br />Descent Rate: %@ (%@)<br />Wind Speed: %@ (%@)<br />Descent Time: %@ %@<br />Drift Distance: %@ %@<br /><br /><br />Generated by Rocket Calculator<br /><br />Available on the Apple <a href=\"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=484673201&mt=8\">App Store</a>",title,altitudeString, altitudeUnitString, descentRateString, descentRateUnitString, windSpeedString, windSpeedUnitString, descentTimeString, descentTimeUnitString, driftDistanceString, driftDistanceUnitString];
    
    
    return msg;

}

- (void)enableTextfields
{
    
    [altitudeTextField setUserInteractionEnabled:YES];
    [descentRateTextField setUserInteractionEnabled:YES];
    [windSpeedTextField setUserInteractionEnabled:YES];
    
}

- (void) resignAllResponders
{
    
    [altitudeTextField resignFirstResponder];
    [descentRateTextField resignFirstResponder];
    [windSpeedTextField resignFirstResponder];
    
}

- (void) resetTextfieldDisplay
{
    
    // Tint required fields
    
    if (altitudeTextField.text == nil || [altitudeTextField.text isEqualToString:@""] || [altitudeTextField.text doubleValue] == 0) {
        
        self.altitudeTextField.text = nil;
        [self.altitudeTextField setRequired:YES];
    } else {
        
        [self.altitudeTextField setRequired:NO];
    }
    
    if (descentRateTextField.text == nil || [descentRateTextField.text isEqualToString:@""] || [descentRateTextField.text doubleValue] == 0) {
        
        self.descentRateTextField.text = nil;
        [self.descentRateTextField setRequired:YES];
    } else {
        
        [self.descentRateTextField setRequired:NO];
    }
    
    if (windSpeedTextField.text == nil || [windSpeedTextField.text isEqualToString:@""] || [windSpeedTextField.text doubleValue] == 0) {
        
        self.windSpeedTextField.text = nil;
        [self.windSpeedTextField setRequired:YES];
    } else {
        
        [self.windSpeedTextField setRequired:NO];
    }
    
}

@end
