//
//  InformationListViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 11/25/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "InformationListViewController.h"
#import "QuickReferenceRootViewController.h"
#import "GenericInfoViewController.h"

@implementation InformationListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    NSLog(@"initWithStyle called");
    self = [super initWithStyle:style];
    if (self) {

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
        // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];

    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    switch (indexPath.row) {
        case 0:
            
            cell.textLabel.text = @"Quick Reference";
            break;
            
        case 1:
            
            cell.textLabel.text = @"Sources of Research";
            break;
            
        case 2:
            
            cell.textLabel.text = @"Support / Feedback";
            break;
 
        case 3:
            
            cell.textLabel.text = @"About";
            break;

        default:
            break;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    static GenericInfoViewController *infoView = nil;
    static UIViewController *nextController = nil;
    
    switch (indexPath.row) 
    {
        case 0:
        {
            nextController = [[QuickReferenceRootViewController alloc] initWithStyle:UITableViewStyleGrouped];
            
        }
            
            break;
            
        case 1:
        {
            infoView = [[GenericInfoViewController alloc] init];
            infoView.htmlFileName = @"bibliography";
            
        }
            
            break;
            
        case 2:
        {
            infoView = [[GenericInfoViewController alloc] init];
            infoView.htmlFileName = @"contact";
            
        }
            break;
            
        case 3:
        {
            infoView = [[GenericInfoViewController alloc] init];
            infoView.htmlFileName = @"about";
            
        }

            break;
            
            
        default:
            
            break;
            
    }
    
    if (indexPath.row == 0) {
        [self.navigationController pushViewController:nextController animated:YES];
        [nextController release];
    } else {
        
        [self.navigationController pushViewController:infoView animated:YES];
        [infoView release];
    }
    
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

}

@end
