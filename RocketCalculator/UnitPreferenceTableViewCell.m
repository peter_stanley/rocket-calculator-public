//
//  UnitPreferenceTableViewCell.m
//  RocketCalculator
//
//  Created by Peter Stanley on 4/6/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import "UnitPreferenceTableViewCell.h"

@implementation UnitPreferenceTableViewCell

@synthesize unitLabel, preferenceLabel;

- (void)dealloc
{
    [preferenceLabel release];
    [unitLabel release];
    
    [super dealloc];
}

@end
