//
//  StaticPortSizeCalculatorViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

@class RAUnit;
@class CalcTextfield;

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface StaticPortSizeCalculatorViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIPrintInteractionControllerDelegate> 
{
    bool isIpad;
    
    double diameter;
    double length;
    double portSize;
    int numPorts;
    double scaledPosition;
    double scaledXPoint;
    
    CalcTextfield *diameterTextField;
    UILabel *diameterUnitLabel;
    CalcTextfield *lengthTextField;
    UILabel *lengthUnitLabel;
    CalcTextfield *numPortsTextField;
    UILabel *portSizeLabel;
    UILabel *portSizeUnitLabel;
    
    RAUnit *diameterUnit;
    RAUnit *lengthUnit;
    RAUnit *portSizeUnit;
    
    UIScrollView *scrollView;
    UIScrollView *rulerScrollView;
    UIImageView *rulerImageView;
    UIImage *rulerImage;
    UITextField *activeField;
    UIActionSheet *sheet;

}

@property (nonatomic, retain) IBOutlet CalcTextfield *diameterTextField;
@property (nonatomic, retain) IBOutlet UILabel *diameterUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *lengthTextField;
@property (nonatomic, retain) IBOutlet UILabel *lengthUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *numPortsTextField;
@property (nonatomic, retain) IBOutlet UILabel *portSizeLabel;
@property (nonatomic, retain) IBOutlet UILabel *portSizeUnitLabel;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIScrollView *rulerScrollView;
@property (nonatomic, retain) IBOutlet UIImageView *rulerImageView;
@property (nonatomic, retain) UIImage *rulerImage;

- (IBAction)updateFields:(id)sender;
- (UIImage *)imageByDrawingLineOnImage:(UIImage *)image;
- (IBAction)infoButtonAction:(id)sender;
- (IBAction) exportButtonAction:(id)sender;
- (IBAction) showEmailModalView:(id)sender;
- (IBAction) printCalculation:(id)sender;
- (NSString *) exportMsgText;
- (NSString *) exportMsgHtml;
- (void) enableTextfields;
- (void) resignAllResponders;
- (void) resetTextfieldDisplay;

@end