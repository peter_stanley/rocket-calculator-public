//
//  MotorSelectListViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 3/23/12.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import "MotorSelectListViewController.h"
#import "RocketCalculatorAppDelegate.h"
#import "Calculator.h"
#import "MotorSelectListViewCell.h"
#import "Motor.h"

@implementation MotorSelectListViewController

@synthesize delegate, motorsArray, allSUMotorsArray, allReloadsMotorsArray, currentMotor, selectedMotor, selectedRow, selectedSection, currentMotorType, doneButton, motorTypeControl, motorSelectListTableView, motorSelectListViewCell;

#pragma mark -

- (id)init {
	if ((self = [super initWithNibName:@"MotorSelectListViewController" bundle:nil])) {
		
		self.title = @"Motors";
        

	}
	
	return self;
	
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    return [self init];
    
}

- (void)viewDidLoad {

    [super viewDidLoad];
    
    if (!currentMotorType) {
        
        currentMotorType = 0;
        
    }
    
    self.motorTypeControl.selectedSegmentIndex = currentMotorType;
    
    self.motorsArray = [NSMutableArray arrayWithCapacity:1];
    
    [self loadMotorsArray:(int)currentMotorType];

    selectedSection = [self.motorsArray count];

    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];
        
}

- (void)viewDidUnload {
    
    self.motorSelectListViewCell = nil;
    self.motorSelectListTableView = nil;
    self.motorTypeControl = nil;
    self.doneButton = nil;
    self.allReloadsMotorsArray = nil;
    self.allSUMotorsArray = nil;
    self.motorsArray = nil;
    
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated 
{
    
    self.motorTypeControl.selectedSegmentIndex = currentMotorType;
    
    [self.motorSelectListTableView reloadData];
        
    [super viewWillAppear:animated];
    
}

- (void)dealloc 
{
    
    [motorSelectListViewCell release];
    [motorSelectListTableView release];
    [motorTypeControl release];
    [doneButton release];
    [allReloadsMotorsArray release];
    [allSUMotorsArray release];
    [motorsArray release];
    
    [super dealloc];
}

#pragma mark -
#pragma mark Tableview methods

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView 
{

    return [[UILocalizedIndexedCollation currentCollation] sectionTitles];

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if ([[self.motorsArray objectAtIndex:section] count] > 0) {

        return [[[UILocalizedIndexedCollation currentCollation] sectionTitles] objectAtIndex:section];
        
    }
    
    return nil;    
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{

    return [[UILocalizedIndexedCollation currentCollation] sectionForSectionIndexTitleAtIndex:index];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self.motorsArray count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    NSInteger numberOfRows = 0;
    numberOfRows = [[self.motorsArray objectAtIndex:section] count];
    //numberOfRows = [self.motorsArray count];

    return numberOfRows;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 59;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MotorSelectListViewCellIdentifier = @"MotorSelectViewCell";
    
    MotorSelectListViewCell *cell = (MotorSelectListViewCell *)[tableView dequeueReusableCellWithIdentifier:MotorSelectListViewCellIdentifier];
    if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"MotorSelectListViewCell" owner:self options:nil];
        cell = motorSelectListViewCell;
        self.motorSelectListViewCell = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
    
}

- (void)configureCell:(MotorSelectListViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
            
    // Configure the cell

    Motor *motor = [[self.motorsArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    cell.label.text = motor.code;
    cell.labelDetail.text = motor.mfg;
    cell.modelNumber.text = motor.modelNumber;
    //cell.propellantType.text = motor.propellantType;
    cell.propellantType.text = [NSMutableString stringWithFormat:@"%@ %dmm", motor.propellantType, motor.dia];
    
    
    if (selectedSection < [self.motorsArray count]) {
        if (indexPath.row == selectedRow && indexPath.section == selectedSection) {

            [cell setAccessoryType:UITableViewCellAccessoryCheckmark]; 
        
        } else {
        
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (selectedSection < [self.motorsArray count]) {
        
        [[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:selectedSection]] setAccessoryType:UITableViewCellAccessoryNone];

    }
    
    selectedRow = indexPath.row;
    selectedSection = indexPath.section;
        
    [[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:selectedSection]] setAccessoryType:UITableViewCellAccessoryCheckmark];
        
    Motor *motor = [[self.motorsArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    self.currentMotor = motor;

    selectedMotor = currentMotor;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dismissViewController:(id)sender
{

    if (!selectedMotor) {
        
        currentMotor = nil;
        
    }
    
    [delegate motorSelectListViewControllerWasDismissed:self];

   // [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)loadMotorsArray:(int)motorType
{
    
    Calculator *calc = [[Calculator alloc] init];
    
    int mType = motorType;
    
    NSString *mTypeString = nil;
    
    switch (mType) {
        case 0: // all reloads
            mTypeString = @"AllReloads_JSON";
            break;
            
        case 1: // All SU -- includes aerotech, estes and quest
            mTypeString = @"AllSU_JSON";
            break;
            
        default:
            mTypeString = @"AllReloads_JSON";
            break;
    }
    
    NSError *error = nil;
    
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:mTypeString
                                                         ofType:@"txt"];
    
    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
    
    id json = [NSJSONSerialization JSONObjectWithData:data
                                              options:kNilOptions
                                                error:&error];
    
    UILocalizedIndexedCollation *theCollation = [UILocalizedIndexedCollation currentCollation];
    
        
    if (self.motorsArray) {
        [self.motorsArray removeAllObjects];
    }

    NSMutableArray *tmpArray = [NSMutableArray array];
    for (NSDictionary *motorDict in json) {
        Motor *tmpMotor = [[Motor alloc] init];
        
        tmpMotor.Itot = [[motorDict valueForKey:@"total_impulse_Ns"] doubleValue];
        
        NSString *impulseClass = [calc calcMotorClassLetterOnlyFromTotalImpulse:tmpMotor.Itot];
        tmpMotor.impClass = impulseClass;
        tmpMotor.mfg = [motorDict valueForKey:@"manufacturer"];
        tmpMotor.code = [motorDict valueForKey:@"motor_type"];
        tmpMotor.modelNumber = [motorDict valueForKey:@"model_number"];
        tmpMotor.propellantType = [motorDict valueForKey:@"propellant_type"];
        
        
        tmpMotor.avgThrust = [[motorDict valueForKey:@"average_thrust_N"] doubleValue];
        tmpMotor.burn_time = [[motorDict valueForKey:@"burn_time_s"] doubleValue];
        tmpMotor.initWt = [[motorDict valueForKey:@"total_weight_g"] doubleValue];
        tmpMotor.propWt = [[motorDict valueForKey:@"propellant_weight_g"] doubleValue];
        tmpMotor.dia = [[motorDict valueForKey:@"diameter_mm"] intValue];

        [tmpArray addObject:tmpMotor];
        
        [tmpMotor release];
        
    }
    
    for (Motor *theMotor in tmpArray) {
        
        NSInteger sect = [theCollation sectionForObject:theMotor collationStringSelector:@selector(impClass)];
        
        theMotor.sectionNumber = sect;
        
    }
        
    NSInteger highSection = [[theCollation sectionTitles] count];
    
    NSMutableArray *sectionArrays = [NSMutableArray arrayWithCapacity:highSection];
    
    for (int i=0; i<=highSection; i++) {
        
        NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
        
        [sectionArrays addObject:sectionArray];
        
    }
        
    for (Motor *theMotor in tmpArray) {
        
        [(NSMutableArray *)[sectionArrays objectAtIndex:theMotor.sectionNumber] addObject:theMotor];
        
    }
        
    for (NSMutableArray *sectionArray in sectionArrays) {
        
        NSArray *sortedSection = [theCollation sortedArrayFromArray:sectionArray
                                  
                                            collationStringSelector:@selector(code)];
        
        [self.motorsArray addObject:sortedSection];
        
    }
    
    [calc release];
    
}

- (IBAction)changeMotorType:(id)sender
{
    
    currentMotorType = (int)self.motorTypeControl.selectedSegmentIndex;
    
    [self loadMotorsArray:(int)currentMotorType];
    
    [self.motorSelectListTableView reloadData];
    
}

@end