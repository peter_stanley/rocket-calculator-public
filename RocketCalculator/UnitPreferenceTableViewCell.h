//
//  UnitPreferenceTableViewCell.h
//  RocketCalculator
//
//  Created by Peter Stanley on 4/6/11.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

@interface UnitPreferenceTableViewCell : UITableViewCell {
    
    UILabel *unitLabel;
    UILabel *preferenceLabel;
}

@property (nonatomic, retain) IBOutlet UILabel *unitLabel;
@property (nonatomic, retain) IBOutlet UILabel *preferenceLabel;

@end
