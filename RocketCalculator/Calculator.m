//
//  Calculator.m
//  RocketCalculator
//
//  Created by Peter Stanley on 10/27/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator

// Density of air = 1.22 kg/m3
// Gravity acceleration = 9.8 m/s2
// Descent velocity default 3 m/s
//#define NEWTON_PER_GRAM .00980665001
//#define NEWTON_PER_KILOGRAM 1
//#define NEWTON_PER_KILOGRAM 9.81

// Motor Classes
#define A 2.5
#define B 5
#define C 10
#define D 20
#define E 40
#define F 80
#define G 160
#define H 320
#define I 640
#define J 1280
#define K 2560
#define L 5120
#define M 10240
#define N 20480
#define O 40960
#define P 81920
#define Q 163840

#define NEWTON_PER_KILOGRAM 9.80665001
#define GRAVITY_ACCEL_g 9.80665
#define DENSITY_AIR_r 1.225
#define VELOCITY_THRU_AIR_v 3
#define R 22.16 // Combustion gas constant (ft- lbf/lbm R) for FFFF BP
#define Rda 287.05 // specific gas constant for dry air J/Kg.K
#define T 3307 // Combustion gas temperature, degrees R for BP
#define SEALEVEL_STD_ATM_PRESSURE 101.325 //kPa
#define SEALEVLE_STD_TEMPERATURE 288.15 //K
#define TEMPERATURE_LAPSE_RATE 0.0065 //K/m

#define IDEAL_GAS_CONSTANT 8.31447 // J/(mol·K)
#define MOLAR_MAS_DRY_AIR 0.0289644 // kg/mol

#define SHAPE_CIRCLE 0
#define SHAPE_SQUARE 1
#define SHAPE_HEXAGON 2
#define SHAPE_OCTOGON 3

#define MIN_256_BREAK 31
#define MAX_256_BREAK 46

#define MIN_440_BREAK 50
#define MAX_440_BREAK 76

#define MIN_632_BREAK 75
#define MAX_632_BREAK 114

// Altitude Prediction

- (NSString *)calcMotorClassFromTotalImpulse:(double)i
{    

    double p = 0;
    NSString *c = @"";
    
    if (i <= A) {
        
        p = i / A;
        c = @"A";
        
    } else if (i > A && i <= B) {
        
        p = i / B;
        c = @"B";
        
    } else if (i > B && i <= C) {
        
        p = i / C;
        c = @"C";
        
    } else if (i > C && i <= D) {
        
        p = i / D;
        c = @"D";
        
    } else if (i > D && i <= E) {
        
        p = i / E;
        c = @"E";
        
    } else if (i > E && i <= F) {
        
        p = i / F;
        c = @"F";
        
    } else if (i > F && i <= G) {
        
        p = i / G;
        c = @"G";
        
    } else if (i > G && i <= H) {
        
        p = i / H;
        c = @"H";
        
    } else if (i > H && i <= I) {
        
        p = i / I;
        c = @"I";
        
    } else if (i > I && i <= J) {
        
        p = i / J;
        c = @"J";
        
    }  else if (i > J && i <= K) {
        
        p = i / K;
        c = @"K";
        
    } else if (i > K && i <= L) {
        
        p = i / L;
        c = @"L";
        
    } else if (i > L && i <= M) {
        
        p = i / M;
        c = @"M";
        
    } else if (i > M && i <= N) {
        
        p = i / N;
        c = @"N";
        
    } else if (i > N && i <= O) {
        
        p = i / O;
        c = @"O";
        
    } else if (i > O && i <= P) {
        
        p = i / P;
        c = @"P";
        
    }else if (i > P && i <= Q) {
        
        p = i / Q;
        c = @"Q";
        
    }else if (i > Q) {
        
        p = 0;
        c = @">Q";
        
    }
    
    p = p * 100;
    
    NSString *mc = [NSString stringWithFormat:@"%.1f%% %@",p,c];
    
    return mc;
    
}

- (NSString *)calcMotorClassLetterOnlyFromTotalImpulse:(double)i
{
    
    NSString *c = @"";
    
    if (i <= A) {
        
        c = @"A";
        
    } else if (i > A && i <= B) {
        
        c = @"B";
        
    } else if (i > B && i <= C) {
        
        c = @"C";
        
    } else if (i > C && i <= D) {
        
        c = @"D";
        
    } else if (i > D && i <= E) {
        
        c = @"E";
        
    } else if (i > E && i <= F) {
        
        c = @"F";
        
    } else if (i > F && i <= G) {
        
        c = @"G";
        
    } else if (i > G && i <= H) {
        
        c = @"H";
        
    } else if (i > H && i <= I) {
        
        c = @"I";
        
    } else if (i > I && i <= J) {
        
        c = @"J";
        
    }  else if (i > J && i <= K) {
        
        c = @"K";
        
    } else if (i > K && i <= L) {
        
        c = @"L";
        
    } else if (i > L && i <= M) {
        
        c = @"M";
        
    } else if (i > M && i <= N) {
        
        c = @"N";
        
    } else if (i > N && i <= O) {
        
        c = @"O";
        
    } else if (i > O && i <= P) {
        
        c = @"P";
        
    }else if (i > P && i <= Q) {
        
        c = @"Q";
        
    }else if (i > Q) {
        
        c = @">Q";
        
    }
        
    return c;
    
}

- (double) calcDiameterFromArea:(double)area
{
    double r = sqrt(area / M_PI);
    double d = 2 * r;
    
    return d;
}

// returns kilograms
- (double)calcMotorCaseMassFromPropellantMass:(double)p andTotalMotorMass:(double)m
{
    
    double c = m - p;
    
    return  c;
}

// returns kilograms
- (double)calcPropellantMassFromMotorCaseMass:(double)c andTotalMotorMass:(double)m
{
    
    double p = m - c;
    
    return p;
}

// returns kilograms
- (double)calcTotalMotorMassFromMotorCaseMass:(double)c andPropellantMass:(double)p
{
    
    double t = c + p;
    
    return t;
    
}

// returns Newtons
- (double)calcAvgThrustFromTotalImpulse:(double)t andBurnTime:(double)b
{
    
    double a = t / b;
    
    return a;
}

// thrust in Newtons, returns seconds
- (double)calcBurnTimeFromTotalImpulse:(double)t andAvgThrust:(double)a
{
    
    double b = t / a;
    
    return b;
}

// time in seconds, returns Newton-seconds
- (double) calcTotalImpulseFromAvgThrust:(double)a andBurnTime:(double)b
{
    
    double t = a * b;
    
    return t;
    
}

// gets and returns kilograms
- (double) calcAverageThrustingMassFromRocketMass:(double)rm andMotorCaseMass:(double)cm andPropellantMass:(double)pm
{
    double avgThrustingMass = rm + cm + (pm / 2);
    
    return avgThrustingMass;
}

- (double) calcAverageThrustingMassFromRocketMass:(double)rm andTotalMotorMass:(double)tmm andPropellantMass:(double)pm
{
    double avgThrustingMass = rm + (tmm - pm) + (pm / 2);
    //double avgThrustingMass = rm + cm + (pm / 2);
    
    return avgThrustingMass;
}

// gets and returns kilograms
- (double) calcBurnoutMassFromRocketMass:(double)rm andMotorCaseMass:(double)cm
{
    double burnoutMass = rm + cm;
    
    return burnoutMass;
}

// area in square meters
- (double) calcKFromCd:(double)c andFromArea:(double)a andAirDensity:(double)ad
{
    // convert area from cm2 to m2
    a = a * 0.0001;

    double k = .5 * ad * c * a;

    //NSLog(@"k = %f", k);
    return k;
}


// mass in kilograms, thrust in Newtons, time in seconds returns m/s
- (double) calcBurnoutVelocityFromAvgThrustingMass:(double)ms andK:(double)k andAvgThrust:(double)at andBurnTime:(double)bt
{
    double q;

    if (k) {
        
        q = sqrt((at - ms * GRAVITY_ACCEL_g) / k);
        
    } else {
        
        q = sqrt(at - ms);
        
    }
    
    double x;
    
    if (k) {
        
        x = 2 * k * q / ms;
        
    } else {
        
        x = 2 * q / ms;
        
    }
     

    double burnoutVelocity = q * (1 - exp(-x * bt)) / (1 + exp(-x * bt)); 
    
    return burnoutVelocity;
    
}

- (double)calcBurnoutVelocityZeroDragFromAvgThrustingMass:(double)ms andAvgThrust:(double)at andBurnoutAltitude:(double)alt
{
    
    double burnoutVelocity;
    
    burnoutVelocity = sqrt(((2 * alt) / ms) * (at - (ms * GRAVITY_ACCEL_g)));
    
    return burnoutVelocity;
    
}

- (double)calcBurnoutAltitudeFromAvgThrustingMass:(double)ms andK:(double)k andAvgThrust:(double)at andBurnoutVelocity:(double)v
{//=(-ms/2/k)*LN((F-ms*9.8-k*v^2)/(F-ms*9.8))
    
    double burnoutAltitude;
            
    burnoutAltitude = (-ms/2/k)*log((at-ms*GRAVITY_ACCEL_g-k*pow(v, 2))/(at-ms*GRAVITY_ACCEL_g));
        
    return burnoutAltitude;
    
}


// mass in kilograms, returns meters
- (double) calcBurnoutAltitudeFromAvgThrustingMass:(double)ms andK:(double)k andAvgThrust:(double)at andBurnTime:(double)bt
{

    double burnoutAltitude;
    
    burnoutAltitude = (ms / k) * log(cosh(((bt / ms) * sqrt((k * (at - (ms * GRAVITY_ACCEL_g)))))));
        
    return burnoutAltitude;
    
}

- (double) calcBurnoutAltitudeZeroDragFromAvgThrustingMass:(double)ms andAvgThrust:(double)at andBurnTime:(double)bt
{
    
    double burnoutAltitude;
    
    burnoutAltitude = .5 * ((at / ms) - GRAVITY_ACCEL_g) * pow(bt, 2);
    
    return burnoutAltitude;
    
}

// mass in kilograms, velocity in m/s. returns meters
- (double) calcCoastAltitudeFromBurnoutMass:(double)bm andK:(double)k andBurnoutVelocity:(double)bv
{
//=(mf/2/k)*LN((mf*9.8+k*v^2)/(mf*9.8))
    double coastAltitude;
    
        coastAltitude = (bm/2/k)*log((bm*GRAVITY_ACCEL_g+k*pow(bv, 2))/(bm*GRAVITY_ACCEL_g));
        
    //double coastAltitude = (bm / (2 * k)) * log(((k * pow(bv, 2))/(bm * GRAVITY_ACCEL_g)) + 1);
    
    return coastAltitude;
}

// takes and returns meters
- (double) calcPeakAltitudeFromBurnoutAltitude:(double)ba andCoastAltitude:(double)ca
{
    
    double peakAltitude = ba + ca;
    
    return peakAltitude;
    
}

- (double) calcPeakAltitudeZeroDragFromBurnoutAltitude:(double)ba andAvgThrustingMass:(double)ms andAvgThrust:(double)at
{
    
    double peakAltitude;
    
    peakAltitude = (at * ba) / (ms * GRAVITY_ACCEL_g);
    
    return peakAltitude;
    
}
// mass in kilograms, returns secons
- (double) calcCoastTimeFromBurnoutMass:(double)bm andBurnoutVelocity:(double)bv andK:(double)k
{

    double qa,qb;
    
        qa = sqrt(bm * GRAVITY_ACCEL_g / k);
        qb = sqrt(GRAVITY_ACCEL_g * k / bm);
        
    double coastTime = atan(bv / qa)/ qb;

    //double coastTime = (sqrt((burnoutMass/(GRAVITY_ACCEL_g * k)))) * atan(bv * sqrt((k / (GRAVITY_ACCEL_g * burnoutMass))));
   // double coastTime =  sqrt(bm / (k * GRAVITY_ACCEL_g)) * atan(bv / sqrt(bm * GRAVITY_ACCEL_g / k));
    
    return coastTime;
}

- (double) calcCoastTimeZeroDragFromBurnoutAltitude:(double)ba andPeakAltitude:(double)pa
{
    
    double coastTime;
    
    coastTime = sqrt((2 / GRAVITY_ACCEL_g) * (pa - ba));
    
    return coastTime;
    
}

- (double) calcAirDensityFromAltitudeInMeters:(double)h andTemperatureInKelvin:(double)T1
{
    
    double p;
    double p0 = SEALEVEL_STD_ATM_PRESSURE;
    double T0 = SEALEVLE_STD_TEMPERATURE;
    double g = GRAVITY_ACCEL_g;
    double L1 = TEMPERATURE_LAPSE_RATE;
    double R1 = IDEAL_GAS_CONSTANT;
    double M1 = MOLAR_MAS_DRY_AIR;
    double airDensity;
    
    p = p0 * pow(((1 - ((L1 * h) / T0))), ((g * M1) / (R1 * L1)));

    // convert p from kilopascals to pascals
    
    p = p * 1000;
    
    airDensity = (p / (Rda * T1));
    
    return airDensity;
    
}
// Thrust to weight calculations, mass is in kilograms

- (double) calcMinThrustForMass: (double)mass andRatio:(int)ratio
{
    
    double minThrust = mass * NEWTON_PER_KILOGRAM *ratio;
    
    return minThrust;
}

- (double) calcMaxMassForThrust:(double)newtons andRatio:(int)ratio
{

    double maxMass = (newtons / ratio) / NEWTON_PER_KILOGRAM;
    
    return  maxMass;
}

- (double) calcThrustToWeightRatioFromThrust:(double)t andWeight:(double)w
{
    
    double r = t / (w * NEWTON_PER_KILOGRAM);
    
    return r;
    
}


// Parachute size calculations

// All parachute calculators expect mass in kilograms, velocity in meters per second, length in meters.

- (double) calcChuteDiameterFromMass:(double)mass andVelocity:(double)velocity andCd:(double)Cd andShape:(int)shape andAirDensity:(double)ad
{

    double m = mass;
    double v = velocity;
    double cd = Cd;
    double chuteDiameter = 0;
    double area = [self calcAreaFromMass:m andVelocity:v andCd:cd andAirDensity:ad];
    
    // if round the following would always work.
    // double chuteDiameter = sqrt((8 * m * GRAVITY_ACCEL_g) / (M_PI * DENSITY_AIR_r * cd * pow(v,2)));
    
    switch (shape) {
        case SHAPE_CIRCLE:

            chuteDiameter = sqrt((4 * area) / M_PI);
            
            break;
            
        case SHAPE_SQUARE:
            
            chuteDiameter = sqrt(area);
            
            break;
            
        case SHAPE_HEXAGON:
            
            chuteDiameter = sqrt(area * (1 / .866));
            
            break;
            
        case SHAPE_OCTOGON:
            
            chuteDiameter = sqrt(area * (1 / .828));
            
            break;
            
        default:
            break;
    }
    
    return chuteDiameter;
}

- (double) calcChuteAreaFromDiameter:(double)diameter andShape:(int)shape
{
    double d = diameter;
    int s = shape;
    double calcArea = 0;
    
    switch (s) {
        case SHAPE_CIRCLE:
            
            calcArea = .25 * M_PI * pow(d, 2);
            
            break;
            
        case SHAPE_SQUARE:
            
            calcArea = pow(d, 2);
            
            break;
            
        case SHAPE_HEXAGON:
            
            calcArea = .866 * pow(d, 2);
            
            break;
            
        case SHAPE_OCTOGON:
            
            calcArea = .828 * pow(d, 2);
            
            break;
            
        default:
            break;
    }


    return calcArea;
}

// assumes a round spill hole
- (double) calcSpillHoleAreaFromDiameter:(double)diameter
{

    double d = diameter;
    double calcArea = 0;
    
    calcArea = .25 * M_PI * pow(d, 2);

    return calcArea;
}

// Note for round chutes only!
- (double) calcChuteDescentVelocityFromMass:(double)mass andDiameter:(double)diameter andCd:(double)Cd andAirDensity:(double)ad
{

    double m = mass;
    double d = diameter;
    double cd = Cd;
    
    double descentVelocity = sqrt((8 * m * GRAVITY_ACCEL_g) / (M_PI * ad * cd * pow(d, 2)));
    
    return descentVelocity;
}


// Generic formulas
- (double) calcAreaFromMass:(double)mass andVelocity:(double)velocity andCd:(double)Cd andAirDensity:(double)ad
{    
    double m = mass;
    double v = velocity;
    double cd = Cd;
    
    double area = (2 * m * GRAVITY_ACCEL_g) / (ad * cd * pow(v, 2));
    
    return area;
}

- (double) calcDescentVelocityFromMass:(double)mass andArea:(double)area andCd:(double)Cd andAirDensity:(double)ad
{
    
    double m = mass;
    double a = area;
    double cd = Cd;
    
    double v = sqrt((2 * m * GRAVITY_ACCEL_g) / (ad * cd * a));
    
    return v;
}

// Drift Distance Calculations
// altitude is in meters and descent rate is meters/second. Time is in seconds

- (double)calcTimeFromAltitude:(double)a andDescentRate:(double)dr
{
    double t = 0;
    
    t = a / dr;
    
    return t;
}

- (double)calcDriftDistanceFromTime:(double)t andWindSpeed:(double)ws
{
    double dd = 0;
    dd = t * ws;

    return  dd;
}

// Streamer calculations
// mass in kilograms, velocity in m/s, length in meters

- (double) calcStreamerWidthFromArea:(double)area
{
    
    double a = area;
    double w = sqrt(a / 10);
    
    return w;
}

- (double) calcStreamerLengthFromWidth:(double)width andArea:(double)area
{
    
    double w = width;
    double a = area;
    double l = a / w;
    
    return l;
}

- (double) calcStreamerDescentVelocityFromMass:(double)mass andArea:(double)area andCd:(double)Cd andAirDensity:(double)ad
{
    
    double m = mass;
    double a = area;
    double cd = Cd;
    
    double v = sqrt((2 * m * GRAVITY_ACCEL_g) / (ad * cd * a));
    
    return v;
}

- (double) calcStreamerAreaFromMass:(double)mass // Tim Van Milligan method
{
    
    double m = mass;
    double a = .85 * m;
    return a;
}

// BP Calculations length/width in meters, pressure in psi, volume in cubic inches, mass in kilograms
// calculator converts meters to inches for calculation, convert mass to grams

// returns cubic inches
- (double) calcVolumeFromWidth:(double)width andLength:(double)length
{
    double inchesWidth = width * 39.3700787;
    double inchesLength = length *  39.3700787;
    
    double v = inchesLength * M_PI * pow(inchesWidth, 2) / 4;

    return v;
}

// returns kilograms
- (double) calcBPFromVolume:(double)volume andPressure:(double)pressure
{
    
    double v = volume;
    double p = pressure;
    
    double kg = ((p * (v * 453.59237)) / ((12 * R) * T)) * 0.001;

    return kg;
}

// returns psi
- (double) calcPressureFromMass:(double)mass andVolume:(double)volume
{
    double m = mass * 2.20462262;
    double v = volume;
    double p =( m * ((12 * R) * T)) / v;

    return p;
}

- (double) calcAreaOfCircle:(double)width
{
    double w = width;
    
    double a = M_PI * pow((w/2), 2);

    return a;
}

// **** Area is square inches and pressure is psi
- (double) calcForceFromArea:(double)a andPressure:(double)p
{
    double force = a * p;

    return force;
}

// Assumes forces is lbf and area is square inches. Returns pressure in psi
- (double) calcPsiFromForce:(double)f andArea:(double)a
{
    double pressure = f / a;
    
    return pressure;
}

//The following two methods assume force is in pounds force

- (int) calcMin256ScrewsFromForce:(double)f
{
    int num256 = f / MAX_256_BREAK;
    
    return num256;
}

- (int) calcMax256ScrewsFromForce:(double)f
{
    int num256 = f / MIN_256_BREAK;

    return num256;
}

- (int) calcAvg256ScrewsFromForce:(double)f
{
    //int num256 = f / ((MIN_256_BREAK + MAX_256_BREAK) / 2);
    
    float num256;
    int num256Rounded;
    
    num256 = f / ((MIN_256_BREAK + MAX_256_BREAK) / 2);
    num256Rounded = (int)roundf(num256);

    return  num256Rounded;
    
}

- (int) calcMin440ScrewsFromForce:(double)f
{
    int num440 = f / MAX_440_BREAK;

    return num440;
}

- (int) calcMax440ScrewsFromForce:(double)f
{
    int num440 = f / MIN_440_BREAK;

    return num440;
}

- (int) calcAvg440ScrewsFromForce:(double)f
{
    //int num440 = f / ((MIN_440_BREAK + MAX_440_BREAK) / 2);
    
    
    float num440;
    int num440Rounded;
    
    num440 = f / ((MIN_440_BREAK + MAX_440_BREAK) / 2);
    num440Rounded = (int)roundf(num440);
    
    return  num440Rounded;
    
}

- (int) calcMin632ScrewsFromForce:(double)f
{
    int num632 = f / MAX_632_BREAK;
    
    return num632;
}

- (int) calcMax632ScrewsFromForce:(double)f
{
    int num632 = f / MIN_632_BREAK;
    
    return num632;
}

- (int) calcAvg632ScrewsFromForce:(double)f
{
   // int num632 = f / ((MIN_632_BREAK + MAX_632_BREAK) / 2);
    
    float num632;
    int num632Rounded;
    
    num632 = f / ((MIN_632_BREAK + MAX_632_BREAK) / 2);
    num632Rounded = (int)roundf(num632);
    
    return  num632Rounded;
    
}


// Port size calculation // width and length are meters, but converted to inches. value is computed as inches and returned as meters

- (double)calcPortSizeFromWidth:(double)width andLength:(double)length andNumberPorts:(int)numPorts
{
    double inchesWidth = width * 39.3700787;
    double inchesLength = length * 39.3700787;
    double areaOverVolume = sqrt((M_PI * pow((.25 / 2), 2)) / 100); // should approx. .02216

    double inchesPortDiameter = areaOverVolume * inchesWidth * sqrt(inchesLength / numPorts);
    
    double metersPortDiameter = inchesPortDiameter * 0.0254;
    
    return metersPortDiameter;
}

// Two station tracking  //angle units in radians

- (double) calcAlt1ValueFromAzimuth1:(double)az1 andAzimuth2:(double)az2 andElevation1:(double)elev1 andBaseline:(double)b
{
    
    double baseline = b;
    double azimuth1 = az1;
    double azimuth2 = az2;
    double elevation1 = elev1;
    
    double betaAngle = M_PI - azimuth1 - azimuth2;
    double bcLength = (baseline * sin(azimuth2)) / sin(betaAngle);
    double alt1 = tan(elevation1) * bcLength;
    
    return alt1;

}

- (double)calcAlt2ValueFromAzimuth1:(double)az1 andAzimuth2:(double)az2 andElevation2:(double)elev2 andBaseline:(double)b
{
    
    double baseline = b;
    double azimuth1 = az1;
    double azimuth2 = az2;
    double elevation2 = elev2;
    
    double betaAngle = M_PI - azimuth1 - azimuth2;
    double acLength = (baseline * sin(azimuth1))/sin(betaAngle);
    double alt2 = tan(elevation2) * acLength;
    
    return alt2;

}

- (double)calcAverageAltFromAlt1:(double)a1 andAlt2:(double)a2
{
    double alt1 = a1;
    double alt2 = a2;
    double avgAlt = (alt1 + alt2) / 2;
    return avgAlt;
}

- (double)calcClosureErrorFromAlt1:(double)a1 andAlt2:(double)a2
{
    double alt1 = a1;
    double alt2 = a2;
    double cError = fabs(((alt1 - alt2)/(alt1 + alt2)) * 100);

    return cError;
}

// Single station tracking //elevation in radians and baseline in meters

- (double)calcAltitudeFromBaseline:(double)b andElevation:(double)e
{
    
    double baseline = b;
    double elevation = e;
    
    double altitude = baseline * tan(elevation);
    
    return altitude;
}


@end
