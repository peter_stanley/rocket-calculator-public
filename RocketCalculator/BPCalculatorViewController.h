//
//  BPCalculatorViewController.h
//  RocketCalculator
//
//  Created by Peter Stanley on 10/25/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

@class RAUnit;
@class CalcTextfield;

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface BPCalculatorViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UIPrintInteractionControllerDelegate> 
{
    bool isIpad;
    
    double length;
    double width;
    double pressure;
    double amount;
    double force;
    double poundsForce;

    int numScrews256;
    int numScrews440;
    int numScrews632;
    
    CalcTextfield *lengthTextField;
    UILabel *lengthUnitLabel;
    CalcTextfield *widthTextField;
    UILabel *widthUnitLabel;
    CalcTextfield *pressureTextField;
    UILabel *pressureUnitLabel;
    CalcTextfield *bpAmountTextField;
    UILabel *bpAmountUnitLabel;
    CalcTextfield *forceTextField;
    UILabel *forceUnitLabel;
    UILabel *widthUnitLabel2;
    UILabel *count256Label;
    UILabel *count440Label;
    UILabel *count632Label;
    
    RAUnit *lengthUnit;
    RAUnit *widthUnit;
    RAUnit *pressureUnit;
    RAUnit *amountUnit;
    RAUnit *forceUnit;
    RAUnit *tmpForceUnit;
    
    UIScrollView *scrollView;
    
    UITextField *activeField;
    UIActionSheet *sheet;

}

@property (nonatomic, retain) IBOutlet CalcTextfield *lengthTextField;
@property (nonatomic, retain) IBOutlet UILabel *lengthUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *widthTextField;
@property (nonatomic, retain) IBOutlet UILabel *widthUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *pressureTextField;
@property (nonatomic, retain) IBOutlet UILabel *pressureUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *bpAmountTextField;
@property (nonatomic, retain) IBOutlet UILabel *bpAmountUnitLabel;
@property (nonatomic, retain) IBOutlet CalcTextfield *forceTextField;
@property (nonatomic, retain) IBOutlet UILabel *forceUnitLabel;
@property (nonatomic, retain) IBOutlet UILabel *widthUnitLabel2;
@property (nonatomic, retain) IBOutlet UILabel *count256Label;
@property (nonatomic, retain) IBOutlet UILabel *count440Label;
@property (nonatomic, retain) IBOutlet UILabel *count632Label;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

- (IBAction)updateFields:(id)sender;
- (void) updateShearPinCounts;
- (IBAction)infoButtonAction:(id)sender;
- (IBAction) exportButtonAction:(id)sender;
- (IBAction) showEmailModalView:(id)sender;
- (IBAction) printCalculation:(id)sender;
- (NSString *) exportMsgText;
- (NSString *) exportMsgHtml;
- (void) enableTextfields;
- (void) resignAllResponders;
- (void) resetTextfieldDisplay;

@end