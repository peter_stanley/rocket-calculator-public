//
//  GenericFormulaViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 11/17/11.
//  Copyright (c) 2011 Peter Stanley. All rights reserved.
//

#import "GenericFormulaViewController.h"

@implementation GenericFormulaViewController

@synthesize htmlFileName, webView, dismissButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    webView.delegate = self;
    
    NSString * nsstrPath = [ [ NSBundle mainBundle ] pathForResource : htmlFileName ofType : @"html" ] ;
    NSURL * nsURL = [ NSURL fileURLWithPath : nsstrPath ] ;
    NSURLRequest * nsURLRequest = [ NSURLRequest requestWithURL : nsURL ] ;
    [ self.webView loadRequest : nsURLRequest ] ;
    
    // Just replace vwWeb with your UIWebView reference! 
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    self.dismissButton = nil;
    self.webView = nil;
    self.htmlFileName = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc
{

    [dismissButton release];
    [webView release];
    [htmlFileName release];
    
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)dismissView:(id)sender {
    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType; 
{
    NSURL *requestURL =[ [ request URL ] retain ]; 
    if ( ( [ [ requestURL scheme ] isEqualToString: @"http" ] || [ [ requestURL scheme ] isEqualToString: @"https" ] || [ [ requestURL scheme ] isEqualToString: @"mailto" ]) 
        && ( navigationType == UIWebViewNavigationTypeLinkClicked ) ) { 
        return ![ [ UIApplication sharedApplication ] openURL: [ requestURL autorelease ] ]; 
    } 
    [ requestURL release ]; 
    return YES; 
}

@end
