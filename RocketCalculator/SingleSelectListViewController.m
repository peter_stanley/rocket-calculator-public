//
//  SingleSelectListViewController.m
//  RocketCalculator
//
//  Created by Peter Stanley on 3/23/12.
//  Copyright 2011 Peter Stanley. All rights reserved.
//

#import "SingleSelectListViewController.h"
#import "RocketCalculatorAppDelegate.h"

#import "SingleSelectListViewCell.h"

@implementation SingleSelectListViewController

@synthesize delegate, selectedRow, itemList, listName, doneButton, singleSelectListTableView, singleSelectListViewCell;

#pragma mark -

- (id)init {
	if ((self = [super initWithNibName:@"SingleSelectListViewController" bundle:nil])) {
		
		self.title = self.listName;
	}
	
    
	return self;
	
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    return [self init];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedRow = [itemList count];
    //NSUInteger index = arc4random_uniform((uint32_t) predictionArray.count);
    
    isIpad = [(RocketCalculatorAppDelegate *)[[UIApplication sharedApplication]delegate] deviceIsIpad];
        
}

- (void)viewDidUnload {
    
    self.singleSelectListViewCell = nil;
    self.singleSelectListTableView = nil;
    self.doneButton = nil;
    self.listName = nil;
    self.itemList = nil;
    //self.selectedRow = [itemList count];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated 
{
        
    [self.singleSelectListTableView reloadData];
        
    [super viewWillAppear:animated];
    
}

- (void)dealloc 
{
    
    [singleSelectListViewCell release];
    [singleSelectListTableView release];
    [doneButton release];
    [listName release];
    [itemList release];
    
    [super dealloc];
}

#pragma mark -
#pragma mark Tableview methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    NSInteger numberOfRows = 0;
    numberOfRows = [itemList count];

    return numberOfRows;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 59;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    return listName;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *SingleSelectListViewCellIdentifier = @"SingleSelectViewCell";
    
    SingleSelectListViewCell *cell = (SingleSelectListViewCell *)[tableView dequeueReusableCellWithIdentifier:SingleSelectListViewCellIdentifier];
    if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"SingleSelectListViewCell" owner:self options:nil];
        cell = singleSelectListViewCell;
        self.singleSelectListViewCell = nil;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
    
}

- (void)configureCell:(SingleSelectListViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
            
        // Configure the cell
   // NSDictionary *tmpDict = [itemList objectAtIndex:indexPath.row];
    cell.label.text = [[itemList objectAtIndex:indexPath.row] valueForKey:@"labelHeader"];
    cell.labelDetail.text = [[itemList objectAtIndex:indexPath.row] valueForKey:@"labelDetail"];
    if (indexPath.row == selectedRow) {
        NSLog(@"selectedRow = %lu", (unsigned long)selectedRow);
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark]; 
        
    } else {
        
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (selectedRow < [itemList count]) {
        [[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0]] setAccessoryType:UITableViewCellAccessoryNone];

    }
    
    [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
    
    selectedRow = indexPath.row;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dismissViewController:(id)sender
{

    [delegate singleSelectListViewControllerWasDismissed:self];

    //[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end